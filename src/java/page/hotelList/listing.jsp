<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="ex" uri="/WEB-INF/custom.tld"%>

<%@page import="model.Facility" %>
<%@page import="java.util.ArrayList" %>
<%@page import="model.Room" %>
<%@page contentType="text/html" pageEncoding="UTF-8" %>

<!doctype html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <title>Hotel Petra Listing (list)</title>

        <%@ include file="../partial/include-head.jsp" %>

        <!--[if IE 9]>
        <script src="js/media.match.min.js"></script>
        <![endif]-->
    </head>
    <%ArrayList<Room> rooms = (ArrayList<Room>) request.getAttribute("roomList");%>
    <body>

        <!-- Start Main-Wrapper -->
        <div id="main-wrapper">

            <%@ include file="../partial/include-header.jsp" %>

            <!-- Start Header-Section -->
            <section class="header-section listing">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <h3 class="title-section pull-left">
                                Search Result
                            </h3>
                            <ul class="breadcrumbs custom-list list-inline pull-right">
                                <li><a href="#">Home</a></li>
                                <li><a href="#">Search Result</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </section>
            <!-- End Header-Section -->

            <section id="listing">
                <div class="container">
                    <div class="row">
                        <div class="sidebar col-md-3">
                            <div class="fleets-filters toggle-container">
                                <h5 class="toggle-title">Filters</h5>
                                <aside class="toggle-content">
                                    <div class="search">
                                        <form action="#" class="default-form">
                                            <input type="text" placeholder="Search">
                                            <i class="fa fa-search"></i>
                                        </form>
                                    </div>
                                    
                                    <div class="general">
                                        <h5 class="title">General</h5>
                                        <form action="#" class="default-form">
                                            <span class="arrival calendar">
                                                <input type="text" name="arrival" placeholder="Arrival"
                                                       data-dateformat="m/d/y">
                                                <i class="fa fa-calendar"></i>
                                            </span>
                                            <span class="departure calendar">
                                                <input type="text" name="departure" placeholder="Departure"
                                                       data-dateformat="m/d/y">
                                                <i class="fa fa-calendar"></i>
                                            </span>
                                            <span class="adults select-box">
                                                <select name="adults" data-placeholder="Adults">
                                                    <option>Adults</option>
                                                    <option value="1">1 adult</option>
                                                    <option value="2">2 adults</option>
                                                    <option value="3">3 adults</option>
                                                    <option value="4">4 adults</option>
                                                </select>
                                            </span>
                                            <span class="children select-box">
                                                <select name="children" data-placeholder="Children">
                                                    <option>Children</option>
                                                    <option value="1">1 children</option>
                                                    <option value="2">2 childrens</option>
                                                    <option value="3">3 childrens</option>
                                                    <option value="4">4 childrens</option>
                                                </select>
                                            </span>
                                        </form>
                                        <h5 class="title">Price</h5>
                                        <div class="slider-range-container box-row">
                                            <div class="slider-range" data-min="2000" data-max="100000" data-step="2000"
                                                 data-default-min="2000" data-default-max="50000" data-currency="$">
                                            </div>
                                            <div class="clearfix">
                                                <input type="text" class="range-from" value="$2000">
                                                <input type="text" class="range-to" value="$50000">
                                            </div>
                                        </div>
                                        <h5 class="title">Amenities</h5>
                                        <ul class="custom-list additional-filter-list clearfix">
                                            <li>
                                                <span class="checkbox-input">
                                                    <input id="additional-filter-ac" checked="checked" type="checkbox">
                                                    <label for="additional-filter-ac">AC</label>
                                                </span>
                                            </li>
                                            <li>
                                                <span class="checkbox-input">
                                                    <input id="additional-filter-hotbath" checked="checked"
                                                           type="checkbox">
                                                    <label for="additional-filter-hotbath">Hotbath</label>
                                                </span>
                                            </li>
                                            <li>
                                                <span class="checkbox-input">
                                                    <input id="additional-filter-parking" type="checkbox">
                                                    <label for="additional-filter-parking">Parking</label>
                                                </span>
                                            </li>
                                            <li>
                                                <span class="checkbox-input">
                                                    <input id="additional-filter-room-service" type="checkbox">
                                                    <label for="additional-filter-room-service">Room Service</label>
                                                </span>
                                            </li>
                                            <li>
                                                <span class="checkbox-input">
                                                    <input id="additional-filter-safe" checked="checked"
                                                           type="checkbox">
                                                    <label for="additional-filter-safe">Safe</label>
                                                </span>
                                            </li>
                                            <li>
                                                <span class="checkbox-input">
                                                    <input id="additional-filter-tv" checked="checked" type="checkbox">
                                                    <label for="additional-filter-tv">TV</label>
                                                </span>
                                            </li>
                                            <li>
                                                <span class="checkbox-input active">
                                                    <input id="additional-filter-wifi" checked="checked"
                                                           type="checkbox">
                                                    <label for="additional-filter-wifi">Wifi</label>
                                                </span>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="buttons">
                                        <button class="btn btn-transparent-gray pull-left">
                                            Filter
                                        </button>
                                        <button class="btn btn-transparent-gray pull-right">
                                            Reset
                                        </button>
                                    </div>
                                </aside>
                            </div>
                            <div class="special-offers sidebar">
                                <h5 class="title-section">Special Offers</h5>
                                <div class="offers-content">
                                    <ul class="custom-list">
                                        <li>
                                            <div class="thumbnail"><img src="img/listing-1.jpg" alt="" class="img-responsive">
                                            </div>
                                            <h5 class="title"><a href="#">King Suite</a></h5>
                                            <span class="price">from $99/day</span>
                                        </li>
                                        <li>
                                            <div class="thumbnail"><img src="img/listing-1.jpg" alt="" class="img-responsive">
                                            </div>
                                            <h5 class="title"><a href="#">King Suite</a></h5>
                                            <span class="price">from $99/day</span>
                                        </li>
                                        <li>
                                            <div class="thumbnail"><img src="img/listing-1.jpg" alt="" class="img-responsive">
                                            </div>
                                            <h5 class="title"><a href="#">King Suite</a></h5>
                                            <span class="price">from $99/day</span>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="listing-content col-md-9">
                            <div class="listing-pagination">
                                <h5 class="title pull-left">Available Room</h5>
                                <span class="adults select-box pull-right">
                                    <select name="perpage" data-placeholder="9 Per Page">
                                        <option>Per Page</option>
                                        <option value="1">10 Per Page</option>
                                        <option value="2">11 Per Page</option>
                                        <option value="3">12 Per Page</option>
                                        <option value="4">13 Per Page</option>
                                    </select>
                                </span>
                                <ul class="pagination custom-list list-inline pull-right">
                                    <li class="prev"><a href="#">Prev</a></li>
                                    <li class="number"><a href="#">1</a></li>
                                    <li class="number"><a href="#">2</a></li>
                                    <li class="number"><a href="#">3</a></li>
                                    <li class="next"><a href="#">Next</a></li>
                                </ul>
                            </div>

                            <%
                                for (int i = 0; i < rooms.size(); i++) {
                                    Room currentRoom = rooms.get(i);
                            %>

                            <div class="listing-room-list">
                                <div class="thumbnail">
                                    <div class="thumbnail-slider">
                                        <%if (currentRoom.getIconURL() != null) {%>
                                        <img src="<%= currentRoom.getIconURL()%>" alt="" class="img-responsive">

                                        <%
                                        } else {
                                        %>

                                        <img src="img/listing-1.jpg" alt="" class="img-responsive">
                                        <% }%>
                                        <!--<img src="img/listing-1.jpg" alt="" class="img-responsive">-->
                                    </div>
                                </div>
                                <div class="listing-room-content">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <header>
                                                <div class="pull-left">
                                                    <h5 class="title">
                                                        <a href="#"><%= currentRoom.getRoomNumber()%>
                                                        </a>
                                                    </h5>
                                                    <ul class="tags custom-list list-inline pull-left">
                                                        <li><a href="#"><%= currentRoom.getNoOfBed()%> Bed</a></li>
                                                        <li><a href="#"><%= currentRoom.getMaxNoOfAdult()%> Adults</a></li>
                                                        <li><a href="#"><%= currentRoom.getMaxNoOfChildren()%> Childrens</a>
                                                        </li>
                                                        <li><a href="#">Sea View</a></li>
                                                    </ul>
                                                </div>
                                                <div class="pull-right">
                                                    <span class="price">
                                                        from $<%= currentRoom.getPrice().getPrice()%>/day
                                                    </span>
                                                </div>
                                            </header>
                                            <div class="listing-facitilities">
                                                <!--<div class="listing-facitilities">-->
                                                <div class="row">
                                                    <% int count = 0;
                                                        for (Facility f : currentRoom.getInRoomFacilities()) {
                                                            if (count % 3 == 0) {%>

                                                    <%
                                                        }
                                                    %>
                                                    <!--<div class="row">-->
                                                    <div class="col-md-3 col-sm-3">
                                                        <ul class="facilities-list custom-list">
                                                            <li><%= f.getIconURL()%><span><%= f.getName()%></span></li>
                                                        </ul>
                                                    </div>
                                                    <!--</div>-->
                                                    <% //if (count % 3 == 0) {%>
                                                    <!--</div>-->
                                                    <% //}
                                                        count++;
                                                    } %>
                                                    <!--                                                <div class="row">
                                                                                                            <div class="col-md-3 col-sm-3">
                                                                                                                <ul class="facilities-list custom-list">
                                                    <% // for(Facility f:currentRoom.getInRoomFacilities()){%>
                                                    <li><%--= f.getIconURL()%><span><%= f.getName()--%></span></li>
                                                    
                                                    <%// } %>
                                                    <li><i class="fa fa-leaf"></i><span>AC</span></li>
                                                    <li><i class="fa fa-heart-o"></i><span>Hotbath</span></li>
                                                    <li><i class="fa fa-car"></i><span>Parking</span></li>
                                                </ul>
                                            </div>-->
                                                    <!--                                                    <div class="col-md-3 col-sm-3">
                                                                                                            <ul class="facilities-list custom-list">
                                                                                                                <li><i class="fa fa-female"></i><span>Room service</span></li>
                                                                                                                <li><i class="fa fa-key"></i><span>Free safe</span></li>
                                                                                                                <li><i class="fa fa-desktop"></i><span>TV & Audio</span></li>
                                                                                                            </ul>
                                                                                                        </div>
                                                                                                        <div class="col-md-3 col-sm-3">
                                                                                                            <ul class="facilities-list custom-list">
                                                                                                                <li><i class="fa fa-wifi"></i><span>WIFI access</span></li>
                                                                                                                <li><i class="fa fa-clock-o"></i><span>24/7 Service</span></li>
                                                                                                            </ul>
                                                                                                        </div>-->
                                                    <div class="col-md-3 col-sm-3 pull-right">
                                                        <button class="btn btn-transparent-gray">
                                                            Read More
                                                        </button>
                                                        <button class="btn btn-gray-dark">
                                                            Book now
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <%}%>
                            <div class="listing-pagination footer">
                                <ul class="pagination custom-list list-inline text-center no-margin">
                                    <li class="prev"><a href="#">Prev</a></li>
                                    <li class="number"><a href="#">1</a></li>
                                    <li class="number"><a href="#">2</a></li>
                                    <li class="number"><a href="#">3</a></li>
                                    <li class="next"><a href="#">Next</a></li>
                                </ul>
                            </div>
                            <div class="special-offers listing">
                                <h5 class="title-section">Special Offers</h5>
                                <div class="offers-content">
                                    <ul class="custom-list">
                                        <li>
                                            <div class="thumbnail"><img src="img/listing-1.jpg" alt="" class="img-responsive">
                                            </div>
                                            <h5 class="title"><a href="#">King Suite</a></h5>
                                            <span class="price">from $99/day</span>
                                        </li>
                                        <li>
                                            <div class="thumbnail"><img src="img/listing-1.jpg" alt="" class="img-responsive">
                                            </div>
                                            <h5 class="title"><a href="#">King Suite</a></h5>
                                            <span class="price">from $99/day</span>
                                        </li>
                                        <li>
                                            <div class="thumbnail"><img src="img/listing-1.jpg" alt="" class="img-responsive">
                                            </div>
                                            <h5 class="title"><a href="#">King Suite</a></h5>
                                            <span class="price">from $99/day</span>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <%@include file="../partial/include-footer.jsp" %>

        </div>
        <!-- End Main-Wrapper -->

        <%@ include file="../partial/include-js.jsp" %>

    </body>

</html>