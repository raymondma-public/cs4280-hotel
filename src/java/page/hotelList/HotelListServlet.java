/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package page.hotelList;

import database.DatabaseOperator;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Hotel;
import model.Room;
import page.BaseServlet;

/**
 *
 * @author RaymondMa
 */
public class HotelListServlet extends BaseServlet {

    private DatabaseOperator dbOperator = null;

    @Override
    public void init() throws ServletException {
        super.init(); //To change body of generated methods, choose Tools | Templates.
        dbOperator = DatabaseOperator.getInstance();
    }

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        Map<String, Object> model = new HashMap<String, Object>();
        int pageSize = 4;
        String pageString = (String) request.getParameter("page");

        int totalPage = 0;
        ArrayList<Hotel> hotels = null;
        totalPage = Hotel.getTotalPageNoFromDB(pageSize);
        int page = 1;

        if (pageString == null) {

            hotels = Hotel.getHotelListFromDB(1, pageSize);
        } else {
            page = Integer.parseInt(pageString);
            if (page <= 0) {// page number<0
                hotels = Hotel.getHotelListFromDB(1, pageSize);

                page = 1;
            } else if (page > totalPage) {// page number> all
                hotels = Hotel.getHotelListFromDB(totalPage, pageSize);
                page = totalPage;
            } else {// normal
                hotels = Hotel.getHotelListFromDB(page, pageSize);
            }

        }
        request.setAttribute("page", page);

        request.setAttribute("totalPage", totalPage);

        request.setAttribute("hotelList", hotels);

        String[] hotelImages = new String[hotels.size()];
        for (int i = 0; i < hotels.size(); i++) {
            Hotel tempHotel = hotels.get(i);
            hotelImages[i] = Hotel.getHotelImage("" + tempHotel.getId());

        }
        request.setAttribute("hotelImages", hotelImages);

        // TODO fix this, remove model (no need to pass variable as a Map)
        model.put("user", request.getSession().getAttribute("user"));
        model.put("message", "Hello World");
        request.setAttribute("model", model);
//        model.put("hotel", "酒店");
//        
//        
//        request.setAttribute("hotel", model);
        // Forward to JSP file to display message

        RequestDispatcher dispatcher = request
                .getRequestDispatcher("/WEB-INF/classes/page/hotelList/hotelList.jsp");
        dispatcher.forward(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
