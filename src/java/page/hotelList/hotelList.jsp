<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="ex" uri="/WEB-INF/custom.tld"%>

<%@page import="java.util.ArrayList"%>
<%@page import="model.Hotel" %>
<!doctype html>
<html lang="en">

    <!-- Mirrored from 188.226.241.135/hotelpetra/homepage_v1/news.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 24 Mar 2016 07:46:01 GMT -->
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <title><ex:TranslateTag  lang="${lang}">Hotels</ex:TranslateTag></title>

        <%@ include file="../partial/include-head.jsp" %>

        <!--[if IE 9]>
        <script src="js/media.match.min.js"></script>
        <![endif]-->
    </head>
    <%ArrayList<Hotel> hotels = (ArrayList<Hotel>) request.getAttribute("hotelList");
     String[] hotelImages=( String[] )request.getAttribute("hotelImages");
    %>
    <body>

        <!-- Start Main-Wrapper -->
        <div id="main-wrapper">

            <%@ include file="../partial/include-header.jsp" %>

            <!-- Start Header-Section -->
            <section class="header-section contact">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <h3 class="title-section pull-left">
                                <ex:TranslateTag  lang="${lang}">Hotels</ex:TranslateTag>
                                </h3>
                                <ul class="breadcrumbs custom-list list-inline pull-right">
                                    <li><ex:TranslateTag  lang="${lang}">Home</ex:TranslateTag></li>
                                <li><ex:TranslateTag  lang="${lang}">Hotels</ex:TranslateTag></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </section>
                <!-- End Header-Section -->

                <!-- Start News -->
                <section class="news">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="listing-pagination">
                                            <ul class="pagination custom-list list-inline">
                                                <li class="prev"><a href="hotelList?page=${page-1}">Prev</a></li>

                                            <c:forEach begin="1" end="${totalPage}" varStatus="loop">
                                                <li class="number"><a href="hotelList?page=<c:out value="${loop.index}"/>">${loop.index}</a></li>
                                                </c:forEach>
                                            <li class="prev"><a href="hotelList?page=${page+1}">Next</a></li>
                                        </ul>
                                    </div>
                                    <!--                            <ul class="pagination custom-list list-inline pull-right">
                                                                   
                                                                </ul>-->
                                </div>
                            </div>


                            <div class="row">
                                <%for(int i=0;i<hotels.size();i++){
                            Hotel currentHotel=hotels.get(i);%>
                                <div class="col-md-6">
                                    <div class="post ">
                                        <div class="post-thumbnail">
                                            <%if(hotelImages[i] !=null){%>
                                            <img src="<%=hotelImages[i]%>" alt="" class="img-responsive">
                                            <%}else{%>
                                            <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSvj78Q6M19k8GeCWnrkUdQyAhV2lMHruSG6GRiMgDMW9Tj66a_" alt="" class="img-responsive">
                                            <%}%>
                                        </div>
                                        <div class="post-content">
                                            <header class="post-header">
                                                <h5 class="pull-left title">
                                                    <a href="<%=request.getContextPath()%>/roomList?hotelId=<%=currentHotel.getId()%>"><%=currentHotel.getName()%></a>
                                                </h5>
                                                <!--<span class="pull-right date">21 July 2014</span>-->
                                            </header>
                                            <!--                                    <div class="post-meta">
                                                                                    <span class="category">
                                                                                        <i class="fa fa-tags"></i>
                                                                                        <a href="#">Lifestyle</a>, <a href="#">Luxury</a>
                                                                                    </span>
                                                                                    <span class="author">
                                                                                        <i class="fa fa-user"></i>
                                                                                        <a href="#">Admin</a>
                                                                                    </span>
                                                                                    <span class="comments">
                                                                                        <i class="fa fa-comment"></i>
                                                                                        <a href="#">2 Comments</a>
                                                                                    </span>
                                                                                </div>-->
                                            <p><%=currentHotel.getDescription()%></p>
                                            <a href="<%=request.getContextPath()%>/roomList?hotelId=<%=currentHotel.getId()%>" class="btn btn-transparent-gray"><ex:TranslateTag  lang="${lang}">Room List</ex:TranslateTag></a>
                                            <a href="<%=request.getContextPath()%>/hotelDetail?hotelId=<%=currentHotel.getId()%>" class="btn btn-transparent-gray"><ex:TranslateTag  lang="${lang}">Hotel Detail</ex:TranslateTag></a>
                                            </div>
                                        </div>
                                    </div>


                                <%}%>

                            </div>
                            <div class="listing-pagination">
                                <ul class="pagination custom-list list-inline">
                                    <li class="prev"><a href="hotelList?page=${page-1}">Prev</a></li>

                                    <c:forEach begin="1" end="${totalPage}" varStatus="loop">
                                        <li class="number"><a href="hotelList?page=<c:out value="${loop.index}"/>">${loop.index}</a></li>
                                        </c:forEach>
                                    <li class="prev"><a href="hotelList?page=${page+1}">Next</a></li>
                                </ul>
                            </div>
                        </div>

                    </div>
                </div>
            </section>
            <!-- End News -->

            <!-- Start Partners -->
            <section class="partners partners-white">
                <div class="container">
                    <div class="row">
                        <div class="preamble col-md-12">
                            <h3>Our Partners</h3>
                        </div>
                        <div class="col-md-12">
                            <div id="clients-slider" class="owl-carousel">
                                <img src="img/company1.png" alt="" class="img-responsive">
                                <img src="img/company2.png" alt="" class="img-responsive">
                                <img src="img/company3.png" alt="" class="img-responsive">
                                <img src="img/company1.png" alt="" class="img-responsive">
                                <img src="img/company2.png" alt="" class="img-responsive">
                                <img src="img/company3.png" alt="" class="img-responsive">
                                <img src="img/company1.png" alt="" class="img-responsive">
                                <img src="img/company2.png" alt="" class="img-responsive">
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- End Partners -->

            <%@include file="../partial/include-footer.jsp" %>

        </div>
        <!-- End Main-Wrapper -->

        <%@ include file="../partial/include-js.jsp" %>
    </body>

    <!-- Mirrored from 188.226.241.135/hotelpetra/homepage_v1/news.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 24 Mar 2016 07:46:02 GMT -->
</html>