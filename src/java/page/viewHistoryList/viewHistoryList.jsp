<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="ex" uri="/WEB-INF/custom.tld"%>

<%@page import="model.Facility" %>
<%@page import="java.util.ArrayList" %>
<%@page import="model.Room" %>
<%@page import="model.Price" %>
<%@page contentType="text/html" pageEncoding="UTF-8" %>

<!doctype html>
<html lang="en">

    <head>
        <title><ex:TranslateTag  lang="${lang}">View History</ex:TranslateTag></title>
        <%@ include file="../partial/include-head.jsp" %>
    </head>

    <body>

        <!-- Start Main-Wrapper -->
        <div id="main-wrapper">

            <%@ include file="../partial/include-header.jsp" %>

            <!-- Start Header-Section -->
            <section class="header-section listing">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <h3 class="title-section pull-left">
                                <ex:TranslateTag  lang="${lang}">View History</ex:TranslateTag>
                            </h3>
                            <ul class="breadcrumbs custom-list list-inline pull-right">
                                <li><ex:TranslateTag  lang="${lang}">Home</ex:TranslateTag></li>
                                <li><ex:TranslateTag  lang="${lang}">View History</ex:TranslateTag></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </section>
            <!-- End Header-Section -->

            <section id="listing">
                <div class="container">
                    <form class="row default-form" method="get">
                        <%@include file="../partial/include-userPageMenu.jsp" %>

                        <div class="listing-content col-md-9">

                            <div class="listing-pagination">

                                <div class="ibox-content">
                                    <div class="row m-b-sm m-t-sm">
                                        <!--                                        <div class="col-md-1">
                                                                                    <button type="button" id="loading-example-btn" class="btn btn-white btn-sm"><i class="fa fa-refresh"></i> Refresh</button>
                                                                                </div>-->
                                        <!--                                        <div class="col-md-12">
                                                                                    <div class="input-group"><input type="text" placeholder="Search" class="input-sm form-control"> <span class="input-group-btn">
                                                                                            <button type="button" class="btn btn-sm btn-primary"> Go!</button> </span></div>
                                                                                </div>-->
                                    </div>

                                    <div class="project-list">

                                        <table class="table table-hover">
                                            <tbody>

                                                <c:forEach items="${viewHistorys}" var="viewHistory">
                                                    <tr>
                                                        <!--                                                    <td class="project-status">
                                                                                                                <span class="label label-primary">Active</span>
                                                                                                            </td>-->
                                                        <td class="project-title">
                                                            <a href="room/${viewHistory.room.id}">${viewHistory.room.roomNumber}</a>
                                                            <br>
                                                            <small>${viewHistory.date}</small>
                                                        </td>
                                                        <td class="project-completion">
                                                            $${viewHistory.room.price}
                                                            <!--                                                        <small>Completion with: 48%</small>
                                                                                                                    <div class="progress progress-mini">
                                                                                                                        <div style="width: 48%;" class="progress-bar"></div>
                                                                                                                    </div>-->
                                                        </td>

                                                        <td class="project-people">
                                                            <c:if test="${not empty viewHistory.room.iconURL}">
                                                                <a href=""><img alt="image" width="50px" height="50px" class="img-circle" src="${viewHistory.room.iconURL}"></a>
                                                                </c:if>
                                                        </td>
                                                        <td class="project-people">

                                                        </td>
                                                        <td class="project-actions pull-right">
                                                            <a href="removeViewHistory?roomId=${viewHistory.room.id}" class="btn btn-danger btn-sm"><i class="fa fa-remove"></i> <ex:TranslateTag  lang="${lang}">Remove</ex:TranslateTag> </a>
                                                            <!--<a href="#" class="btn btn-white btn-sm"><i class="fa fa-pencil"></i> Edit </a>-->
                                                        </td>
                                                    </tr>
                                                </c:forEach>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>


                            </div>

                        </div>
                    </form>
                </div>
            </section>

            <%@include file="../partial/include-footer.jsp" %>

        </div>
        <!-- End Main-Wrapper -->

        <%@ include file="../partial/include-js.jsp" %>

    </body>

</html>