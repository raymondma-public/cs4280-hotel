package page.userRegister;

import com.google.gson.Gson;
import model.Hotel;
import model.User;
import org.apache.commons.validator.ValidatorException;
import page.BaseServlet;

import java.io.IOException;
import java.net.URLEncoder;
import java.sql.SQLException;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class RegisterUserServlet extends BaseServlet {

    @Override
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // do nothing
        
    }

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        redirectHome(response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        super.doPost(request, response);
        HttpSession session = request.getSession(false);
        // TODO: rate limit by counting the number of account registered within a session
        Map<String, Object> data = (Map<String, Object>) request.getAttribute("responseData");

        String role = "customer";
        String email = request.getParameter("email");
        String password = request.getParameter("password");
        String managingHotelId = null;

        User logonUser = (User) request.getAttribute("user");
        if (logonUser != null && logonUser.isChiefManager()) {
            role = request.getParameter("role");
            managingHotelId = request.getParameter("managingHotelId");
        }

        data.put("role", role);
        data.put("email", email);
        data.put("password", password);
        data.put("managingHotelId", managingHotelId);

        try {
            Hotel hotel = null;
            if (role.equals("manager"))
                hotel = Hotel.getHotelFromDB(Integer.parseInt(managingHotelId));
            User.register(role, email, password, hotel);
            request.setAttribute("success_message", "Register successful, please login");
            if (logonUser == null || !logonUser.isChiefManager())
                dispatch(HttpServletResponse.SC_ACCEPTED, "WEB-INF/classes/page/index/index.jsp", request, response);
        } catch (ValidatorException ex) {
            request.setAttribute("error_message", ex.getMessage());
            data.put("error", ex.getMessage());
            if (logonUser == null || !logonUser.isChiefManager())
                dispatch(HttpServletResponse.SC_BAD_REQUEST, "/WEB-INF/classes/page/index/index.jsp", request, response);
        } catch (SQLException ex) {
            request.setAttribute("error_message", "Unexpected Error, please try again later");
            if (ex.getErrorCode() == 2627)
                request.setAttribute("error_message", "Email already register, please user another one");
            data.put("error", ex.getMessage());
            if (logonUser == null || !logonUser.isChiefManager())
                dispatch(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "/WEB-INF/classes/page/index/index.jsp", request, response);
        } finally {
            Logger.getLogger("register").log(Level.INFO, new Gson().toJson(data));
            if (logonUser != null && logonUser.isChiefManager()) {
                String path = request.getHeader("referer");
                if (path == null)
                    path = "home";
                if (request.getAttribute("success_message") != null)
                    response.sendRedirect(path + "?success_message=" + URLEncoder.encode("User Created", "UTF-8"));
                else
                    response.sendRedirect(path + "?error_message=" + URLEncoder.encode((String) request.getAttribute("error_message"), "UTF-8"));
            }
        }
    }

}
