<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="ex" uri="/WEB-INF/custom.tld"%>

<%@page import="model.Facility" %>
<%@page import="java.util.ArrayList" %>
<%@page import="model.Room" %>
<%@page import="model.Price" %>
<%@page contentType="text/html" pageEncoding="UTF-8" %>

<!doctype html>
<html lang="en">

    <head>
        <title><ex:TranslateTag  lang="${lang}">Edit User</ex:TranslateTag></title>
        <%@ include file="../partial/include-head.jsp" %>
    </head>

    <body>

        <!-- Start Main-Wrapper -->
        <div id="main-wrapper">

            <%@ include file="../partial/include-header.jsp" %>

            <!-- Start Header-Section -->
            <section class="header-section listing">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <h3 class="title-section pull-left">
                                <ex:TranslateTag  lang="${lang}">Edit User</ex:TranslateTag>
                                </h3>
                                <ul class="breadcrumbs custom-list list-inline pull-right">
                                    <li><ex:TranslateTag  lang="${lang}">Home</ex:TranslateTag></li>
                                <li><ex:TranslateTag  lang="${lang}">User Detail</ex:TranslateTag></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </section>
                <!-- End Header-Section -->

                <section id="listing">
                    <div class="container">
                        <!--<form class="row default-form" method="get">-->
                    <%@include file="../partial/include-userPageMenu.jsp" %>

                    <div class="listing-content col-md-9">

                        <div class="listing-pagination">
                            <h5 class="title "><ex:TranslateTag  lang="${lang}">Edit User Detail</ex:TranslateTag></h5>





                                <!--<button class="btn btn-primary pull-right"><a href="editUserPage">Edit</a></button>-->


                            <c:if test="${not empty user.iconImageURL}">
                                <p> <br><img src="${user.iconImageURL}" width="100px" height="100px"></p>
                                </c:if>

                            <c:if test="${not empty user.userType}">
                                <p> <ex:TranslateTag  lang="${lang}">User Type</ex:TranslateTag>: ${user.userType}</p>
                            </c:if>

                            <form id="registerForm" method="POST">
                                <!---form--->           <div class="form-group">
                                    <!---input width--->    <div class="col-xs-12">


                                        <label for="InputName">Email</label>
                                        <div class="input-group">
                                            <div type="email" class="form-control" name="email" placeholder="Enter Email" disabled>${user.email}</div>
                                            <span class="input-group-addon"><span class="fa fa-envelope-o"></span></span>
                                        </div>
                                        <br>

                                        <label for="InputName"><ex:TranslateTag  lang="${lang}">Password(If do not need to change, do not fill in)</ex:TranslateTag></label>
                                        <div class="input-group">
                                            <input type="password" class="form-control" name="password" placeholder="<ex:TranslateTag  lang="${lang}">Enter Password</ex:TranslateTag>">
                                            <span class="input-group-addon"><span class="fa fa-lock"></span></span>
                                        </div>

                                        <div class="input-group">
                                            <input type="password" class="form-control" name="reEnterPassword" placeholder="Re-type Password">
                                            <span class="input-group-addon"><span class="fa fa-lock"></span></span>
                                        </div>

                                        <!--------------------------------------separator---------------------------------------------------------------> <hr>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-xs-12">

                                        <label for="InputName"><ex:TranslateTag  lang="${lang}">User Name</ex:TranslateTag></label>
                                        <div class="input-group">
                                            <input type="text" class="form-control" name="name" placeholder="Enter User Name"  value="${user.userName}" required>
                                            <span class="input-group-addon"><span class="fa fa-user"></span></span>
                                        </div>
                                        <br>


                                        <label for="InputName"><ex:TranslateTag  lang="${lang}">Phone</ex:TranslateTag></label>
                                        <div class="input-group">
                                            <input type="phone" class="form-control" name="phone" placeholder="Enter Phone" value="${user.phone}" required>
                                            <span class="input-group-addon"><span class="fa fa-phone"></span></span>
                                        </div>
                                        <br>
                                        <!--------------------------------------separator---------------------------------------------------------------> <hr>
                                    </div>       
                                </div> 

                                <!--                                <div class="form-group"> 
                                                                    <div class="col-xs-12">
                                                                        <label for="InputEmail">Enter Email</label>
                                                                        <div class="input-group">
                                                                            <input type="email" class="form-control" name="email" placeholder="Enter Email" required>
                                                                            <span class="input-group-addon"><span class="glyphicon glyphicon-asterisk"></span></span>
                                                                        </div>
                                                                        --------------------------break----------------------------------------------------------- <br>
                                                                    </div>
                                                                </div>
                                
                                                                <div class="form-group">
                                                                    <div class="col-xs-12">
                                                                        <label for="InputStreetName">Address</label>
                                                                        <div class="input-group">
                                                                            <input type="text" class="form-control" name="street_name" placeholder="Enter Street Name and Number" required>
                                                                            <span class="input-group-addon"><span class="glyphicon glyphicon-asterisk"></span></span>
                                                                        </div>
                                                                        --------------------------break----------------------------------------------------------- <br> 
                                                                    </div>                     
                                                                </div>
                                
                                                                <div class="form-group">
                                                                    <div class="col-xs-12">
                                                                        <label for="InputCity">City</label>
                                                                        <div class="input-group">
                                                                            <input type="text" class="form-control" name="city" placeholder="Enter City" required>
                                                                            <span class="input-group-addon"><span class="glyphicon glyphicon-asterisk"></span></span>
                                                                        </div>
                                                                        --------------------------break----------------------------------------------------------- <br> 
                                                                    </div>
                                                                </div>
                                
                                                                <div class="form-group">
                                                                    <div class="col-xs-12">
                                                                        <label for="InputProvince">Province</label>
                                                                        <div class="input-group">
                                                                            <input type="text" class="form-control" name="province" placeholder="Enter Province" required>
                                                                            <span class="input-group-addon"><span class="glyphicon glyphicon-asterisk"></span></span> 
                                                                        </div>
                                                                        --------------------------break----------------------------------------------------------- <br>                     
                                                                    </div>
                                                                </div>-->

                                <div class="form-group">
                                    <div class="input-group-addon">
                                        <input type="submit" name="submit" id="submit" value="<ex:TranslateTag  lang="${lang}">Submit</ex:TranslateTag>" class="btn btn-success pull-right">

                                    </div>
                                </div>
                            </form>



                        </div>



                    </div>
                    <!--</form>-->
                </div>
            </section>

            <%@include file="../partial/include-footer.jsp" %>

        </div>
        <!-- End Main-Wrapper -->

        <%@ include file="../partial/include-js.jsp" %>

    </body>

</html>