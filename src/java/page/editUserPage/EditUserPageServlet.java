package page.editUserPage;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import database.DatabaseOperator;
import exception.UnauthorizedException;
import model.User;
import page.BaseServlet;

public class EditUserPageServlet extends BaseServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        super.doGet(request, response);
        try {
            checkUserLogon(request);

            dispatch("/WEB-INF/classes/page/editUserPage/editUserPage.jsp", request, response);
        } catch (UnauthorizedException e) {
            redirectError(response, 401);
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        super.doPost(request, response);
        try {
            checkUserLogon(request);
            User user = (User) request.getAttribute("user");

            String name = request.getParameter("name");
            String password = request.getParameter("password");
            String reEnterPassword = request.getParameter("reEnterPassword");
            String phone = request.getParameter("phone");

            if (!password.equals(reEnterPassword)) {
                request.setAttribute("error_message", "Password mismatch");
                dispatch(HttpServletResponse.SC_BAD_REQUEST, "/WEB-INF/classes/page/editUserPage/editUserPage.jsp", request, response);
                return;
            }

            Connection con = DatabaseOperator.getInstance().getConnection();
            try {
                con.setAutoCommit(false);

                User.updateNameToDB(user.getId(), name);
                user.setUserName(name);
                User.updatePhoneToDB(user.getId(), phone);
                user.setPhone(phone);
                User.updatePasswordToDB(user.getId(), password);

                con.commit();
                request.setAttribute("success_message", "User detail Updated");
                dispatch(HttpServletResponse.SC_OK, "/WEB-INF/classes/page/userDetail/userDetail.jsp", request, response);
                return;
            } catch (SQLException ex) {
                Logger.getLogger(EditUserPageServlet.class.getName()).log(Level.SEVERE, null, ex);
                try {
                    con.rollback();
                    con.setAutoCommit(true);
                } catch (SQLException ex1) {
                    Logger.getLogger(EditUserPageServlet.class.getName()).log(Level.SEVERE, null, ex1);
                }
            }

            request.setAttribute("error_message", "Unexpected Error, please try again later");
            dispatch(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "/WEB-INF/classes/page/editUserPage/editUserPage.jsp", request, response);
        } catch (UnauthorizedException e) {
            redirectError(response, 401);
        }
    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        // do nothing
    }

}
