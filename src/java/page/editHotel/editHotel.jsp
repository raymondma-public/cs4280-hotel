<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="ex" uri="/WEB-INF/custom.tld"%>


<%@ page contentType="text/html" pageEncoding="UTF-8" %>

<!doctype html>
<html lang="en">

    <head>
        <title><ex:TranslateTag  lang="${lang}">Edit Hotel</ex:TranslateTag></title>
        <%@ include file="../partial/include-head.jsp" %>
        <style>
            <%@ include file="editHotel.css" %>
        </style>
    </head>

    <body>

        <!-- Start Main-Wrapper -->
        <div id="main-wrapper">

            <%@ include file="../partial/include-header.jsp" %>

            <!-- Start Header-Section -->
            <section class="header-section listing">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <h3 class="title-section pull-left">
                                <ex:TranslateTag  lang="${lang}">Edit Hotel</ex:TranslateTag>
                                </h3>
                                <ul class="breadcrumbs custom-list list-inline pull-right">
                                    <li><ex:TranslateTag  lang="${lang}">Home</ex:TranslateTag></li>
                                <li><ex:TranslateTag  lang="${lang}">Edit Hotel</ex:TranslateTag></li>

                                </ul>
                            </div>
                        </div>
                    </div>
                </section>
                <!-- End Header-Section -->

                <section id="listing">
                    <div class="container">

                            <div class="sidebar col-md-3">
                        <div class="fleets-filters toggle-container">
                            <div class="special-offers sidebar">
                                <a href="${basePath}/userList"><h5 class="title-section"><ex:TranslateTag  lang="${lang}">User List</ex:TranslateTag></h5></a>
                            </div>
                            <div class="special-offers sidebar">
                                <a href="${basePath}/createUser"><h5 class="title-section"><ex:TranslateTag  lang="${lang}">Create User</ex:TranslateTag></h5></a>
                            </div>

                            <div class="special-offers sidebar">
                                <a href="${basePath}/createHotel"><h5 class="title-section"><ex:TranslateTag  lang="${lang}">Create Hotel</ex:TranslateTag></h5></a>
                            </div>
                            <div class="special-offers sidebar">
                                <a href="${basePath}/manageHotelList"><h5 class="title-section"><ex:TranslateTag  lang="${lang}">Manage Hotel List</ex:TranslateTag></h5></a>
                            </div>
                        </div>

                    </div>

                        <div class="listing-content col-md-9">

                            <div class="listing-pagination">
                                <form  class="form-horizontal" method="POST">
                                    <input type="hidden" name="hotelId" value="${hotel.id}"/>
                                <div class="form-group">
                                    <label for="hotelType"  required class="col-sm-2 control-label"><ex:TranslateTag  lang="${lang}">Hotel Type</ex:TranslateTag></label>
                                        <div class="col-sm-10">
                                            <input type="text" required name="hotelType" class="form-control" id="purpose" placeholder="<ex:TranslateTag  lang="${lang}">Hotel Type</ex:TranslateTag>" value="${hotel.hotelType}">
                                        </div>  
                                    </div>
                                    <div class="form-group">
                                        <label for="name" class="col-sm-2 control-label"><ex:TranslateTag  lang="${lang}">Hotel Name</ex:TranslateTag></label>
                                        <div class="col-sm-10">
                                            <input type="text" required name="name" class="form-control" id="purpose" placeholder="<ex:TranslateTag  lang="${lang}">Hotel Name</ex:TranslateTag>" value="${hotel.name}">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="phone" class="col-sm-2 control-label"><ex:TranslateTag  lang="${lang}">Phone</ex:TranslateTag></label>
                                        <div class="col-sm-10">
                                            <input type="text" required name="phone" class="form-control" id="purpose" placeholder="<ex:TranslateTag  lang="${lang}">Phone</ex:TranslateTag>" value="${hotel.phone}">
                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <label for="description" class="col-sm-2 control-label"><ex:TranslateTag  lang="${lang}">Description</ex:TranslateTag></label>
                                        <div class="col-sm-10">
                                            <textarea name="description" required class="form-control" id="description" placeholder="<ex:TranslateTag  lang="${lang}">Description</ex:TranslateTag>">${hotel.description}</textarea>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <a href="http://mondeca.com/index.php/en/any-place-en" target="_blank"><ex:TranslateTag  lang="${lang}">Get your hotel location here</ex:TranslateTag></a>
                                    </div>

                                    <div class="form-group">
                                        <label for="latitude" class="col-sm-2 control-label"><ex:TranslateTag  lang="${lang}">Latitude</ex:TranslateTag></label>
                                        <div class="col-sm-10">
                                            <input type="text" name="latitude" required class="form-control" id="purpose" placeholder="<ex:TranslateTag  lang="${lang}">Latitude</ex:TranslateTag>" value="${hotel.latitude}">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="longitude" class="col-sm-2 control-label"><ex:TranslateTag  lang="${lang}">Longitude</ex:TranslateTag></label>
                                        <div class="col-sm-10">
                                            <input type="text" required name="longitude" class="form-control" id="purpose" placeholder="<ex:TranslateTag  lang="${lang}">Longitude</ex:TranslateTag>" value="${hotel.longitude}">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-sm-offset-2 col-sm-10">
                                            <button type="submit" class="btn btn-default">Update</button>
                                        </div>
                                    </div>

                                </form>
                            </div>

                        </div>
                        <!--</form>-->
                    </div>
                </section>

            <%@include file="../partial/include-footer.jsp" %>

        </div>
        <!-- End Main-Wrapper -->

        <%@ include file="../partial/include-js.jsp" %>
        <script>
            <%@ include file="editHotel.js" %>
        </script>

    </body>

</html>