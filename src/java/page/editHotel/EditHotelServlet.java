/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package page.editHotel;

import exception.ForbiddenException;
import exception.UnauthorizedException;
import exception.UserNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Facility;
import model.Hotel;
import model.Room;
import model.User;
import page.BaseServlet;
import page.editRoom.EditRoomServlet;
import page.management.createRoom.createRoomServlet;

/**
 *
 * @author RaymondMa
 */
public class EditHotelServlet extends BaseServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            checkUserPermission("chiefManager", request);

            request.setAttribute("basePath", "management");

            ArrayList<Facility> facilities = Facility.getFacilities();

            User user = this.getCurrentUser(request);

            String hotelId = request.getParameter("hotelId");

//            Room room = Room.getRoom(Integer.parseInt(roomId));
            Hotel hotel = Hotel.getHotelFromDB(Integer.parseInt(hotelId));

            request.setAttribute("hotel", hotel);
//            if (room.getHotel().getId() != user.getManagingHotel().getId()) {
//                redirectError(response, 401);
//                return;
//            }
//            ArrayList<Boolean> containF = new ArrayList();
//            boolean[] containF = new boolean[facilities.size()];
//            int count = 0;
//            System.out.println("Size: " + room.getInRoomFacilities().size());
//
//            for (Facility f : facilities) {
//                containF[count] = false;
//
//                System.out.println("===========" + f.getId());
//                for (Facility fRoom : room.getInRoomFacilities()) {
//                    System.out.println("Compare : " + f.getId() + " " + fRoom.getId());
//                    if (f.getId() == fRoom.getId()) {
//
//                        containF[count] = true;
//                        continue;
//                    }
//                }
//                count++;
//
//            }
//            request.setAttribute("containF", containF);
//            request.setAttribute("facilities", facilities);

            dispatch("/WEB-INF/classes/page/editHotel/editHotel.jsp", request, response);
        } catch (UnauthorizedException e) {
            redirectError(response, 401);
        } catch (ForbiddenException e) {
            redirectError(response, 403);
        } catch (SQLException e) {
            Logger.getLogger(createRoomServlet.class.getName()).log(Level.WARNING, null, e);
            redirectError(response, 500);
        } catch (UserNotFoundException ex) {
            Logger.getLogger(EditRoomServlet.class.getName()).log(Level.SEVERE, null, ex);
            redirectError(response, 401);
            return;
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        try {
            checkUserPermission("chiefManager", request);

            request.setAttribute("basePath", "management");

//            ArrayList<Facility> facilities = Facility.getFacilities();
            User user = this.getCurrentUser(request);

            String hotelId = request.getParameter("hotelId");

            String hotelType = request.getParameter("hotelType");
            String name = request.getParameter("name");
            String phone = request.getParameter("phone");
            String description = request.getParameter("description");
            String latitudeString = request.getParameter("latitude");
            String longitudeString = request.getParameter("longitude");
            double latitude = Double.parseDouble(latitudeString);
            double longitude = Double.parseDouble(longitudeString);
            Hotel.updateHotel(hotelId, hotelType, name, phone, description, latitude, longitude);
            Hotel hotel = Hotel.getHotelFromDB(Integer.parseInt(hotelId));
//            Room room = Room.getRoom(Integer.parseInt(roomId));
            request.setAttribute("hotel", hotel);

            request.setAttribute("success_message", "Changed");
            dispatch("/WEB-INF/classes/page/editHotel/editHotel.jsp", request, response);
        } catch (UnauthorizedException e) {
            redirectError(response, 401);
        } catch (ForbiddenException e) {
            redirectError(response, 403);
        } catch (SQLException e) {
            Logger.getLogger(createRoomServlet.class.getName()).log(Level.WARNING, null, e);
            request.setAttribute("error_message", "SQL: " + e.getMessage());
            dispatch("/WEB-INF/classes/page/editHotel/editHotel.jsp", request, response);
//            redirectError(response, 500);
        } catch (UserNotFoundException ex) {
            Logger.getLogger(EditRoomServlet.class.getName()).log(Level.SEVERE, null, ex);
            redirectError(response, 401);
            return;
        }

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
