/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package page.booking;

import exception.BookingRangeNotOkException;
import exception.UnauthorizedException;
import exception.UserNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.BookMark;
import model.Booking;
import model.User;
import page.BaseServlet;

/**
 *
 * @author RaymondMa
 */
public class BookingServlet extends BaseServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        BaseServlet.redirectError(response, 404);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        System.out.println("BookingServelt.doPost");
        try {
            super.checkUserLogon(request);

        } catch (UnauthorizedException ex) {
            BaseServlet.redirectError(response, 401);
            return;
        }

        try {

            String roomId = request.getParameter("roomId");
            String start = request.getParameter("start");
            String end = request.getParameter("end");
            User user = this.getCurrentUser(request);
            System.out.println("before addBookingToDB");
            Booking.addBookingToDB(user.getId(), roomId, start, end);

            int bookingId = Booking.getLastModifiedId();
            response.sendRedirect("bookingDetail?bookingId=" + bookingId);
            return;
        } catch (UserNotFoundException ex) {
            BaseServlet.redirectError(response, 401);
            return;
        } catch (SQLException ex) {
            Logger.getLogger(BookingServlet.class.getName()).log(Level.SEVERE, null, ex);
            String roomId = request.getParameter("roomId");
            response.sendRedirect("room/" + roomId);
            return;
        } catch (BookingRangeNotOkException ex) {

            Logger.getLogger(BookingServlet.class.getName()).log(Level.SEVERE, null, ex);
            String roomId = request.getParameter("roomId");
            response.sendRedirect("room/" + roomId);
            return;
        }

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
