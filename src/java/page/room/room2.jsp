<%-- 
    Document   : room
    Created on : Apr 3, 2016, 7:38:21 PM
    Author     : RaymondMa
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="ex" uri="/WEB-INF/custom.tld"%>

<%@ page import="model.Room" %>
<%@page import="model.Facility" %>
<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<% Room room = (Room) request.getAttribute("room");%>

<!doctype html>
<html lang="en">

    <!-- Mirrored from 188.226.241.135/hotelpetra/homepage_v1/room.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 24 Mar 2016 07:45:09 GMT -->
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <title>Hotel Petra Room</title>

        <%@ include file="../partial/include-head.jsp" %>
    </head>

    <body>

        <!-- Start Main-Wrapper -->
        <div id="main-wrapper">

            <%@ include file="../partial/include-header.jsp" %>


            <!-- Start Header-Section -->
            <section class="header-section room" style="background: url(${room.iconURL})   ;  background-size: cover">
                <div id="gradient"></div>
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <h3 class="title-section pull-left">
                                ${room.roomNumber} - ${room.purpose}
                            </h3>
                            <ul class="breadcrumbs custom-list list-inline pull-right">

                            </ul>
                        </div>
                    </div>
                </div>
            </section>
            <!-- End Header-Section -->

            <!-- Start Room -->
            <section id="room">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="room-wrapper negative-margin">
                                <div class="sidebar col-md-3">
                                    <!--<div id="map" lat="${room.hotel.latitude}" long="${room.hotel.longitude}" latitude="${room.hotel.latitude}" longitude="${room.hotel.longitude}"></div>-->
                                    <c:if test="${room.hotel.latitude>0 &&room.hotel.longitude>0 }">
                                        <!--<iframe class="col-md-12"  frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.com/maps?q='+${room.hotel.latitude}+','+${room.hotel.longitude}+'&hl=es;z=5&amp;output=embed"></iframe><br /><small><a href="https://maps.google.com/maps?q='+${room.hotel.latitude}+','+${room.hotel.longitude}+'&hl=es;z=14&amp;output=embed" style="color:#0000FF;text-align:left" target="_blank">See map bigger</a></small>-->
                                        <iframe
                                            class="col-md-12"
                                            width="600"
                                            height="450"
                                            frameborder="0" style="border:0"
                                            src="https://www.google.com/maps/embed/v1/place?key=AIzaSyAT-e2GjPZMh1Lfqe7wyFYVDwmfnZSHv34
                                            &center=${room.hotel.latitude},${room.hotel.longitude}
                                            &zoom=18&q=record+stores+in+Seattle" allowfullscreen>
                                        </iframe>
                                    </c:if>

                                    <c:if test="${not empty user}">
                                        <div class="sidebar-widget reservation">
                                            <h5 class="widget-title"><ex:TranslateTag  lang="${lang}">Make a reservation</ex:TranslateTag></h5>
                                                <aside class="widget-content">
                                                    <!--<form action="booking" method="post">-->
                                                    <form action="booking" class="default-form" method="post">

                                                        <input type="hidden" name="roomId" value="${room.id}">
                                                    <span class="arrival calendar">
                                                        <input type="text" name="start" placeholder="Arrival" data-dateformat="m/d/y">
                                                        <i class="fa fa-calendar"></i>
                                                    </span>
                                                    <span class="departure calendar">
                                                        <input type="text" name="end" placeholder="Departure" data-dateformat="m/d/y">
                                                        <i class="fa fa-calendar"></i>
                                                    </span>
                                                    <!--                                                <span class="adults select-box">
                                                                                                        <select name="adults" data-placeholder="Adults">
                                                                                                            <option>Adults</option>
                                                                                                            <option value="1">1 adult</option>
                                                                                                            <option value="2">2 adults</option>
                                                                                                            <option value="3">3 adults</option>
                                                                                                            <option value="4">4 adults</option>
                                                                                                        </select>
                                                                                                    </span>
                                                                                                    <span class="children select-box">
                                                                                                        <select name="children" data-placeholder="Children">
                                                                                                            <option>Children</option>
                                                                                                            <option value="1">1 children</option>
                                                                                                            <option value="2">2 childrens</option>
                                                                                                            <option value="3">3 childrens</option>
                                                                                                            <option value="4">4 childrens</option>
                                                                                                        </select>
                                                                                                    </span>-->
                                                    <!--<h5>Booking Cost: <span>$0.00</span></h5>-->
                                                    <!--<button class="btn btn-transparent-gray">Book</button>-->
                                                    <input type="submit"  class="btn btn-transparent-gray" value="Book"/>
                                                </form>
                                            </aside>
                                        </div>
                                    </c:if >
                                    <%@include file="../partial/include-suggestList-vertical-wraped-content.jsp" %>

                                </div>
                                <div class="room-content col-md-9">
                                    <div class="room-general">
                                        <img src="${room.iconURL}" alt="" class="img-responsive">
                                        <header>
                                            <div class="pull-left">
                                                <ul class="tags custom-list list-inline pull-left">
                                                    <li>${room.noOfBed} <ex:TranslateTag  lang="${lang}">Bed</ex:TranslateTag></li>
                                                    <li>${room.maxNoOfAdult} <ex:TranslateTag  lang="${lang}">Adult</ex:TranslateTag></li>
                                                    <li>${room.maxNoOfChildren} <ex:TranslateTag  lang="${lang}">Child</ex:TranslateTag></li>
                                                    </ul>
                                                </div>
                                                <div class="pull-right">
                                                    <span class="price">
                                                        from ${room.price}/<ex:TranslateTag  lang="${lang}">day</ex:TranslateTag>
                                                    </span>
                                                </div>
                                            </header>
                                        </div>
                                        <div class="room-about">
                                            <h5 class="title-section">  ${room.hotel.name} - ${room.roomNumber} - ${room.purpose}</h5><br/>
                                        <c:if test="${room.rating>=0}">
                                            <ex:TranslateTag  lang="${lang}">Rating</ex:TranslateTag>: ${room.rating}
                                        </c:if >

                                        <c:if test="${room.rating<0}">
                                            <ex:TranslateTag  lang="${lang}">No rating Yet</ex:TranslateTag>
                                        </c:if >
                                    </div>
                                    <div class="room-tabs">
                                        <ul class="nav nav-tabs">
                                            <li class="active"><a data-toggle="tab" href="#amenities"><ex:TranslateTag  lang="${lang}">Amenities</ex:TranslateTag></a></li>
                                            <li><a data-toggle="tab" href="#description"><ex:TranslateTag  lang="${lang}">Description</ex:TranslateTag></a></li>
                                       
                                                <li><a data-toggle="tab" href="#reviews"><ex:TranslateTag  lang="${lang}">Reviews</ex:TranslateTag></a></li>
                                                </ul>

                                                <div class="tab-content">
                                                    <div id="amenities" class="tab-pane fade in active">
                                                        <div class="listing-facitilities">
                                                            <div class="row">
                                                            <% int count = 0;
                                                                           if( room.getInRoomFacilities().size()>0){
                                                                               for (Facility f : room.getInRoomFacilities()) {
                                                                System.out.println("facility");
                                                                                   if (count % 3 == 0) {%>

                                                            <%
                                                                }
                                                            %>
                                                            <!--<div class="row">-->
                                                            <div class="col-md-3 col-sm-3">
                                                                <ul class="facilities-list custom-list">
                                                                    <li><%= f.getIconURL()%><span><%= f.getName()%></span></li>
                                                                </ul>
                                                            </div>
                                                            <!--</div>-->
                                                            <% //if (count % 3 == 0) {%>
                                                            <!--</div>-->
                                                            <% //}
                                                                count++;
                                                        
                                                        
                                                            }}else{ %>
                                                            No Facilities Added.
                                                            <%}%>

                                                            <!--                                                        <div class="col-md-4 col-sm-4">
                                                                                                                        <ul class="facilities-list custom-list">
                                                                                                                            <li><i class="fa fa-leaf"></i><span>AC</span></li>
                                                                                                                            <li><i class="fa fa-heart-o"></i><span>Hotbath</span></li>
                                                                                                                            <li><i class="fa fa-car"></i><span>Parking</span></li>
                                                                                                                        </ul>
                                                                                                                    </div>
                                                                                                                    <div class="col-md-4 col-sm-4">
                                                                                                                        <ul class="facilities-list custom-list">
                                                                                                                            <li><i class="fa fa-female"></i><span>Room service</span></li>
                                                                                                                            <li><i class="fa fa-key"></i><span>Free safe</span></li>
                                                                                                                            <li><i class="fa fa-desktop"></i><span>TV & Audio</span></li>
                                                                                                                        </ul>
                                                                                                                    </div>
                                                                                                                    <div class="col-md-4 col-sm-4">
                                                                                                                        <ul class="facilities-list custom-list">
                                                                                                                            <li><i class="fa fa-wifi"></i><span>WIFI access</span></li>
                                                                                                                            <li><i class="fa fa-clock-o"></i><span>24/7 Service</span></li>
                                                                                                                        </ul>
                                                                                                                    </div>-->
                                                        </div>
                                                    </div>
                                                </div>
                                                <div id="description" class="tab-pane fade">
                                                    <p>${room.description}</p>
                                                </div>
                                                <div id="images" class="tab-pane fade">
                                                    <div class="images-gallery">
                                                        <div class="row">
                                                            <div class="img-single col-md-3">
                                                                <img src="img/listing-1.jpg" alt="" class="img-responsive">
                                                            </div>
                                                            <div class="img-single col-md-3">
                                                                <img src="img/listing-1.jpg" alt="" class="img-responsive">
                                                            </div>
                                                            <div class="img-single col-md-3">
                                                                <img src="img/listing-1.jpg" alt="" class="img-responsive">
                                                            </div>
                                                            <div class="img-single col-md-3">
                                                                <img src="img/listing-1.jpg" alt="" class="img-responsive">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>


                                                <div id="reviews" class="tab-pane fade">
                                                    <ul class="reviews-list custom-list">

                                                        <!--                                                        <div class="thumbnail">
                                                                                                                    <img src="img/avatar.jpg" alt="">
                                                                                                                </div>-->
                                                        <c:forEach items="${bookingReviews}" var="bookingReview">
                                                            <li>
                                                                <div class="review-content">
                                                                    <header>
                                                                        <h5>${bookingReview.email}</h5>
                                                                        <!--                                                                <ul class="stars custom-list list-inline">
                                                                                                                                            <li><i class="fa fa-star"></i></li>
                                                                                                                                            <li><i class="fa fa-star"></i></li>
                                                                                                                                            <li><i class="fa fa-star"></i></li>
                                                                                                                                            <li><i class="fa fa-star"></i></li>
                                                                                                                                            <li><i class="fa fa-star"></i></li>
                                                                                                                                        </ul>-->
                                                                        ${bookingReview.startDate} - ${bookingReview.endDate}
                                                                    </header>
                                                                    <p>${bookingReview.message}</p>
                                                                    <br/>
                                                                </div>
                                                            </li>
                                                        </c:forEach>


                                                    </ul>
                                                </div>

                                            </div>
                                        </div>
                                        <%@include file="../partial/include-relatedList.jsp" %>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!-- End Room -->


                <%@include file="../partial/include-footer.jsp" %>

            </div>
            <!-- End Main-Wrapper -->

            <%@ include file="../partial/include-js.jsp" %>
        </body>

        <!-- Mirrored from 188.226.241.135/hotelpetra/homepage_v1/room.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 24 Mar 2016 07:45:10 GMT -->
    </html>