<%@ page import="model.Room" %>
<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<% Room room = (Room) request.getAttribute("room");%>
<!doctype html>
<html lang="en">

    <head>
        <title>Room</title>
        <%@ include file="../partial/include-head.jsp" %>
    </head>

    <body>

        <!-- Start Main-Wrapper -->
        <div id="main-wrapper">

            <%@ include file="../partial/include-header.jsp" %>

            <!-- Start Header-Section -->
            <section class="header-slider room">
                <div class="background-slider">
                    <%-- photo of the hotel --%>
                    <img src="img/header-slider-1.jpg" alt="">
                    <img src="img/header-slider-1.jpg" alt="">
                    <img src="img/header-slider-1.jpg" alt="">
                </div>
            </section>
            <!-- End Header-Section -->

            <!-- Start Room -->
            <section id="room">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="room-wrapper negative-margin">
                                <div class="room-content col-md-12">
                                    <div class="room-general">
                                        <img src="img/room-1.jpg" alt="" class="img-responsive">
                                        <header>
                                            <div class="pull-left">
                                                <h5 class="title">
                                                    <%= room.getRoomNumber()%> - <%= room.getPurpose()%>
                                                </h5>
                                                <ul class="tags pt-0 custom-list list-inline pull-left">
                                                    <li><%= room.getNoOfBed()%> Bed</li>
                                                    <li><%= room.getMaxNoOfAdult()%> Adult</li>
                                                    <li><%= room.getMaxNoOfChildren()%> Child</li>
                                                        <%--<li>Sea View</li>--%>
                                                </ul>
                                            </div>
                                            <div class="pull-right">
                                                <span class="price">
                                                    from <%= room.getPrice().toString()%>/day
                                                </span>
                                            </div>
                                        </header>
                                    </div>
                                    <%--<div class="room-about">--%>
                                    <%--<h5 class="title-section">About this room</h5>--%>
                                    <%--<p><%= room.getDescription()%></p>--%>
                                    <%--</div>--%>
                                    <div class="room-tabs">
                                        <ul class="nav nav-tabs">
                                            <li class="active"><a data-toggle="tab" href="#description">Description</a></li>
                                            <li><a data-toggle="tab" href="#amenities">Amenities</a></li>
                                                <%--<li><a data-toggle="tab" href="#images">Images</a></li>--%>
                                            <li><a data-toggle="tab" href="#reviews">Reviews</a></li>
                                        </ul>

                                        <div class="tab-content">
                                            <div id="description" class="tab-pane in fade active">
                                                <p><%= room.getDescription()%></p>
                                            </div>
                                            <div id="amenities" class="tab-pane fade">
                                                <div class="listing-facitilities">
                                                    <div class="row">
                                                        <div class="col-md-4 col-sm-4">
                                                            <ul class="facilities-list custom-list">
                                                                <li><i class="fa fa-leaf"></i><span>AC</span></li>
                                                                <li><i class="fa fa-heart-o"></i><span>Hotbath</span></li>
                                                                <li><i class="fa fa-car"></i><span>Parking</span></li>
                                                            </ul>
                                                        </div>
                                                        <div class="col-md-4 col-sm-4">
                                                            <ul class="facilities-list custom-list">
                                                                <li><i class="fa fa-female"></i><span>Room service</span></li>
                                                                <li><i class="fa fa-key"></i><span>Free safe</span></li>
                                                                <li><i class="fa fa-desktop"></i><span>TV & Audio</span></li>
                                                            </ul>
                                                        </div>
                                                        <div class="col-md-4 col-sm-4">
                                                            <ul class="facilities-list custom-list">
                                                                <li><i class="fa fa-wifi"></i><span>WIFI access</span></li>
                                                                <li><i class="fa fa-clock-o"></i><span>24/7 Service</span></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <%--<div id="images" class="tab-pane fade">--%>
                                            <%--<div class="images-gallery">--%>
                                            <%--<div class="row">--%>
                                            <%--<div class="img-single col-md-3">--%>
                                            <%--<img src="img/listing-1.jpg" alt="" class="img-responsive">--%>
                                            <%--</div>--%>
                                            <%--<div class="img-single col-md-3">--%>
                                            <%--<img src="img/listing-1.jpg" alt="" class="img-responsive">--%>
                                            <%--</div>--%>
                                            <%--<div class="img-single col-md-3">--%>
                                            <%--<img src="img/listing-1.jpg" alt="" class="img-responsive">--%>
                                            <%--</div>--%>
                                            <%--<div class="img-single col-md-3">--%>
                                            <%--<img src="img/listing-1.jpg" alt="" class="img-responsive">--%>
                                            <%--</div>--%>
                                            <%--</div>--%>
                                            <%--</div>--%>
                                            <%--</div>--%>
                                            <div id="reviews" class="tab-pane fade">
                                                <ul class="reviews-list custom-list">
                                                    <li>
                                                        <div class="thumbnail">
                                                            <img src="img/avatar.jpg" alt="">
                                                        </div>
                                                        <div class="review-content">
                                                            <header>
                                                                <h5>John Doe</h5>
                                                                <ul class="stars custom-list list-inline">
                                                                    <li><i class="fa fa-star"></i></li>
                                                                    <li><i class="fa fa-star"></i></li>
                                                                    <li><i class="fa fa-star"></i></li>
                                                                    <li><i class="fa fa-star"></i></li>
                                                                    <li><i class="fa fa-star"></i></li>
                                                                </ul>
                                                            </header>
                                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Facilis
                                                                quam nihil commodi aperiam quibusdam debitis, fugit reiciendis
                                                                dolore totam unde. Quod vitae porro dignissimos debitis
                                                                quibusdam possimus, molestias repudiandae assumenda.</p>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class=" sidebar sidebar-widget reservation">
                                            <h5 class="widget-title">Make a reservation</h5>
                                            <aside class="widget-content">
                                                <form action="reservation" class="default-form">
                                                    <span class="arrival calendar">
                                                        <input type="text" name="arrival" placeholder="Arrival" data-dateformat="m/d/y">
                                                        <i class="fa fa-calendar"></i>
                                                    </span>
                                                    <span class="departure calendar">
                                                        <input type="text" name="departure" placeholder="Departure" data-dateformat="m/d/y">
                                                        <i class="fa fa-calendar"></i>
                                                    </span>
                                                    <span class="adults select-box">
                                                        <select name="adults" data-placeholder="Adults">
                                                            <option>Adults</option>
                                                            <option value="1">1 adult</option>
                                                            <option value="2">2 adults</option>
                                                            <option value="3">3 adults</option>
                                                            <option value="4">4 adults</option>
                                                        </select>
                                                    </span>
                                                    <span class="children select-box">
                                                        <select name="children" data-placeholder="Children">
                                                            <option>Children</option>
                                                            <option value="1">1 children</option>
                                                            <option value="2">2 childrens</option>
                                                            <option value="3">3 childrens</option>
                                                            <option value="4">4 childrens</option>
                                                        </select>
                                                    </span>
                                                    <h5>Booking Cost: <span>$0.00</span></h5>
                                                    <button class="btn btn-transparent-gray">Make reservation</button>
                                                </form>
                                            </aside>
                                        </div>
                                    </div>


                                    <div class="room-related">
                                        <h5 class="title-section">Related Offers</h5>
                                        <div class="col-md-4">
                                            <div class="listing-room-grid">
                                                <div class="thumbnail">
                                                    <img src="img/listing-1.jpg" alt="" class="img-responsive">
                                                </div>
                                                <div class="listing-room-content">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <header>
                                                                <h5 class="title">
                                                                    <a href="#">King Suite</a>
                                                                </h5>
                                                                <ul class="tags custom-list list-inline">
                                                                    <li><a href="#">1 Bed</a></li>
                                                                    <li><a href="#">2 People</a></li>
                                                                    <li><a href="#">Sea View</a></li>
                                                                </ul>
                                                            </header>
                                                            <span class="price">
                                                                from $99/day
                                                            </span>
                                                            <button class="btn btn-transparent-gray">
                                                                Read More
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="listing-room-grid">
                                                <div class="thumbnail">
                                                    <img src="img/listing-1.jpg" alt="" class="img-responsive">
                                                </div>
                                                <div class="listing-room-content">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <header>
                                                                <h5 class="title">
                                                                    <a href="#">King Suite</a>
                                                                </h5>
                                                                <ul class="tags custom-list list-inline">
                                                                    <li><a href="#">1 Bed</a></li>
                                                                    <li><a href="#">2 People</a></li>
                                                                    <li><a href="#">Sea View</a></li>
                                                                </ul>
                                                            </header>
                                                            <span class="price">
                                                                from $99/day
                                                            </span>
                                                            <button class="btn btn-transparent-gray">
                                                                Read More
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="listing-room-grid">
                                                <div class="thumbnail">
                                                    <img src="img/listing-1.jpg" alt="" class="img-responsive">
                                                </div>
                                                <div class="listing-room-content">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <header>
                                                                <h5 class="title">
                                                                    <a href="#">King Suite</a>
                                                                </h5>
                                                                <ul class="tags custom-list list-inline">
                                                                    <li><a href="#">1 Bed</a></li>
                                                                    <li><a href="#">2 People</a></li>
                                                                    <li><a href="#">Sea View</a></li>
                                                                </ul>
                                                            </header>
                                                            <span class="price">
                                                                from $99/day
                                                            </span>
                                                            <button class="btn btn-transparent-gray">
                                                                Read More
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- End Room -->

            <%@include file="../partial/include-footer.jsp" %>

        </div>
        <!-- End Main-Wrapper -->

        <%@ include file="../partial/include-js.jsp" %>
    </body>

</html>