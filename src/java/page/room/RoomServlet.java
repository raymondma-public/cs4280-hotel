package page.room;

import model.Room;
import page.BaseServlet;
import utils.Is;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpSession;
import model.Booking;
import model.BookingReview;
import model.User;
import model.ViewHistory;

public class RoomServlet extends BaseServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String roomIdStr = request.getPathInfo().replaceFirst("/", "");
        Room room = null;
        if (Is.PositiveInteger(roomIdStr)) {
            Integer roomId = Integer.parseInt(roomIdStr);
            room = Room.getRoom(roomId);

            ArrayList<BookingReview> bookingReviews;
            try {
                bookingReviews = BookingReview.getBookingReviewListMessageFromDB("" + roomId);
                System.out.println("bookingReviews size:" + bookingReviews.size());
                request.setAttribute("bookingReviews", bookingReviews);
            } catch (SQLException ex) {

                Logger.getLogger(RoomServlet.class.getName()).log(Level.SEVERE, null, ex);
            }

            if (room == null) {
                request.setAttribute("error_message", "Room Not Found");
                dispatch("/WEB-INF/classes/page/error/404.jsp", request, response);
                return;
            }
        } else {
            request.setAttribute("error_message", roomIdStr + " is not a valid room id");
        }

        HttpSession session = request.getSession(false);
        User user = (User) session.getAttribute("user");
        if (user != null) {
            ViewHistory.addViewedHistoryToDB(user.getId(), "" + room.getId());
        }

        request.setAttribute("room", room);

        ArrayList<Room> suggestRoomList = Room.getSuggestedRoomList();
        request.setAttribute("suggestRoomList", suggestRoomList);
        ArrayList<Room> relatedRoomList = Room.getRelatedRoomList("" + room.getHotel().getId());
        request.setAttribute("relatedRoomList", relatedRoomList);

        dispatch("/WEB-INF/classes/page/room/room2.jsp", request, response);
//        dispatch("room.jsp", request, response);

    }

}
