<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="ex" uri="/WEB-INF/custom.tld"%>

<%@page import="model.Facility" %>
<%@page import="java.util.ArrayList" %>
<%@page import="model.Room" %>
<%@page import="model.Price" %>
<%@page contentType="text/html" pageEncoding="UTF-8" %>

<!doctype html>
<html lang="en">

    <head>
        <title><ex:TranslateTag  lang="${lang}">User Detail</ex:TranslateTag></title>
        <%@ include file="../partial/include-head.jsp" %>
    </head>

    <body>

        <!-- Start Main-Wrapper -->
        <div id="main-wrapper">

            <%@ include file="../partial/include-header.jsp" %>

            <!-- Start Header-Section -->
            <section class="header-section listing">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <h3 class="title-section pull-left">
                                <ex:TranslateTag  lang="${lang}">User Detail</ex:TranslateTag>
                            </h3>
                            <ul class="breadcrumbs custom-list list-inline pull-right">
                                <li><ex:TranslateTag  lang="${lang}">Home</ex:TranslateTag></li>
                                <li><ex:TranslateTag  lang="${lang}">User Detail</ex:TranslateTag></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </section>
            <!-- End Header-Section -->

            <section id="listing">
                <div class="container">
                    <!--<form class="row default-form" method="get">-->
                    <%@include file="../partial/include-userPageMenu.jsp" %>

                    <div class="listing-content col-md-9">

                        <div class="listing-pagination">
                            <h5 class="title "><ex:TranslateTag  lang="${lang}">User Detail</ex:TranslateTag></h5>


                            <c:if test="${not empty user.iconImageURL}">
                                <p> <br><img src="${user.iconImageURL}" width="100px" height="100px"></p>
                                </c:if>
                                <c:if test="${not empty user.userName}">
                                <p><ex:TranslateTag  lang="${lang}">User Name</ex:TranslateTag>: ${user.userName}
                                </p>
                            </c:if>
                            <c:if test="${not empty user.email}">
                                <p>  Email: ${user.email}
                                </p>
                            </c:if>


                            <c:if test="${not empty user.phone}">
                                <p> <ex:TranslateTag  lang="${lang}">Phone</ex:TranslateTag>: ${user.phone}</p>
                            </c:if>
                            <c:if test="${not empty user.userType}">
                                <p> <ex:TranslateTag  lang="${lang}">User Type</ex:TranslateTag>: ${user.userType}</p>
                            </c:if>

                            <a href="editUserPage" class="btn btn-primary pull-right"><ex:TranslateTag  lang="${lang}">Edit</ex:TranslateTag></a>
                        </div>
                    
                    </div>
                    <!--</form>-->
                </div>
            </section>

            <%@include file="../partial/include-footer.jsp" %>

        </div>
        <!-- End Main-Wrapper -->

        <%@ include file="../partial/include-js.jsp" %>

    </body>

</html>