<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="ex" uri="/WEB-INF/custom.tld"%>

<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<!doctype html>
<html lang="en">

    <head>
        <title>Forbidden - Hotel Petra</title>

        <%@ include file="../partial/include-head.jsp" %>
    </head>

    <body>

        <!-- Start Main-Wrapper -->
        <div id="main-wrapper">

            <%@ include file="../partial/include-header.jsp" %>

            <!-- Start Header-Section -->
            <section class="header-section error">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <h3 class="title-section pull-left">
                                403
                            </h3>
                        </div>
                    </div>
                </div>
            </section>
            <!-- End Header-Section -->

            <!-- Start Team -->
            <section class="error">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <h1 style="font-size: 9em;">403</h1>
                            <h3>Oops! You don't have enough permission.</h3>
                        </div>
                    </div>
                </div>
            </section>
            <!-- End Team -->

            <%@ include file="../partial/include-footer.jsp" %>

        </div>
        <!-- End Main-Wrapper -->

        <%@ include file="../partial/include-js.jsp" %>

    </body>

</html>