package page.errorPage;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import page.BaseServlet;

public class ErrorServlet extends BaseServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
      
        String statusCode = request.getRequestURI().replace(request.getContextPath(), "").replaceFirst("/", "");
        List<String> statusCodeArr = Arrays.asList("404", "403", "401");
        if (!statusCodeArr.contains(statusCode)) {
            statusCode = "404";
        }
//        response.setStatus(Integer.parseInt(statusCode));
        dispatch("/WEB-INF/classes/page/errorPage/" + statusCode + ".jsp", request, response);
    }

}
