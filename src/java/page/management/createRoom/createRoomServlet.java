package page.management.createRoom;

import com.oreilly.servlet.MultipartRequest;
import com.oreilly.servlet.multipart.FileRenamePolicy;
import exception.ForbiddenException;
import exception.UnauthorizedException;
import model.*;
import org.apache.commons.validator.ValidatorException;
import page.BaseServlet;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.net.URLEncoder;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;

public class createRoomServlet extends BaseServlet {

    private static String basePath = "management";

    @Override
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, UnauthorizedException, ForbiddenException {
        // do nothing
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        super.doGet(request, response);
        try {
            checkUserPermission("manager", request);

            request.setAttribute("basePath", basePath);

            ArrayList<Facility> facilities = Facility.getFacilities();

            request.setAttribute("facilities", facilities);

            dispatch("/WEB-INF/classes/page/management/createRoom/createRoom.jsp", request, response);
        } catch (UnauthorizedException e) {
            redirectError(response, 401);
        } catch (ForbiddenException e) {
            redirectError(response, 403);
        } catch (SQLException e) {
            Logger.getLogger(createRoomServlet.class.getName()).log(Level.WARNING, null, e);
            redirectError(response, 500);
        }

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        super.doPost(request, response);
        try {
            checkUserPermission("manager", request);
            final User user = (User) request.getAttribute("user");

            Hotel hotel = user.getManagingHotel();
            int hotelId = hotel.getId();

            ServletContext context = request.getServletContext();
            String filePath = context.getInitParameter("file-upload");
            FileRenamePolicy policy = new FileRenamePolicy() {
                @Override
                public File rename(File file) {
                    return new File(file.getParentFile(), user.getId() + "_" + Calendar.getInstance().getTimeInMillis() + "_" + file.getName());
                }
            };
            MultipartRequest req = new MultipartRequest(request, filePath, 3000000, policy);

            String purpose = req.getParameter("purpose");
            String roomNumber = req.getParameter("roomNumber");
            String description = req.getParameter("description");
            String noOfBed = req.getParameter("noOfBed");
            String maxNoOfAdult = req.getParameter("maxNoOfAdult");
            String maxNoOfChildren = req.getParameter("maxNoOfChildren");
            String price = req.getParameter("price");
            String[] facilities = req.getParameterValues("facilities");
            File icon = req.getFile("icon");
            String iconUrl = "";
            if (icon != null)
                iconUrl = "http://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath() + "/uploads/" + icon.getName();

            int priceId = Price.addPrice(true, price);
            Room newRoom = Room.addRoom(priceId, purpose, roomNumber, description, noOfBed, maxNoOfAdult, maxNoOfChildren, hotelId, iconUrl);

            Facility.removeFacilities("" + newRoom.getId());
            if (facilities != null) {
                for (String fString : facilities) {
                    Facility.addFacility("" + newRoom.getId(), fString);
                }
            }
            response.setStatus(HttpServletResponse.SC_ACCEPTED);
            request.setAttribute("success_message", "Room Added");
            dispatch("/WEB-INF/classes/page/management/createRoom/createRoom.jsp", request, response);

        } catch (UnauthorizedException e) {
            redirectError(response, 401);
        } catch (ForbiddenException e) {
            redirectError(response, 403);
        } catch (ValidatorException e) {
            String path = request.getContextPath() + "/" + basePath + "/createRoom?error_message=" + URLEncoder.encode(e.getMessage(), "UTF-8");
            response.sendRedirect(path);
        } catch (SQLException e) {
            String path = request.getContextPath() + "/" + basePath + "/createRoom?error_message=" + URLEncoder.encode("SQL: " + e.getMessage(), "UTF-8");
            response.sendRedirect(path);
            Logger.getLogger(createRoomServlet.class.getName()).log(Level.SEVERE, null, e);
        }

    }
}
