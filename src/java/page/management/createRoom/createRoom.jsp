<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="ex" uri="/WEB-INF/custom.tld"%>
<%@ page contentType="text/html" pageEncoding="UTF-8" %>

<!doctype html>
<html lang="en">

<head>
    <title><ex:TranslateTag  lang="${lang}">Create Room</ex:TranslateTag></title>
    <%@ include file="../../partial/include-head.jsp" %>
    <style>
        <%@ include file="createRoom.css" %>
    </style>
</head>

<body>

<!-- Start Main-Wrapper -->
<div id="main-wrapper">

    <%@ include file="../../partial/include-header.jsp" %>

    <!-- Start Header-Section -->
    <section class="header-section listing">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h3 class="title-section pull-left">
                        <ex:TranslateTag  lang="${lang}">Create Room</ex:TranslateTag>
                    </h3>
                    <ul class="breadcrumbs custom-list list-inline pull-right">
                        <li><ex:TranslateTag  lang="${lang}">Home</ex:TranslateTag></li>
                        <li><ex:TranslateTag  lang="${lang}">Management</ex:TranslateTag></li>
                        <li><ex:TranslateTag  lang="${lang}">Create Room</ex:TranslateTag></li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <!-- End Header-Section -->

    <section id="listing">
        <div class="container">
            <div class="sidebar col-md-3">
                <div class="fleets-filters toggle-container">
                    <div class="special-offers sidebar">
                        <a href="${basePath}/createRoom"><h5 class="title-section"><ex:TranslateTag  lang="${lang}">Create Room</ex:TranslateTag></h5></a>
                    </div>
                    <div class="special-offers sidebar">
                        <a href="${basePath}/manageRoomList"><h5 class="title-section"><ex:TranslateTag  lang="${lang}">Manage Room List</ex:TranslateTag></h5></a>
                    </div>
                </div>
            </div>

            <div class="listing-content col-md-9">

                <div class="listing-pagination">
                    <form class="form-horizontal" method="POST" enctype="multipart/form-data">
                        <div class="form-group">
                            <label for="image" class="col-sm-2 control-label"><ex:TranslateTag  lang="${lang}">Room Image</ex:TranslateTag></label>
                            <div class="col-sm-10">
                                <input type="file" name="icon" class="" id="image" accept="image">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="purpose" class="col-sm-2 control-label"><ex:TranslateTag  lang="${lang}">Purpose</ex:TranslateTag></label>
                            <div class="col-sm-10">
                                <input type="text" name="purpose" class="form-control" id="purpose" placeholder="Purpose">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="roomNumber" class="col-sm-2 control-label"><ex:TranslateTag  lang="${lang}">Room Number</ex:TranslateTag></label>
                            <div class="col-sm-10">
                                <input type="text" name="roomNumber" class="form-control" id="roomNumber" placeholder="Room Number">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="description" class="col-sm-2 control-label"><ex:TranslateTag  lang="${lang}">Description</ex:TranslateTag></label>
                            <div class="col-sm-10">
                                <textarea name="description" class="form-control" id="description" placeholder="Description"></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="noOfBed" class="col-sm-2 control-label"><ex:TranslateTag  lang="${lang}">Number of bed</ex:TranslateTag></label>
                            <div class="col-sm-10">
                                <input type="number" name="noOfBed" class="form-control" id="noOfBed" placeholder="Number of bed">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="maxNoOfAdult" class="col-sm-2 control-label"><ex:TranslateTag  lang="${lang}">Number of Adult</ex:TranslateTag></label>
                            <div class="col-sm-10">
                                <input type="number" name="maxNoOfAdult" class="form-control" id="maxNoOfAdult" placeholder="Number of Adult">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="maxNoOfChildren" class="col-sm-2 control-label"><ex:TranslateTag  lang="${lang}">Number of Children</ex:TranslateTag></label>
                            <div class="col-sm-10">
                                <input type="number" name="maxNoOfChildren" class="form-control" id="maxNoOfChildren" placeholder="Number of Children">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="price" class="col-sm-2 control-label"><ex:TranslateTag  lang="${lang}">Price</ex:TranslateTag></label>
                            <div class="col-sm-4">
                                <input type="number" name="price" class="form-control" id="price" placeholder="Price">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label"><ex:TranslateTag  lang="${lang}">Facilities</ex:TranslateTag></label>
                            <div class="col-sm-10">
                                <c:forEach var="facility" items="${facilities}">
                                <label class="control-label">
                                    <input type="checkbox" name="facilities" value="${facility.name}">${facility.name}&nbsp;&nbsp;&nbsp;
                                </label>
                                </c:forEach>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                <button type="submit" class="btn btn-default">Create</button>
                            </div>
                        </div>
                    </form>
                </div>

            </div>
            <!--</form>-->
        </div>
    </section>

    <%@include file="../../partial/include-footer.jsp" %>

</div>
<!-- End Main-Wrapper -->

<%@ include file="../../partial/include-js.jsp" %>
<script>
    <%@ include file="createRoom.js" %>
</script>

</body>

</html>