/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package page.management.manageHotelList;

import exception.ForbiddenException;
import exception.UnauthorizedException;
import exception.UserNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Hotel;
import model.Room;
import model.User;
import page.BaseServlet;
import page.manageRoomList.ManageRoomListServlet;

/**
 *
 * @author RaymondMa
 */
public class ManageHotelListServlet extends BaseServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            this.checkUserLogon(request);
            this.checkUserPermission("chiefManager", request);
            User user = this.getCurrentUser(request);

//            Hotel managingHotel = user.getManagingHotel();
//            ArrayList<Room> rooms;
            ArrayList<Hotel> hotels = Hotel.getHotelListFromDB();
            request.setAttribute("basePath", request.getContextPath() + "/management");
            request.setAttribute("hotels", hotels);
//            request.setAttribute("basePath", "management");
//            rooms = Room.getRoomListForManagement("" + managingHotel.getId());
//            request.setAttribute("hotel", managingHotel);
//            request.setAttribute("rooms", rooms);
            BaseServlet.dispatch("/WEB-INF/classes/page/management/manageHotelList/manageHotelList.jsp", request, response);

        } catch (UnauthorizedException ex) {
            Logger.getLogger(ManageRoomListServlet.class.getName()).log(Level.SEVERE, null, ex);
            this.redirectError(response, 401);
            return;
        } catch (UserNotFoundException ex) {
            Logger.getLogger(ManageRoomListServlet.class.getName()).log(Level.SEVERE, null, ex);
            this.redirectError(response, 401);
            return;
        } catch (ForbiddenException ex) {
            Logger.getLogger(ManageRoomListServlet.class.getName()).log(Level.SEVERE, null, ex);
            this.redirectError(response, 403);
            return;
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
