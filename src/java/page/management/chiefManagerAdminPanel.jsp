<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="ex" uri="/WEB-INF/custom.tld"%>

<%@ page contentType="text/html" pageEncoding="UTF-8" %>

<!doctype html>
<html lang="en">

    <head>
        <title><ex:TranslateTag  lang="${lang}">Management</ex:TranslateTag></title>
        <%@ include file="../partial/include-head.jsp" %>
    </head>

    <body>

        <!-- Start Main-Wrapper -->
        <div id="main-wrapper">

            <%@ include file="../partial/include-header.jsp" %>

            <!-- Start Header-Section -->
            <section class="header-section listing">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <h3 class="title-section pull-left">
                                <ex:TranslateTag  lang="${lang}">Management</ex:TranslateTag>
                            </h3>
                            <ul class="breadcrumbs custom-list list-inline pull-right">
                                <li><ex:TranslateTag  lang="${lang}">Home</ex:TranslateTag></li>
                                <li><ex:TranslateTag  lang="${lang}">Management</ex:TranslateTag></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </section>
            <!-- End Header-Section -->

            <section id="listing">
                <div class="container">
                    <div class="sidebar col-md-12">
                        <div class="fleets-filters toggle-container">
                            <div class="special-offers sidebar">
                                <a href="${basePath}/userList"><h5 class="title-section"><ex:TranslateTag  lang="${lang}">User List</ex:TranslateTag></h5></a>
                            </div>
                            <div class="special-offers sidebar">
                                <a href="${basePath}/createUser"><h5 class="title-section"><ex:TranslateTag  lang="${lang}">Create User</ex:TranslateTag></h5></a>
                            </div>

                            <div class="special-offers sidebar">
                                <a href="${basePath}/createHotel"><h5 class="title-section"><ex:TranslateTag  lang="${lang}">Create Hotel</ex:TranslateTag></h5></a>
                            </div>
                            <div class="special-offers sidebar">
                                <a href="${basePath}/manageHotelList"><h5 class="title-section"><ex:TranslateTag  lang="${lang}">Manage Hotel List</ex:TranslateTag></h5></a>
                            </div>
                        </div>

                    </div>
                </div>
            </section>

            <%@include file="../partial/include-footer.jsp" %>

        </div>
        <!-- End Main-Wrapper -->

        <%@ include file="../partial/include-js.jsp" %>

    </body>

</html>