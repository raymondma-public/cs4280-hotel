/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package page.management.createHotel;

import exception.ForbiddenException;
import exception.UnauthorizedException;
import exception.UserNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Facility;
import model.Hotel;
import model.Price;
import model.Room;
import model.User;
import org.apache.commons.validator.ValidatorException;
import page.BaseServlet;
import page.management.createRoom.createRoomServlet;

/**
 *
 * @author RaymondMa
 */
public class CreateHotelServlet extends BaseServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            checkUserPermission("chiefManager", request);

//            request.setAttribute("basePath", "management");
//            ArrayList<Facility> facilities = Facility.getFacilities();
//            request.setAttribute("facilities", facilities);
            request.setAttribute("basePath", "management");
            dispatch("/WEB-INF/classes/page/management/createHotel/createHotel.jsp", request, response);
        } catch (UnauthorizedException e) {
            redirectError(response, 401);
        } catch (ForbiddenException e) {
            redirectError(response, 403);
        }
//        catch (SQLException e) {
//            Logger.getLogger(createRoomServlet.class.getName()).log(Level.WARNING, null, e);
//            redirectError(response, 500);
//        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        try {
            checkUserPermission("chiefManager", request);
            User user = this.getCurrentUser(request);

            Hotel hotel = user.getManagingHotel();
//            if(hotel){
//                
//            }
//            int hotelId = hotel.getId();
//
//            String purpose = request.getParameter("purpose");
//            String roomNumber = request.getParameter("roomNumber");
//            String description = request.getParameter("description");
//            String noOfBed = request.getParameter("noOfBed");
//            String maxNoOfAdult = request.getParameter("maxNoOfAdult");
//            String maxNoOfChildren = request.getParameter("maxNoOfChildren");
//            String priceForChild = request.getParameter("priceForChild");
//            String priceForAdult = request.getParameter("priceForAdult");
//            String[] facilities = request.getParameterValues("facilities");
//            String icon = request.getParameter("icon");
//
//            int priceId = Price.addPrice(true, priceForAdult);
//
//            Room newRoom = Room.addRoom(priceId, purpose, roomNumber, description, noOfBed, maxNoOfAdult, maxNoOfChildren, hotelId);
            String hotelType = request.getParameter("hotelType");
            String name = request.getParameter("name");
            String phone = request.getParameter("phone");
            String description = request.getParameter("description");
            String latitudeString = request.getParameter("latitude");
            String longitudeString = request.getParameter("longitude");
            double latitude = Double.parseDouble(latitudeString);
            double longitude = Double.parseDouble(longitudeString);

            Hotel.addHotel(hotelType, name, phone, description, latitude, longitude);

//            Facility.removeFacilities("" + newRoom.getId());
//            for (String fString : facilities) {
//                Facility.addFacility("" + newRoom.getId(), fString);
//            }
            response.setStatus(HttpServletResponse.SC_ACCEPTED);
//            request.setAttribute("success_message", "Room Added");

            int hotelId = Hotel.getLastModifiedId();
            response.sendRedirect(request.getContextPath() + "/hotelDetail?hotelId=" + hotelId);

        } catch (UnauthorizedException e) {
            redirectError(response, 401);
        } catch (ForbiddenException e) {
            redirectError(response, 403);
//        } catch (ValidatorException e) {
//            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
//            request.setAttribute("error_message", e.getMessage());
//            dispatch("/WEB-INF/classes/page/management/createRoom/createRoom.jsp", request, response);
        } catch (SQLException e) {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            Logger.getLogger(createRoomServlet.class.getName()).log(Level.SEVERE, null, e);
            request.setAttribute("error_message", "SQL: " + e.getMessage());
            dispatch("/WEB-INF/classes/page/management/createHotel/createHotel.jsp", request, response);
        } catch (UserNotFoundException ex) {
            Logger.getLogger(CreateHotelServlet.class.getName()).log(Level.SEVERE, null, ex);
            redirectError(response, 401);
        }

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
