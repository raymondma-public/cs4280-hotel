<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="ex" uri="/WEB-INF/custom.tld"%>

<%@ page contentType="text/html" pageEncoding="UTF-8" %>

<!doctype html>
<html lang="en">

    <head>
        <title><ex:TranslateTag  lang="${lang}">Create Hotel</ex:TranslateTag></title>
        <%@ include file="../../partial/include-head.jsp" %>
        <style>
            <%@ include file="createHotel.css" %>
        </style>
    </head>

    <body>

        <!-- Start Main-Wrapper -->
        <div id="main-wrapper">

            <%@ include file="../../partial/include-header.jsp" %>

            <!-- Start Header-Section -->
            <section class="header-section listing">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <h3 class="title-section pull-left">
                                <ex:TranslateTag  lang="${lang}">Create Hotel</ex:TranslateTag>
                            </h3>
                            <ul class="breadcrumbs custom-list list-inline pull-right">
                                <li><ex:TranslateTag  lang="${lang}">Home</ex:TranslateTag></li>
                                <li><ex:TranslateTag  lang="${lang}">Management</ex:TranslateTag></li>
                                <li><ex:TranslateTag  lang="${lang}">Create Hotel</ex:TranslateTag></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </section>
            <!-- End Header-Section -->

            <section id="listing">
                <div class="container">
                    <div class="sidebar col-md-3">
                        <div class="fleets-filters toggle-container">
                            <div class="special-offers sidebar">
                                <a href="${basePath}/userList"><h5 class="title-section"><ex:TranslateTag  lang="${lang}">User List</ex:TranslateTag></h5></a>
                            </div>
                            <div class="special-offers sidebar">
                                <a href="${basePath}/createUser"><h5 class="title-section"><ex:TranslateTag  lang="${lang}">Create User</ex:TranslateTag></h5></a>
                            </div>

                            <div class="special-offers sidebar">
                                <a href="${basePath}/createHotel"><h5 class="title-section"><ex:TranslateTag  lang="${lang}">Create Hotel</ex:TranslateTag></h5></a>
                            </div>
                            <div class="special-offers sidebar">
                                <a href="${basePath}/manageHotelList"><h5 class="title-section"><ex:TranslateTag  lang="${lang}">Manage Hotel List</ex:TranslateTag></h5></a>
                            </div>
                        </div>

                    </div>

                    <div class="listing-content col-md-9">

                        <div class="listing-pagination">
                            <form action="management/createHotel" class="form-horizontal" method="POST">
                                <div class="form-group">
                                    <label for="hotelType" class="col-sm-2 control-label"><ex:TranslateTag  lang="${lang}">Hotel Type</ex:TranslateTag></label>
                                    <div class="col-sm-10">
                                        <input type="text" name="hotelType" class="form-control" id="purpose" placeholder="Hotel Type" required>
                                    </div>  
                                </div>
                                <div class="form-group">
                                    <label for="name" class="col-sm-2 control-label"><ex:TranslateTag  lang="${lang}">Hotel Name</ex:TranslateTag></label>
                                    <div class="col-sm-10">
                                        <input type="text" name="name" class="form-control" id="purpose" placeholder="Hotel Name" required>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="phone" class="col-sm-2 control-label"><ex:TranslateTag  lang="${lang}">Phone</ex:TranslateTag></label>
                                    <div class="col-sm-10">
                                        <input type="text" name="phone" class="form-control" id="purpose" placeholder="Phone" required>
                                    </div>
                                </div>


                                <div class="form-group">
                                    <label for="description" class="col-sm-2 control-label"><ex:TranslateTag  lang="${lang}">Description</ex:TranslateTag></label>
                                    <div class="col-sm-10">
                                        <textarea name="description" class="form-control" id="description" placeholder="Description" required></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <a href="http://mondeca.com/index.php/en/any-place-en" target="_blank"><ex:TranslateTag  lang="${lang}">Get your hotel location here</ex:TranslateTag></a>
                                </div>
                                <div class="form-group">
                                    <label for="latitude" class="col-sm-2 control-label"><ex:TranslateTag  lang="${lang}">Latitude</ex:TranslateTag></label>
                                    <div class="col-sm-10">
                                        <input type="text" name="latitude" class="form-control" id="purpose" placeholder="Latitude" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="longitude" class="col-sm-2 control-label"><ex:TranslateTag  lang="${lang}">Longitude</ex:TranslateTag></label>
                                    <div class="col-sm-10">
                                        <input type="text" name="longitude" class="form-control" id="purpose" placeholder="Longitude" required>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-sm-offset-2 col-sm-10">
                                        <button type="submit" class="btn btn-default"><ex:TranslateTag  lang="${lang}">Create</ex:TranslateTag></button>
                                    </div>
                                </div>

                            </form>
                        </div>

                    </div>
                    <!--</form>-->
                </div>
            </section>

            <%@include file="../../partial/include-footer.jsp" %>

        </div>
        <!-- End Main-Wrapper -->

        <%@ include file="../../partial/include-js.jsp" %>
        <script>
            <%@ include file="createHotel.js" %>
        </script>

    </body>

</html>