package page.management.userList;

import exception.ForbiddenException;
import exception.UnauthorizedException;
import model.Hotel;
import model.User;
import page.BaseServlet;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

public class UserListServlet extends BaseServlet {

    @Override
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, UnauthorizedException, ForbiddenException {
        checkUserPermission("chiefManager", request);
        request.setAttribute("roleList", User.getRoleList());
        request.setAttribute("hotelList", Hotel.getHotelListFromDB());

        request.setAttribute("basePath", "management");

        try {
            ArrayList<User> userList = User.getUserList();
            request.setAttribute("userList", userList);

            dispatch("/WEB-INF/classes/page/management/userList/userList.jsp", request, response);
        } catch (SQLException e) {
            request.setAttribute("error_message", e.getMessage());
            dispatch(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "/WEB-INF/classes/page/management/userList/userList.jsp", request, response);
        }

    }
}
