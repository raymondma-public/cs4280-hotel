<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="ex" uri="/WEB-INF/custom.tld"%>

<%@ page contentType="text/html" pageEncoding="UTF-8" %>

<!doctype html>
<html lang="en">

    <head>
        <title><ex:TranslateTag  lang="${lang}">User List</ex:TranslateTag></title>
        <%@ include file="../../partial/include-head.jsp" %>
        <style><%@ include file="userList.css"%></style>
    </head>

    <body>

        <!-- Start Main-Wrapper -->
        <div id="main-wrapper">

            <%@ include file="../../partial/include-header.jsp" %>

            <!-- Start Header-Section -->
            <section class="header-section listing">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <h3 class="title-section pull-left">
                                <ex:TranslateTag  lang="${lang}">User List</ex:TranslateTag>
                                </h3>
                                <ul class="breadcrumbs custom-list list-inline pull-right">
                                    <li><ex:TranslateTag  lang="${lang}">Home</ex:TranslateTag></li>
                                <li><ex:TranslateTag  lang="${lang}">Management</ex:TranslateTag></li>
                                <li><ex:TranslateTag  lang="${lang}">User List</ex:TranslateTag></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </section>
                <!-- End Header-Section -->

                <section id="listing">
                    <div class="container">
                        <div class="sidebar col-md-3">
                            <div class="fleets-filters toggle-container">
                                <div class="special-offers sidebar">
                                    <a href="${basePath}/userList"><h5 class="title-section"><ex:TranslateTag  lang="${lang}">User List</ex:TranslateTag></h5></a>
                                </div>
                                <div class="special-offers sidebar">
                                    <a href="${basePath}/createUser"><h5 class="title-section"><ex:TranslateTag  lang="${lang}">Create User</ex:TranslateTag></h5></a>
                                </div>

                                <div class="special-offers sidebar">
                                    <a href="${basePath}/createHotel"><h5 class="title-section"><ex:TranslateTag  lang="${lang}">Create Hotel</ex:TranslateTag></h5></a>
                                </div>
                                <div class="special-offers sidebar">
                                    <a href="${basePath}/manageHotelList"><h5 class="title-section"><ex:TranslateTag  lang="${lang}">Manage Hotel List</ex:TranslateTag></h5></a>
                                </div>
                            </div>

                        </div>

                        <div class="listing-content col-md-9">

                            <div class="listing-pagination">
                                <table class="table table-hover userListTable editableTable">
                                    <thread>
                                        <th><ex:TranslateTag  lang="${lang}">Profile Picture</ex:TranslateTag></th>
                                        <th><ex:TranslateTag  lang="${lang}">User Name</ex:TranslateTag></th>
                                        <th>Email</th>
                                        <th><ex:TranslateTag  lang="${lang}">Phone</ex:TranslateTag></th>
                                        <th><ex:TranslateTag  lang="${lang}">Type</ex:TranslateTag></th>
                                        <th><ex:TranslateTag  lang="${lang}">Action</ex:TranslateTag></th>
                                    </thread>
                                    <tbody>
                                    <c:forEach items="${userList}" var="user">
                                        <tr data-userId="${user.id}">
                                            <td>
                                                <c:choose>
                                                    <c:when test="${empty user.iconImageURL}">
                                                        <%-- TODO custom tag --%>
                                                        <img src="${user.defaultIconImageURL}" width="100px" height="100px">
                                                    </c:when>
                                                    <c:otherwise>
                                                        <img src="${user.iconImageURL}" width="100px" height="100px">
                                                    </c:otherwise>
                                                </c:choose>
                                            </td>
                                            <td class="editable" data-name="userName">${user.userName}</td>
                                            <td data-name="email">${user.email}</td>
                                            <td class="editable" data-name="phone">${user.phone}</td>
                                            <td>
                                                <select class="form-control input-sm editable" data-name="userType">
                                                    <c:forEach items="${roleList}" var="role">
                                                        <option value="${role}"<c:if test="${role == user.userType}"> selected</c:if>>${role}</option>
                                                    </c:forEach>
                                                </select>
                                                <select data-name="managingHotelId" class="form-control input-sm editable hidden" placeholder="Managing Hotel" disabled>
                                                    <option class="hidden">Managing Hotel</option>
                                                    <c:forEach items="${hotelList}" var="hotel">
                                                        <option value="${hotel.id}" <c:if test="${user.managingHotel.id==hotel.id}"> selected</c:if>>${hotel.name}</option>
                                                    </c:forEach>
                                                </select>

                                            </td>
                                            <td class="action text-center">
                                                <button class="btn btn-sm btn-danger delete"><ex:TranslateTag  lang="${lang}">Delete</ex:TranslateTag></button>
                                                <br>
                                                <button class="btn btn-sm btn-default update"><ex:TranslateTag  lang="${lang}">Update</ex:TranslateTag></button>
                                            </td>
                                        </tr>
                                    </c:forEach>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!--</form>-->
                </div>
            </section>

            <%@include file="../../partial/include-footer.jsp" %>

        </div>
        <!-- End Main-Wrapper -->

        <%@ include file="../../partial/include-js.jsp" %>
        <script><%@ include file="userList.js"%></script>

    </body>

</html>