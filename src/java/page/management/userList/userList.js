$(function () {
    $('table.editableTable').each(function () {
        $(this).find('td.editable').each(function () {
            var $td = $(this);
            var $tr = $td.parents('tr');
            $td.attr('contenteditable', true);
            $td.css({'max-width': $td.width()});

            $td.on('input', function () {
                $tr.addClass('edited');
                $td.addClass('edited');
            });
        });
        $(this).find('select.editable').each(function () {
            var $self = $(this);
            var $tr = $(this).parents('tr');
           $(this).on('change', function () {
               $tr.addClass('edited');
               $self.addClass('edited');
           })
        });
    });

    $(document).on('click', '.userListTable .action button.update', function () {
        var $tr = $(this).parents('tr.edited');
        var userId = $tr.data('userid');
        var data = {};
        var $edited = $tr.find('.edited');
        $edited.each(function () {
            var $input, value;
            if ($(this).is('[data-name]')) {
                $input = $(this);
                value = $input.val() || $input.text();
            } else {
                $input = $(this).find('input[data-name], textarea[data-name], select[data-name]');
                value = $input.val();
            }
            var name = $input.data('name');
            data[name] = value;
        });
        $.ajax({
                method: 'PUT',
                url: 'editUser?userId=' + userId,
                data: JSON.stringify(data),
                dataType: 'json',
                contentType: 'application/json'
            })
            .done(function (data, textStatus, jqXHR) {
                if (data.success) {
                    Utilities.notify("awesome ok", "Success", "Updated", "", "fa-check-circle");
                    $tr.removeClass('edited');
                    $edited.removeClass('edited');
                } else {
                    Utilities.notify("awesome error", textStatus, data.error_message, "Please try again later.", "fa-exclamation-triangle");
                }
            })
            .fail(function (jqXHR, textStatus, errorThrown) {
                var data = jqXHR.responseJSON || {};
                var errorMsg = data.error_message || errorThrown;
                var message = jqXHR.status + " " + errorMsg;
                Utilities.notify("awesome error", textStatus, message, "Please try again later.", "fa-exclamation-triangle");
            });
    });

    $(document).on('click', '.userListTable .action button.delete', function () {
        var confirmed = confirm("Are you sure to delete the user?");
        if (!confirmed) return;
        var $tr = $(this).parents('tr');
        var userId = $tr.data('userid');

        $.ajax({
                method: 'DELETE',
                url: 'editUser?userId=' + userId,
                dataType: 'json'
            })
            .done(function (data, textStatus, jqXHR) {
                if (data.success) {
                    Utilities.notify("awesome ok", "Success", "Deleted", "", "fa-check-circle");
                    $tr.remove();
                } else {
                    Utilities.notify("awesome error", textStatus, data.error_message, "Please try again later.", "fa-exclamation-triangle");
                }
            })
            .fail(function (jqXHR, textStatus, errorThrown) {
                var data = jqXHR.responseJSON || {};
                var errorMsg = data.error_message || errorThrown;
                var message = jqXHR.status + " " + errorMsg;
                Utilities.notify("awesome error", textStatus, message, "Please try again later.", "fa-exclamation-triangle");
            });
    });

    $('[data-name="userType"]').on('change', handleUserType).each(handleUserType);

    function handleUserType() {
        if($(this).val() === 'manager') {
            $(this).next('[data-name="managingHotelId"]').removeClass('hidden').removeAttr('disabled');
        } else {
            $(this).next('[data-name="managingHotelId"]').addClass('hidden').attr('disabled', 'disabled');
        }
    }

});