package page.management;

import exception.ForbiddenException;
import exception.UnauthorizedException;
import model.User;
import page.BaseServlet;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class ManagementServlet extends BaseServlet{

    @Override
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, UnauthorizedException, ForbiddenException {
        String[] authorizedRoles = new String[] {"manager", "chiefManager"};
        checkUserPermission(authorizedRoles, request);
        User user = (User) request.getAttribute("user");

        request.setAttribute("basePath", request.getRequestURI());

        String userType = user.getUserType();
        if (userType.equals("manager")) {
            dispatch("/WEB-INF/classes/page/management/managerAdminPanel.jsp", request, response);
        } else { // if (userType.equals("chiefManager"))
            dispatch("/WEB-INF/classes/page/management/chiefManagerAdminPanel.jsp", request, response);
        }
    }

}
