<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="ex" uri="/WEB-INF/custom.tld"%>

<%@ page contentType="text/html" pageEncoding="UTF-8" %>

<!doctype html>
<html lang="en">

    <head>
        <title><ex:TranslateTag  lang="${lang}">Create User</ex:TranslateTag></title>
        <%@ include file="../../partial/include-head.jsp" %>
        <style>
            <%@ include file="createUser.css" %>
        </style>
    </head>

    <body>

        <!-- Start Main-Wrapper -->
        <div id="main-wrapper">

            <%@ include file="../../partial/include-header.jsp" %>

            <!-- Start Header-Section -->
            <section class="header-section listing">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <h3 class="title-section pull-left">
                                <ex:TranslateTag  lang="${lang}">Create User</ex:TranslateTag>
                            </h3>
                            <ul class="breadcrumbs custom-list list-inline pull-right">
                                <li><ex:TranslateTag  lang="${lang}">Home</ex:TranslateTag></li>
                                <li><ex:TranslateTag  lang="${lang}">Management</ex:TranslateTag></li>
                                <li><ex:TranslateTag  lang="${lang}">Create User</ex:TranslateTag></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </section>
            <!-- End Header-Section -->

            <section id="listing">
                <div class="container">
                    <div class="sidebar col-md-3">

                        <div class="fleets-filters toggle-container">
                            <div class="special-offers sidebar">
                                <a href="${basePath}/userList"><h5 class="title-section"><ex:TranslateTag  lang="${lang}">User List</ex:TranslateTag></h5></a>
                            </div>
                            <div class="special-offers sidebar">
                                <a href="${basePath}/createUser"><h5 class="title-section"><ex:TranslateTag  lang="${lang}">Create User</ex:TranslateTag></h5></a>
                            </div>

                            <div class="special-offers sidebar">
                                <a href="${basePath}/createHotel"><h5 class="title-section"><ex:TranslateTag  lang="${lang}">Create Hotel</ex:TranslateTag></h5></a>
                            </div>
                            <div class="special-offers sidebar">
                                <a href="${basePath}/manageHotelList"><h5 class="title-section"><ex:TranslateTag  lang="${lang}">Manage Hotel List</ex:TranslateTag></h5></a>
                            </div>
                        </div>

                    </div>

                    <div class="listing-content col-md-9">

                        <div class="listing-pagination">
                            <form class="form-horizontal" method="POST" action="register">
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-2 control-label">Email</label>
                                    <div class="col-sm-10">
                                        <input type="email" name="email" class="form-control" id="inputEmail3" placeholder="Email">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputPassword3" class="col-sm-2 control-label"><ex:TranslateTag  lang="${lang}">Password</ex:TranslateTag></label>
                                    <div class="col-sm-10">
                                        <input type="password" name="password" class="form-control" id="inputPassword3" placeholder="Password">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputUserType" class="col-sm-2 control-label"><ex:TranslateTag  lang="${lang}">User Type</ex:TranslateTag></label>
                                    <div class="col-sm-10">
                                        <select name="role" id="inputUserType" class="form-control">
                                            <c:forEach items="${roleList}" var="role">
                                                <option value="${role}">${role}</option>
                                            </c:forEach>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group hidden managingHotel">
                                    <label for="managingHotelId" class="col-sm-2 control-label"><ex:TranslateTag  lang="${lang}">Managing Hotel</ex:TranslateTag></label>
                                    <div class="col-sm-10">
                                        <select name="managingHotelId" id="managingHotelId" class="form-control" disabled>
                                            <c:forEach items="${hotelList}" var="hotel">
                                                <option value="${hotel.id}">${hotel.name}</option>
                                            </c:forEach>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-offset-2 col-sm-10">
                                        <button type="submit" class="btn btn-default"><ex:TranslateTag  lang="${lang}">Create</ex:TranslateTag></button>
                                    </div>
                                </div>
                            </form>
                        </div>

                    </div>
                    <!--</form>-->
                </div>
            </section>

            <%@include file="../../partial/include-footer.jsp" %>

        </div>
        <!-- End Main-Wrapper -->

        <%@ include file="../../partial/include-js.jsp" %>
        <script>
            <%@ include file="createUser.js" %>
        </script>

    </body>

</html>