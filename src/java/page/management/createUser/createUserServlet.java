package page.management.createUser;

import exception.ForbiddenException;
import exception.UnauthorizedException;
import model.Hotel;
import model.User;
import page.BaseServlet;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

public class createUserServlet extends BaseServlet {

    @Override
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, UnauthorizedException, ForbiddenException {
        checkUserPermission("chiefManager", request);
        request.setAttribute("roleList", User.getRoleList());
        request.setAttribute("hotelList", Hotel.getHotelListFromDB());
        System.out.println(Hotel.getHotelListFromDB());

        request.setAttribute("basePath", "management");

        dispatch("/WEB-INF/classes/page/management/createUser/createUser.jsp", request, response);

    }
}
