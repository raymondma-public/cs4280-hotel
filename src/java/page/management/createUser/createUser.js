(function ($) {
    $('#inputUserType').on('change', function () {
        if($(this).val() === 'manager') {
            $('.managingHotel').removeClass('hidden');
            $('#managingHotelId').removeAttr('disabled');
        } else {
            $('.managingHotel').addClass('hidden');
            $('#managingHotelId').attr('disabled', 'disabled');
        }
    });
})(jQuery);