<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="ex" uri="/WEB-INF/custom.tld"%>

<%@page contentType="text/html" pageEncoding="UTF-8" %>

<!doctype html>
<html lang="en">

    <head>
        <title><ex:TranslateTag  lang="${lang}">Manage Rooms</ex:TranslateTag></title>
        <%@ include file="../partial/include-head.jsp" %>
    </head>

    <body>

        <!-- Start Main-Wrapper -->
        <div id="main-wrapper">

            <%@ include file="../partial/include-header.jsp" %>

            <!-- Start Header-Section -->
            <section class="header-section listing">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <h3 class="title-section pull-left">
                               <ex:TranslateTag  lang="${lang}"> Manage Rooms</ex:TranslateTag>
                            </h3>
                            <ul class="breadcrumbs custom-list list-inline pull-right">
                                <li><ex:TranslateTag  lang="${lang}">Home</ex:TranslateTag></li>
                                <li><ex:TranslateTag  lang="${lang}">Management</ex:TranslateTag></li>
                                <li><ex:TranslateTag  lang="${lang}">Manage Room List</ex:TranslateTag></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </section>
            <!-- End Header-Section -->

            <section id="listing">
                <div class="container">
                    <div class="sidebar col-md-3">
                        <div class="fleets-filters toggle-container">
                            <div class="special-offers sidebar">
                                <a href="${basePath}/createRoom"><h5 class="title-section"><ex:TranslateTag  lang="${lang}">Create Room</ex:TranslateTag></h5></a>
                            </div>
                            <div class="special-offers sidebar">
                                <a href="${basePath}/manageRoomList"><h5 class="title-section"><ex:TranslateTag  lang="${lang}">Manage Room List</ex:TranslateTag></h5></a>
                            </div>
                        </div>
                    </div>

                    <div class="listing-content col-md-9">

                        <div class="listing-pagination">

                            <div class="ibox-content">
                                <div class="row m-b-sm m-t-sm">
                                    <!--                                        <div class="col-md-1">
                                                                                <button type="button" id="loading-example-btn" class="btn btn-white btn-sm"><i class="fa fa-refresh"></i> Refresh</button>
                                                                            </div>-->
<!--                                    <div class="col-md-12">
                                        <div class="input-group"><input type="text" placeholder="Search" class="input-sm form-control"> <span class="input-group-btn">
                                                <button type="button" class="btn btn-sm btn-primary"> Go!</button> </span></div>
                                    </div>-->
                                </div>

                                <div class="project-list">

                                    <table class="table table-hover">
                                        <tbody>

                                            <c:forEach items="${rooms}" var="room">
                                                <tr>
                                                    <!--                                                    <td class="project-status">
                                                                                                            <span class="label label-primary">Active</span>
                                                                                                        </td>-->
                                                    <td class="project-title">
                                                        <a href="room/${room.id}">${room.roomNumber}</a>

                                                        <!--<br>-->
                                                        <!--<small>${booking.startDate} - ${booking.endDate}</small>-->
                                                    </td>
                                                    <td class="project-completion">
                                                        $${room.price}
                                                        <!--                                                        <small>Completion with: 48%</small>
                                                                                                                <div class="progress progress-mini">
                                                                                                                    <div style="width: 48%;" class="progress-bar"></div>
                                                                                                                </div>-->
                                                    </td>

                                                    <td class="project-people">
                                                        <c:if test="${not empty room.iconURL}">
                                                            <a href=""><img alt="image" width="50px" height="50px" class="img-circle" src="${room.iconURL}"></a>
                                                            </c:if>
                                                    </td>
                                                    <td class="project-people">
                                                    <td class="project-people">

                                                    </td>
                                                    <td class="project-people">

                                                        <a href="editRoom?roomId=${room.id}" class="btn btn-white btn-sm"><i class="fa fa-pencil"></i> Edit </a>

                                                    </td>

                                                    <td class="project-actions pull-right">

                                                        <!--<a href="#" class="btn btn-white btn-sm"><i class="fa fa-pencil"></i> Edit </a>-->
                                                        <form action="removeRoom" method="post">
                                                            <input type="hidden"  name="roomId" value="${room.id}"/>
                                                            <input type="submit" class="btn btn-danger btn-sm" value="Remove">
                                                        </form>

                                                    </td>

                                                </tr>
                                            </c:forEach>
                                        </tbody>
                                    </table>
                                </div>
                            </div>


                        </div>


                    </div>
                    <!--</form>-->
                </div>
            </section>

            <%@include file="../partial/include-footer.jsp" %>

        </div>
        <!-- End Main-Wrapper -->

        <%@ include file="../partial/include-js.jsp" %>

    </body>

</html>
