/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package page.webChat;

/**
 *
 * @author RaymondMa
 */
import javax.websocket.server.ServerEndpointConfig.Configurator;

/**
 *  * ChatServerEndPointConfigurator  * @author Jiji_Sasidharan  
 */
public class ChatServerEndPointConfigurator extends Configurator {

    private static ChatServerEndPoint chatServer = new ChatServerEndPoint();

    @Override
    public <T> T getEndpointInstance(Class<T> endpointClass) throws InstantiationException {
        return (T) chatServer;
    }
}
