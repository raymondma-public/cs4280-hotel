/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package page.bookmark;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Calendar;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import exception.UnauthorizedException;
import model.BookMark;
import model.Room;
import model.User;
import page.BaseServlet;

public class BookmarkServlet extends BaseServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, UnauthorizedException {
        checkUserLogon(request);

        String roomId = request.getParameter("roomId");
        String hotelId = request.getParameter("hotelId");
        HttpSession session = request.getSession(false);
        User user = (User) session.getAttribute("user");
        if (user != null) {

            BookMark.addBookMarkToDB("" + user.getId(), roomId);
            response.sendRedirect(request.getHeader("referer"));
        }

    }

}
