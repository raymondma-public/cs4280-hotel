<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="ex" uri="/WEB-INF/custom.tld"%>

<%@page import="model.Facility" %>
<%@page import="java.util.ArrayList" %>
<%@page import="model.Room" %>
<%@page import="model.Price" %>
<%@page contentType="text/html" pageEncoding="UTF-8" %>

<!doctype html>
<html lang="en">

    <head>
        <title><ex:TranslateTag  lang="${lang}">Room List</ex:TranslateTag></title>
        <%@ include file="../partial/include-head.jsp" %>
    </head>
    <%ArrayList<Room> rooms = (ArrayList<Room>) request.getAttribute("roomList");%>
    <body>

        <!-- Start Main-Wrapper -->
        <div id="main-wrapper">

            <%@ include file="../partial/include-header.jsp" %>

            <!-- Start Header-Section -->
            <section class="header-section listing">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <h3 class="title-section pull-left">
                                <ex:TranslateTag  lang="${lang}">Search Result</ex:TranslateTag>
                            </h3>
                            <ul class="breadcrumbs custom-list list-inline pull-right">
                                <li><ex:TranslateTag  lang="${lang}">Home</ex:TranslateTag></li>
                                <li><ex:TranslateTag  lang="${lang}">Search Result</ex:TranslateTag></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </section>
            <!-- End Header-Section -->

            <section id="listing">
                <div class="container">

                    <div class="sidebar col-md-3">
                        <div class="fleets-filters toggle-container">
                            <form class="row default-form" method="get">
                                <!--<h5 class="toggle-title">Filters</h5>-->
                                <aside class="toggle-content">
                                    <!--                                    <div class="search">
                                                                            <div class="default-form">
                                                                                <input type="text" placeholder="Search" name="search">
                                                                                <i class="fa fa-search"></i>
                                                                            </div>
                                                                        </div>-->
                                    <div class="general">
                                        <h5 class="title">General</h5>
                                        <span class="arrival calendar">
                                            <input type="text" name="start" placeholder="Start"
                                                   data-dateformat="yy/mm/dd">
                                            <i class="fa fa-calendar"></i>
                                        </span>
                                        <span class="departure calendar">
                                            <input type="text" name="end" placeholder="End"
                                                   data-dateformat="yy/mm/dd">
                                            <i class="fa fa-calendar"></i>
                                        </span>
                                        <span class="adults select-box">
                                            <select name="adults" data-placeholder="<ex:TranslateTag  lang="${lang}">Adults</ex:TranslateTag>">
                                                <option><ex:TranslateTag  lang="${lang}">Adults</ex:TranslateTag></option>
                                                <option value="1">1 <ex:TranslateTag  lang="${lang}">adult</ex:TranslateTag></option>
                                                <option value="2">2 <ex:TranslateTag  lang="${lang}">adults</ex:TranslateTag></option>
                                                <option value="3">3 <ex:TranslateTag  lang="${lang}">adults</ex:TranslateTag></option>
                                                <option value="4">4 <ex:TranslateTag  lang="${lang}">adults</ex:TranslateTag></option>
                                            </select>
                                        </span>
                                        <span class="children select-box">
                                            <select name="children" data-placeholder="<ex:TranslateTag  lang="${lang}">Children</ex:TranslateTag>">
                                                <option>Children</option>
                                                <option value="1">1 <ex:TranslateTag  lang="${lang}">children</ex:TranslateTag></option>
                                                <option value="2">2 <ex:TranslateTag  lang="${lang}">childrens</ex:TranslateTag></option>
                                                <option value="3">3 <ex:TranslateTag  lang="${lang}">childrens</ex:TranslateTag></option>
                                                <option value="4">4 <ex:TranslateTag  lang="${lang}">childrens</ex:TranslateTag></option>
                                            </select>
                                        </span>
                                        <h5 class="title">Price</h5>
                                        <div class="slider-range-container box-row">
                                            <div class="slider-range" data-min="0" data-max="5000" data-step="50"
                                                 data-default-min="0" data-default-max="500" >
                                                <!--data-currency="$"-->
                                            </div>
                                            <div class="clearfix">
                                                <input type="text" class="range-from" value="0" name="priceFrom">
                                                <input type="text" class="range-to" value="500" name="priceTo">
                                            </div>
                                        </div>
                                        <!--                                        <h5 class="title">Amenities</h5>
                                                                                <ul class="custom-list additional-filter-list clearfix">
                                        <c:set var="facilityArr" value="${facilityArr}"></c:set>
                                        <c:set var="facilities" value="${['AC', 'Hotbath', 'Parking', 'Room Service', 'Safe', 'TV', 'Wifi']}"></c:set>
                                        <c:forEach items="${facilities}" var="facility">
                                            <li>
                                                <span class="checkbox-input">
                                                    <input id="filter-${facility}" <c:if test="${facilityArr.contains(facility)}">checked="checked"</c:if> type="checkbox" name="facility" value="<c:out value="${facility}"></c:out>">
                                                    <label for="filter-${facility}">${facility}</label>
                                                </span>
                                            </li>
                                        </c:forEach>
                                    </ul>-->
                                    </div>
                                    <div class="buttons">
                                        <button type="submit" class="btn btn-transparent-gray pull-left">
                                            Filter
                                        </button>
                                        <button type="reset" class="btn btn-transparent-gray pull-right">
                                            Reset
                                        </button>
                                    </div>
                                </aside>

                            </form>
                        </div>

                        <%@ include file="../partial/include-suggestList-vertical.jsp" %>



                    </div>


                    <div class="listing-content col-md-9">
                        <div class="listing-pagination">
                            <form method="get">
                                <h5 class="title pull-left"><ex:TranslateTag  lang="${lang}">Available Room</ex:TranslateTag></h5>
                                <span class="adults select-box pull-right">
                                    <select name="perpage" data-placeholder="<%= request.getAttribute("perpage")%> Per Page" onchange="this.form.submit()">
                                        <option class="hidden">10 Per Page</option>
                                        <c:set var="selectedPerPage" value="${perpage}"></c:set>
                                        <c:forEach items="${['5','10', '25', '50', '100']}" var="perpage">
                                            <option value="${perpage}" <c:if test="${selectedPerPage == perpage}">selected="selected"</c:if>>${perpage} Per Page</option>
                                        </c:forEach>
                                    </select>
                                </span>
                                <ul class="pagination custom-list list-inline pull-right">
                                    <li class="prev"><a href="roomList/${(page-1==0)?'1':(page-1)}${request.querystring}">Prev</a></li>

                                    <c:forEach begin="1" end="${totalPage}" varStatus="loop">
                                        <li class="number"><a href="roomList/<c:out value="${loop.index}"/>?perpage=${perpage}">${loop.index}</a></li>
                                        <!--                                    <li class="number"><a href="roomList/2">2</a></li>
                                                                            <li class="number"><a href="roomList/3">3</a></li>-->
                                    </c:forEach>
                                    <li class="prev"><a href="roomList/${page+1}<%= request.getQueryString() == null ? "" : "?" + request.getQueryString()%>">Next</a></li>
                                </ul>
                            </form>
                        </div>

                        <%
//                                for (int i = 0; i < rooms.size(); i++) {
if(rooms.size()>0){
for (int i = 0; i < rooms.size(); i++) {
                                Room currentRoom = rooms.get(i);
                        %>

                        <div class="listing-room-list">
                            <div class="thumbnail">
                                <div class="thumbnail-slider">
                                    <a href="room/<%= currentRoom.getId()%>">
                                        <% if (currentRoom.getIconURL() != null && !currentRoom.getIconURL().equals("")) {%>
                                            <img src="<%= currentRoom.getIconURL()%>" alt="" class="img-responsive">
                                        <% } else { %>
                                            <img src="img/listing-1.jpg" alt="" class="img-responsive">
                                        <% } %>
                                    </a>
                                </div>
                            </div>
                            <div class="listing-room-content">
                                <div class="row">


                                    <form action="booking" method="post">
                                        <input type="hidden" name="roomId" value="<%= currentRoom.getId()%>">
                                        <div class="col-md-12">
                                            <header>
                                                <div class="pull-left">
                                                    <h5 class="title">
                                                        <a href="room/<%= currentRoom.getId()%>"><%=currentRoom.getHotel().getName()%> - <%= currentRoom.getRoomNumber()%>
                                                        </a>
                                                    </h5>

                                                    <ul class="tags custom-list list-inline pull-left">
                                                        <li><%= currentRoom.getNoOfBed()%> Bed</li>
                                                        <li><%= currentRoom.getMaxNoOfAdult()%> Adults</li>
                                                        <li><%= currentRoom.getMaxNoOfChildren()%> Childrens                                                        </li>
                                                        <!--<li><a href="#">Sea View</a></li>-->
                                                        <%if(currentRoom.getRating()>=0){%>
                                                        </li>  Rating : <%=currentRoom.getRating()%></li>
                                                        <%}else{%>
                                                        No Rating Yet
                                                        <%}%>
                                                    </ul>
                                                </div>
                                                <div class="pull-right">
                                                    <span class="price">
                                                        <%Price priceObj=currentRoom.getPrice();
                                                        if(priceObj!=null){


                                                        %>

                                                        from $<%= priceObj.getPrice()%>/day
                                                        <%}else{%>

                                                        <ex:TranslateTag  lang="${lang}">No set price</ex:TranslateTag>
                                                        <%}%>
                                                    </span>
                                                </div>
                                            </header>
                                            <div class="listing-facitilities">
                                                <!--<div class="listing-facitilities">-->
                                                <div class="row">
                                                    <% int count = 0;
                                                    if( currentRoom.getInRoomFacilities().size()>0){
                                                        for (Facility f : currentRoom.getInRoomFacilities()) {
                                                            
                                                            if (count % 3 == 0) {%>

                                                    <%
                                                        }
                                                    %>
                                                    <!--<div class="row">-->
                                                    <div class="col-md-3 col-sm-3">
                                                        <ul class="facilities-list custom-list">
                                                            <li><%= f.getIconURL()%><span><%= f.getName()%></span></li>
                                                        </ul>
                                                    </div>
                                                    <!--</div>-->
                                                    <% //if (count % 3 == 0) {%>
                                                    <!--</div>-->
                                                    <% //}
                                                        count++;
                                                        
                                                        
                                                    }}else{ %>
                                                    <ex:TranslateTag  lang="${lang}">No facility added.</ex:TranslateTag>
                                                    <%}%>
                                                    <c:if test="${not empty user}">


                                                        <input type="date" style="position:relative; bottom:0;" name="start" placeholder="Arrival" data-dateformat="m/d/y" id="dp1460829925112" class="hasDatepicker">
                                                        <input type="date" style="position:relative; bottom:0;" name="end" placeholder="Arrival" data-dateformat="m/d/y" id="dp1460829925112" class="hasDatepicker">
                                                    </c:if>
                                                    <div class="col-md-3 col-sm-3 pull-right">
                                                        <a href="room/<%= currentRoom.getId()%>" class="btn btn-transparent-gray">
                                                            Read More
                                                        </a>

                                                        <c:if test="${not empty user}"><!--if logined-->
                                                            ${currentRoom.bookMarked}
                                                            <%--<c:choose>--%>
                                                            <%--<c:when test="${currentRoom.bookMarked}">--%>
                                                            <%if(currentRoom.isBookMarked()){%>
                                                            <a href="removeBookmark?roomId=<%= currentRoom.getId()%>" class="btn btn-danger">
                                                                <ex:TranslateTag  lang="${lang}">Bookmarked</ex:TranslateTag>
                                                            </a>
                                                            <%}else{%>
                                                            <%--           </c:when>    
                                                                   <c:otherwise>--%>
                                                            <a href="bookmark?roomId=<%= currentRoom.getId()%>" class="btn btn-primary">
                                                                <ex:TranslateTag  lang="${lang}">Bookmark</ex:TranslateTag>
                                                            </a>
                                                            <%}%>
                                                            <%--    </c:otherwise>
                                                           </c:choose>--%>
                                                        </c:if>

                                                        <!--<button class="btn btn-gray-dark">-->


                                                        <c:if test="${not empty user}">
                                                            <input type="submit" class="btn btn-gray-dark" value=" Book now">
                                                        </c:if>
                                                        <!--</but //ton>-->
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </form>
                                </div>
                            </div>
                        </div>
                        <%}%>
                        <%}else{%>
                        No Room Found
                        <%}%>
                        <div class="listing-pagination footer text-center">
                            <ul class="pagination custom-list list-inline no-margin">
                                <li class="prev"><a  href="roomList/<%= (int)request.getAttribute("page")-1%>?<%= request.getQueryString()%>">Prev</a></li>
                                    <c:forEach begin="1" end="${totalPage}" varStatus="loop">
                                    <li class="number"><a href="roomList/<c:out value="${loop.index}"/>?perpage=${perpage}">${loop.index}</a></li>
                                    <!--                                    <li class="number"><a href="roomList/2">2</a></li>
                                                                        <li class="number"><a href="roomList/3">3</a></li>-->
                                </c:forEach>
                                <li class="next"><a  href="roomList/<%= (int)request.getAttribute("page")+1%>?<%= request.getQueryString()%>">Next</a></li>
                            </ul>
                        </div>
<!--                        <div class="special-offers listing">
                            <h5 class="title-section">Special Offers</h5>
                            <div class="offers-content">
                                <ul class="custom-list">
                                    <li>
                                        <div class="thumbnail"><img src="img/listing-1.jpg" alt="" class="img-responsive">
                                        </div>
                                        <h5 class="title"><a href="#">King Suite</a></h5>
                                        <span class="price">from $99/day</span>
                                    </li>
                                    <li>
                                        <div class="thumbnail"><img src="img/listing-1.jpg" alt="" class="img-responsive">
                                        </div>
                                        <h5 class="title"><a href="#">King Suite</a></h5>
                                        <span class="price">from $99/day</span>
                                    </li>
                                    <li>
                                        <div class="thumbnail"><img src="img/listing-1.jpg" alt="" class="img-responsive">
                                        </div>
                                        <h5 class="title"><a href="#">King Suite</a></h5>
                                        <span class="price">from $99/day</span>
                                    </li>
                                </ul>
                            </div>
                        </div>-->
                    </div>

                </div>
            </section>

            <%@include file="../partial/include-footer.jsp" %>

        </div>
        <!-- End Main-Wrapper -->

        <%@ include file="../partial/include-js.jsp" %>

    </body>

</html>