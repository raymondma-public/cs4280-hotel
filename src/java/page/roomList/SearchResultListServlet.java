package page.roomList;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import model.Room;
import model.User;
import model.ViewHistory;
import page.BaseServlet;
import utils.Is;

public class SearchResultListServlet extends BaseServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Map<String, String[]> queryString = request.getParameterMap();

        String hotelId = request.getParameter("hotelId");
        int totalPage = 0;
        // page size
        String pageSizeStr = request.getParameter("perpage");
        Integer pageSize = 10; // default page size
        if (Is.PositiveInteger(pageSizeStr)) {
            pageSize = Integer.parseInt(pageSizeStr);
        }

        // page number
//        String pageStr = queryString.getOrDefault("page", new String[]{"1"})[0];
        String pageStr = "1";
        if (request.getPathInfo() != null) {
            pageStr = request.getPathInfo().replaceFirst("/", "");
        }

        String startDateString = request.getParameter("start");
        if (startDateString != null && startDateString.equals("")) {
            startDateString = null;
        }
//        if (startDateString != null) {
//            startDateString = startDateString.substring(4);
//        }
        String endDateString = request.getParameter("end");
        if (endDateString != null && endDateString.equals("")) {
            endDateString = null;
        }

//        if (endDateString != null) {
//            endDateString = endDateString.substring(4);
//        }
        String noOfAdultString = request.getParameter("adults");
        try {
            Integer.parseInt(noOfAdultString);
        } catch (NumberFormatException e) {
            noOfAdultString = null;
        }
        String noOfChildrenString = request.getParameter("children");
        try {
            Integer.parseInt(noOfChildrenString);
        } catch (NumberFormatException e) {
            noOfChildrenString = null;
        }

        String priceFromString = request.getParameter("priceFrom");
        if (priceFromString != null) {
            priceFromString = priceFromString.substring(2);
        }
        String priceToString = request.getParameter("priceTo");
        if (priceToString != null) {
            priceToString = priceToString.substring(2);
        }

        String[] facilities = request.getParameterValues("facility");

        System.out.println("startDateString" + startDateString);
        System.out.println("endDateString" + endDateString);

        System.out.println("noOfAdultString" + noOfAdultString);
        System.out.println("noOfChildrenString" + noOfChildrenString);
        System.out.println("priceFromString" + priceFromString);
        System.out.println("priceToString" + priceToString);
//        System.out.println("startDateString" + startDateString);
//
//        for (String f : facilities) {
//            System.out.print(f + " ");
//        }
        System.out.println();

        Integer page = 1; // default page number
        if (Is.PositiveInteger(pageStr)) {
            page = Integer.parseInt(pageStr);
        }

        // result rooms
        ArrayList<Room> rooms = null;
        if (hotelId == null) {//no id --> all hotel room
            totalPage = Room.getTotalPageNoFromDB(pageSize, startDateString, endDateString, noOfAdultString, noOfChildrenString, priceFromString, priceToString, facilities);
            if (page > totalPage) {
                page = totalPage;
            }
            rooms = Room.getRoomList(page, pageSize, startDateString, endDateString, noOfAdultString, noOfChildrenString, priceFromString, priceToString, facilities);

        } else {//have id--> specific hotel room
            totalPage = Room.getTotalPageNoFromDB(hotelId, pageSize, startDateString, endDateString, noOfAdultString, noOfChildrenString, priceFromString, priceToString, facilities);
            if (page > totalPage) {
                page = totalPage;
            }
            rooms = Room.getRoomList(hotelId, page, pageSize, startDateString, endDateString, noOfAdultString, noOfChildrenString, priceFromString, priceToString, facilities);

        }
        HttpSession session = request.getSession(false);
        if (session != null) {
            User user = (User) session.getAttribute("user");
            if (user != null) {

                Room.addBookMarksFromDBToModels(rooms, user.getId(), hotelId);

            }
        }
        // facility
        List<String> facilityArr = new ArrayList();
        if (queryString.containsKey("facility")) {
            facilityArr = Arrays.asList(queryString.get("facility"));
        }

        request.setAttribute("facilityArr", facilityArr);

        request.setAttribute("roomList", rooms);
        request.setAttribute("page", page);
        request.setAttribute("perpage", pageSize);
        request.setAttribute("queryString", queryString);
        request.setAttribute("totalPage", totalPage);
        ArrayList<Room> suggestRoomList = Room.getSuggestedRoomList();
        request.setAttribute("suggestRoomList", suggestRoomList);

        dispatch("/WEB-INF/classes/page/roomList/listing.jsp", request, response);
    }

}
