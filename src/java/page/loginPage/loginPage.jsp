<%-- 
    Document   : loginPage
    Created on : Apr 14, 2016, 8:20:47 PM
    Author     : RaymondMa
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="ex" uri="/WEB-INF/custom.tld"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>


<!doctype html>
<html lang="en">

    <!-- Mirrored from 188.226.241.135/hotelpetra/homepage_v1/contact.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 24 Mar 2016 07:46:06 GMT -->
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <title><ex:TranslateTag  lang="${lang}">Login</ex:TranslateTag></title>
        <%@ include file="../partial/include-head.jsp" %>

        <!--[if IE 9]>
        <script src="js/media.match.min.js"></script>
        <![endif]-->
    </head>

    <body>

        <!-- Start Main-Wrapper -->
        <div id="main-wrapper">

            <%@ include file="../partial/include-header.jsp" %>

            <!-- Start Header-Section -->
            <section class="header-section contact">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <h3 class="title-section pull-left">
                                Contact
                            </h3>
                            <ul class="breadcrumbs custom-list list-inline pull-right">
                                <li><ex:TranslateTag  lang="${lang}">Home</ex:TranslateTag></li>
                                <li><ex:TranslateTag  lang="${lang}">Login</ex:TranslateTag></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </section>
            <!-- End Header-Section -->

            <!-- Start Contact-Form -->
            <div class="container">
                <div class="contact-box">
                    <form action="login" class="contact-form default-form" method="post">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="preamble col-md-12">
                                    <h3><ex:TranslateTag  lang="${lang}">Login</ex:TranslateTag></h3>
                                </div>
                                <!--                                <span class="select-box" title="hotel-type">
                                                                    <select name="hotel-type" data-placeholder="- Select -">
                                                                        <option>- Select -</option>
                                                                        <option value="1">Tent</option>
                                                                        <option value="2">Iber Hotel</option>
                                                                        <option value="3">Bungalow</option>
                                                                    </select>
                                                                </span>-->
                            </div>
                            <!--                            <div class="col-md-6">
                                                            <input type="text" placeholder="Your Name">
                                                        </div>
                                                        <div class="col-md-6">
                                                            <input type="text" placeholder="Your Phone">
                                                        </div>-->
                            <div class="col-md-12">
                                <input type="email" name="email" placeholder="Email">
                            </div>
                            <div class="col-md-12">
                                <input type="password" name="password" placeholder="<ex:TranslateTag  lang="${lang}">Password</ex:TranslateTag>">
                            </div>
                            <!--                            <div class="col-md-12">
                                                            <textarea placeholder="How we can help you"></textarea>
                                                        </div>-->
                            <div class="col-lg-md text-center">
                                <button class="submit-btn button btn-transparent-white"><ex:TranslateTag  lang="${lang}">Login</ex:TranslateTag></button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <!-- End Contact-Form -->

            <!-- Start Contact-Map  -->
            <div id="contact-map">
            </div>
            <!-- End Contact-Map -->


            <!-- Start Partners -->
            <section class="partners">
                <div class="container">
                    <div class="row">
                        <div class="preamble col-md-12">
                            <h3>Our Partners</h3>
                        </div>
                        <div class="col-md-12">
                            <div id="clients-slider" class="owl-carousel">
                                <img src="img/company1.png" alt="" class="img-responsive">
                                <img src="img/company2.png" alt="" class="img-responsive">
                                <img src="img/company3.png" alt="" class="img-responsive">
                                <img src="img/company1.png" alt="" class="img-responsive">
                                <img src="img/company2.png" alt="" class="img-responsive">
                                <img src="img/company3.png" alt="" class="img-responsive">
                                <img src="img/company1.png" alt="" class="img-responsive">
                                <img src="img/company2.png" alt="" class="img-responsive">
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- End Partners -->

            <%@include file="../partial/include-footer.jsp" %>

        </div>
        <!-- End Main-Wrapper -->


        <!-- Scripts -->
        <script src="vendor/jquery-2.1.4.min.js"></script>
        <script src="js/scripts.js"></script>
        <script src="vendor/owl.carousel.min.js"></script>
        <script src="http://maps.googleapis.com/maps/api/js?sensor=false" type="text/javascript"></script>
        <script src="vendor/gmap3.min.js"></script>
        <script src="vendor/jquery-ui-1.10.4.custom.min.js"></script>
        <script src="vendor/jquery.ba-outside-events.min.js"></script>
        <script src="vendor/jqueryui.js"></script>
        <script src="vendor/jquery.vide.min.js"></script>
        <script src="vendor/tab.js"></script>
        <script src="vendor/transition.js"></script>
        <script src="vendor/jquery.matchHeight-min.js"></script>
    </body>

    <!-- Mirrored from 188.226.241.135/hotelpetra/homepage_v1/contact.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 24 Mar 2016 07:46:06 GMT -->
</html>
