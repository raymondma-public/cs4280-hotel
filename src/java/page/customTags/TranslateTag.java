package page.customTags;

import com.rmtheis.yandtran.language.Language;
import com.rmtheis.yandtran.translate.Translate;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.SimpleTagSupport;

public class TranslateTag extends SimpleTagSupport {

    private String lang;
    private static HashMap<String, String> translateCache = new HashMap();

    /**
     * Called by the container to invoke this tag. The implementation of this
     * method is provided by the tag library developer, and handles all tag
     * processing, body iteration, etc.
     */
    @Override
    public void doTag() throws JspException {
        JspWriter out = getJspContext().getOut();

        try {

            Translate.setKey("trnsl.1.1.20160416T144522Z.d8a517460ce7f25d.1a8e83513e2481af85db0d1468b4ef44c21bdf21");

            StringWriter sw = new StringWriter();
            getJspBody().invoke(sw);

            if (translateCache.containsKey(this.lang + "___" + sw.toString())) {
                out.print(translateCache.get(this.lang + "___" + sw.toString()));
                return;
            }

            Language targetLang;
            if (this.lang.equals("en")) {
                targetLang = Language.ENGLISH;
            } else if (this.lang.equals("zh")) {
                targetLang = Language.CHINESE;
            } else if (this.lang.equals("fr")) {
                targetLang = Language.FRENCH;
            } else if (this.lang.equals("it")) {
                targetLang = Language.ITALIAN;
            } else {
                targetLang = Language.ENGLISH;
            }
            String translatedText;
            if (!this.lang.equals("en")) {
                translatedText = Translate.execute(sw.toString(), Language.ENGLISH, targetLang);
            } else {
                translatedText = sw.toString();
            }

            System.out.println(translatedText);

            translateCache.put(this.lang + "___" + sw.toString(), translatedText);
            out.print(translatedText);

        } catch (java.io.IOException ex) {
            throw new JspException("Error in TranslateTag tag", ex);
        } catch (Exception ex) {
            Logger.getLogger(TranslateTag.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void setLang(String lang) {
        if (lang != null && lang.trim() != "") {
            this.lang = lang;
        } else {
            this.lang = "en";
        }
    }

}
