/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package page.roomRating;

import exception.UnauthorizedException;
import exception.UserNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Room;
import model.User;
import page.BaseServlet;

/**
 *
 * @author RaymondMa
 */
public class RoomRatingServlet extends BaseServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        BaseServlet.redirectError(response, 404);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
//        processRequest(request, response);
//        String[] roomRatings = request.getParameterValues("roomRating");

        try {
            super.checkUserLogon(request);
            User user = this.getCurrentUser(request);

            String roomRatings = request.getParameter("roomRating");
            String roomId = request.getParameter("roomId");
            int rating = Integer.parseInt(roomRatings);
//        System.out.println(roomId + " " + roomRatings[0]+ " " + roomRatings[1]+ " " + roomRatings[2]+ " " + roomRatings[3]+ " " + roomRatings[4]);
            System.out.println(roomId + " " + roomRatings);

            Room.addRoomRatingToDB(user.getId(), roomId, rating);
            request.setAttribute("rated", Room.userHasRated(user.getId(), roomId));
            response.sendRedirect(request.getHeader("referer"));

        } catch (UnauthorizedException ex) {
            BaseServlet.redirectError(response, 401);
            return;

        } catch (UserNotFoundException ex) {
            BaseServlet.redirectError(response, 401);
            return;
        } catch (SQLException ex) {
            Logger.getLogger(RoomRatingServlet.class.getName()).log(Level.SEVERE, null, ex);
            BaseServlet.redirectError(response, 401);
            return;
        }

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
