/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package page.removeRoom;

import exception.CannotRemoveException;
import exception.ForbiddenException;
import exception.UnauthorizedException;
import exception.UserNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Booking;
import model.Room;
import model.User;
import page.BaseServlet;
import page.bookingDetail3.BookingDetail3;
import page.pay.PayServlet;

/**
 *
 * @author RaymondMa
 */
public class RemoveRoomServlet extends BaseServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        BaseServlet.redirectError(response, 404);
//        response.setContentType("text/html;charset=UTF-8");
//        PrintWriter out = response.getWriter();
//        try {
//            /* TODO output your page here. You may use following sample code. */
//            out.println("<!DOCTYPE html>");
//            out.println("<html>");
//            out.println("<head>");
//            out.println("<title>Servlet RemoveRoomServlet</title>");            
//            out.println("</head>");
//            out.println("<body>");
//            out.println("<h1>Servlet RemoveRoomServlet at " + request.getContextPath() + "</h1>");
//            out.println("</body>");
//            out.println("</html>");
//        } finally {
//            out.close();
//        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        try {
            this.checkUserLogon(request);
            this.checkUserPermission("manager", request);
        } catch (UnauthorizedException ex) {
            Logger.getLogger(BookingDetail3.class.getName()).log(Level.SEVERE, null, ex);
            this.redirectError(response, 401);
            return;
        } catch (ForbiddenException ex) {
            Logger.getLogger(RemoveRoomServlet.class.getName()).log(Level.SEVERE, null, ex);
              this.redirectError(response, 401);
            return;
        }

        User user;
        try {
            user = this.getCurrentUser(request);
        } catch (UserNotFoundException ex) {
            Logger.getLogger(BookingDetail3.class.getName()).log(Level.SEVERE, null, ex);
            this.redirectError(response, 401);
            return;
        }
        String roomId = request.getParameter("roomId");
//        String priceId = request.getParameter("priceId");
//        String noOfPplString = request.getParameter("noOfPpl");
//        System.out.println("pay param:" + bookingId + " " + priceId + " " + noOfPplString);
//        int noOfPpl = Integer.parseInt(noOfPplString);

        try {
            Room.removeRoom(roomId);
            response.sendRedirect("management/manageRoomList");
        } catch (CannotRemoveException ex) {
            Logger.getLogger(PayServlet.class.getName()).log(Level.SEVERE, null, ex);
            response.sendRedirect((String) request.getAttribute("javax.servlet.forward.request_uri"));
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
