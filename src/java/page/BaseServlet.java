package page;

import com.google.gson.Gson;
//import com.sun.deploy.net.HttpRequest;
import exception.*;
import model.User;
import utils.Is;
import utils.SecurityUtil;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public abstract class BaseServlet extends HttpServlet {

    protected static Gson gson = new Gson();

    protected User getCurrentUser(HttpServletRequest request) throws UserNotFoundException {
        HttpSession session = request.getSession(false);
        if (session == null) {
            throw new UserNotFoundException();
        }
        User user = (User) session.getAttribute("user");
        if (user == null) {
            throw new UserNotFoundException();
        }
        return user;
    }

    protected abstract void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, UnauthorizedException, ForbiddenException;

    protected static void dispatch(String jsp, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        RequestDispatcher dispatcher = request.getRequestDispatcher(jsp);
        dispatcher.forward(request, response);
    }

    protected static void dispatch(int statusCode, String jsp, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setStatus(statusCode);
        dispatch(jsp, request, response);
    }

    protected static void redirectHome(HttpServletResponse response) throws IOException {
        response.sendRedirect("home");
    }

    protected static void redirectError(HttpServletResponse response, int errorCode) throws IOException {
        response.sendRedirect("/Hotel/" + errorCode);
    }

    protected void checkUserPermission(String authorizedRole, HttpServletRequest request) throws UnauthorizedException, ForbiddenException {
        HttpSession session = request.getSession(false);
        if (session != null) {
            User user = (User) session.getAttribute("user");
            checkUserLogon(request);
            if (user != null) {
                String userRole = user.getUserType();
                if (!authorizedRole.equals(userRole)) {
                    throw new ForbiddenException(userRole, authorizedRole);
                }
            }
        }

    }

    protected void checkUserPermission(String[] authorizedRoles, HttpServletRequest request) throws UnauthorizedException, ForbiddenException {
        User user = (User) request.getAttribute("user");
        checkUserLogon(request);
        String userRole = user.getUserType();
        if (Is.NotInList(userRole, authorizedRoles)) {
            throw new ForbiddenException(userRole, authorizedRoles);
        }
    }

    protected void checkUserLogon(HttpServletRequest request) throws UnauthorizedException {
        HttpSession session = request.getSession(false);
        if (session == null) {
            throw new UnauthorizedException();
        }

        User user = (User) session.getAttribute("user");
        if (user == null) {
            throw new UnauthorizedException();
        }
    }

    protected void rateLimit(HttpSession session, int maxNoOfTrial, String event, int durationInMins, int blockingTimeoutInMins) throws RateLimitException {
        String key = event + "_trials";
        String durationKey = event + "_first_trial";
        String timeoutKey = event + "_rate_limit_timeout";
        Integer trialPassed = (Integer) session.getAttribute(key);
        if (trialPassed == null) { // first trial
            session.setAttribute(key, 1);
            session.setAttribute(durationKey, new Date());
        } else {
            session.setAttribute(key, ++trialPassed);

            Date firstTrial = (Date) session.getAttribute(durationKey);
            Date blockingTime = (Date) session.getAttribute(timeoutKey);

            if (blockingTime == null && (new Date().getTime() - firstTrial.getTime())/1000/60 > durationInMins) { // not being blocked, and duration passed, reset count
                session.setAttribute(key, 0);
            }

            if (trialPassed > maxNoOfTrial) { // need to block
                if (blockingTime == null) { // first block
                    session.setAttribute(timeoutKey, new Date());
                } else if ((new Date().getTime() - blockingTime.getTime())/1000/60 > blockingTimeoutInMins) { // timeout, release block
                    session.setAttribute(key, 1);
                } else { // continue block
                    throw new RateLimitException(event);
                }
            }
        }
    }

    protected void resetRateLimit(HttpSession session, String event) {
        String key = event + "_trials";
        String durationKey = event + "_first_trial";
        String timeoutKey = event + "_rate_limit_timeout";
        session.removeAttribute(key);
        session.removeAttribute(durationKey);
        session.removeAttribute(timeoutKey);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        middleware(request);

        try {
            SecurityUtil.checkReferer(request); // http get need to check for referer
            processRequest(request, response);
        } catch (UnauthorizedException e) {
            redirectError(response, 401);
        } catch (CSRFException e) {
            response.setStatus(400);
        } catch (ForbiddenException e) {
            redirectError(response, 403);
        } catch (URISyntaxException e) {
            redirectError(response, 400);
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        middleware(request);

        try {
            processRequest(request, response);
        } catch (UnauthorizedException e) {
            redirectError(response, 401);
        } catch (ForbiddenException e) {
            redirectError(response, 403);
        }
    }

    @Override
    protected void doPut(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        middleware(request);

        try {
            processRequest(request, response);
        } catch (UnauthorizedException e) {
            redirectError(response, 401);
        } catch (ForbiddenException e) {
            redirectError(response, 403);
        }
    }

    private void middleware(HttpServletRequest request) throws IOException {
        HttpSession session = request.getSession(false);
        if (session != null) {
            request.setAttribute("user", session.getAttribute("user"));
        }

        HashMap<String, Object> responseData = new HashMap();
        request.setAttribute("responseData", responseData);
    }

    @Override
    public String getServletInfo() {
        return "Hotel";
    }

}
