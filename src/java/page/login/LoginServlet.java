package page.login;

import com.google.gson.Gson;
import exception.RateLimitException;
import exception.UserLoginNotOKException;
import exception.UserNotFoundException;
import model.User;
import org.apache.commons.validator.ValidatorException;
import page.BaseServlet;

import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class LoginServlet extends BaseServlet {

    @Override
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // do nothing
    }

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
//        redirectHome(response);
        response.sendRedirect(request.getContextPath()+"/loginPage");
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Map<String, Object> data = new HashMap();

        HttpSession session = request.getSession(true);

        String email = request.getParameter("email");
        String password = request.getParameter("password");

        data.put("email", email);
        data.put("password", password);

        try {
            rateLimit(session, 5, "Login", 5, 30); // max 5 login trial within 5 mins, if existed, block for 30 mins
            User user = User.login(email, password);
            session.setAttribute("user", user);
            resetRateLimit(session, "Login"); // reset the rate limit after successful login
            redirectHome(response);
        } catch (ValidatorException ex) {
            request.setAttribute("error_message", ex.getMessage());
            data.put("error", ex.getMessage());
            dispatch(HttpServletResponse.SC_BAD_REQUEST, "/WEB-INF/classes/page/index/index.jsp", request, response);
        } catch (UserLoginNotOKException ex) {
            request.setAttribute("error_message", ex.getMessage());
            data.put("error", ex.getMessage());
            dispatch(HttpServletResponse.SC_SEE_OTHER, "/WEB-INF/classes/page/index/index.jsp", request, response);
        } catch (UserNotFoundException ex) {
            request.setAttribute("error_message", ex.getMessage());
            data.put("error", ex.getMessage());
            dispatch(HttpServletResponse.SC_SEE_OTHER, "/WEB-INF/classes/page/index/index.jsp", request, response);
        } catch (SQLException ex) {
            request.setAttribute("error_message", "Unexpected Error, please try again later");
            data.put("error", ex.getStackTrace());
            dispatch(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "/WEB-INF/classes/page/index/index.jsp", request, response);
        } catch (RateLimitException ex) {
            request.setAttribute("error_message", ex.getMessage() + session.getAttribute("Login_trials"));
            data.put("error", ex.getMessage());
            dispatch(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "/WEB-INF/classes/page/index/index.jsp", request, response);
        } finally {
            Logger.getLogger("login").log(Level.INFO, new Gson().toJson(data));
        }
    }

}
