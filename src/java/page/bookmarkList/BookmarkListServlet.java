/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package page.bookmarkList;

import exception.UnauthorizedException;
import exception.UserNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.BookMark;
import model.User;
import page.BaseServlet;

/**
 *
 * @author RaymondMa
 */
public class BookmarkListServlet extends BaseServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            this.checkUserLogon(request);
        } catch (UnauthorizedException ex) {
            Logger.getLogger(BookmarkListServlet.class.getName()).log(Level.SEVERE, null, ex);
            response.sendError(401);
            return;
        }

        User user;
        try {
            user = this.getCurrentUser(request);

            ArrayList<BookMark> bookmarks = BookMark.getBookMarksFromDB(user.getId());

//        BookMark.addRoomToBookmarkModel(bookmarks);
            request.setAttribute("bookmarks", bookmarks);
//        request.setAttribute("user", user);

            RequestDispatcher dispatcher = request
                    .getRequestDispatcher("/WEB-INF/classes/page/bookmarkList/bookmarkList.jsp");
            dispatcher.forward(request, response);
        } catch (UserNotFoundException ex) {
            Logger.getLogger(BookmarkListServlet.class.getName()).log(Level.SEVERE, null, ex);
            response.sendError(401);
        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
