/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package page.removeHotel;

import exception.CannotRemoveException;
import exception.ForbiddenException;
import exception.UnauthorizedException;
import exception.UserNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Hotel;
import model.Room;
import model.User;
import page.BaseServlet;
import page.bookingDetail3.BookingDetail3;
import page.pay.PayServlet;

/**
 *
 * @author RaymondMa
 */
public class RemoveHotelServlet extends BaseServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        BaseServlet.redirectError(response, 404);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        try {
            this.checkUserLogon(request);
            this.checkUserPermission("chiefManager", request);

            User user = this.getCurrentUser(request);

            String hotelId = request.getParameter("hotelId");

//            try {
                
                Hotel.removeHotel(hotelId);
//                Room.removeRoom(roomId);
                response.sendRedirect("management/manageHotelList");
//            } catch (CannotRemoveException ex) {
//                Logger.getLogger(PayServlet.class.getName()).log(Level.SEVERE, null, ex);
//                response.sendRedirect((String) request.getAttribute("javax.servlet.forward.request_uri"));
//            }
//            

        } catch (UnauthorizedException ex) {
            Logger.getLogger(BookingDetail3.class.getName()).log(Level.SEVERE, null, ex);
            this.redirectError(response, 401);
            return;
        } catch (ForbiddenException ex) {
            Logger.getLogger(RemoveHotelServlet.class.getName()).log(Level.SEVERE, null, ex);
            this.redirectError(response, 401);
            return;
        } catch (UserNotFoundException ex) {
            Logger.getLogger(BookingDetail3.class.getName()).log(Level.SEVERE, null, ex);
            this.redirectError(response, 401);
            return;
        } catch (CannotRemoveException ex) {
            Logger.getLogger(RemoveHotelServlet.class.getName()).log(Level.SEVERE, null, ex);
            response.sendRedirect(request.getHeader("referer"));
        }

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
