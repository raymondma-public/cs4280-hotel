/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package page.bookingDetail3;

import exception.ParseErrorException;
import exception.UnauthorizedException;
import exception.UserNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Booking;
import model.BookingReview;
import model.Room;
import model.User;
import page.BaseServlet;

import page.bookingList.BookingListServlet;
import page.room.RoomServlet;

/**
 *
 * @author RaymondMa
 */
public class BookingDetail3 extends BaseServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        try {
            this.checkUserLogon(request);
            User user;
            user = this.getCurrentUser(request);

            String bookingId = request.getParameter("bookingId");
            Booking booking = null;
            System.out.println("==================" + booking);
            booking = Booking.getOneBookingFromDB(bookingId);
            System.out.println("==================" + booking);
            request.setAttribute("room", booking.getRoom());
            if (!booking.getCustomer().getId().equals(user.getId())) {
                this.redirectError(response, 403);
                return;
            }

            request.setAttribute("booking", booking);
            boolean rated = Room.userHasRated(user.getId(), "" + booking.getRoom().getId());
            request.setAttribute("rated", rated);

            boolean reviewed = Booking.userHasReviewed(user.getId(), bookingId);
            request.setAttribute("reviewed", reviewed);
            if (reviewed) {
                request.setAttribute("reviewMessage", Booking.getBookingReviewMessageFromDB(bookingId));
            }

              ArrayList<BookingReview> bookingReviews;
            try {
                bookingReviews = BookingReview.getBookingReviewListMessageFromDB("" + booking.getRoom().getId());
                System.out.println("bookingReviews size:" + bookingReviews.size());
                request.setAttribute("bookingReviews", bookingReviews);
            } catch (SQLException ex) {

                Logger.getLogger(RoomServlet.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            ArrayList<Room> suggestRoomList = Room.getSuggestedRoomList();
            request.setAttribute("suggestRoomList", suggestRoomList);

            ArrayList<Room> relatedRoomList = Room.getRelatedRoomList("" + booking.getRoom().getHotel().getId());
            request.setAttribute("relatedRoomList", relatedRoomList);

//        this.dispatch("/WEB-INF/classes/page/bookingDetail/bookingDetail.jsp", request, response);
            dispatch("/WEB-INF/classes/page/bookingDetail3/bookingDetail.jsp", request, response);
        } catch (UnauthorizedException ex) {
            Logger.getLogger(BookingDetail3.class.getName()).log(Level.SEVERE, null, ex);
            this.redirectError(response, 401);
            return;
        } catch (UserNotFoundException ex) {
            Logger.getLogger(BookingDetail3.class.getName()).log(Level.SEVERE, null, ex);
            this.redirectError(response, 401);
            return;
        } catch (SQLException ex) {
            Logger.getLogger(BookingListServlet.class.getName()).log(Level.SEVERE, null, ex);
            this.redirectError(response, 404);
        } catch (ParseErrorException ex) {
            Logger.getLogger(BookingDetail3.class.getName()).log(Level.SEVERE, null, ex);
            this.redirectError(response, 404);
        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
