<%-- 
    Document   : room
    Created on : Apr 3, 2016, 7:38:21 PM
    Author     : RaymondMa
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="ex" uri="/WEB-INF/custom.tld"%>

<%@ page import="model.Room" %>
<%@page import="model.Facility" %>
<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<% Room room = (Room) request.getAttribute("room");%>

<!doctype html>
<html lang="en">

    <!-- Mirrored from 188.226.241.135/hotelpetra/homepage_v1/room.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 24 Mar 2016 07:45:09 GMT -->
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <title>Hotel Petra Room</title>

        <%@ include file="../partial/include-head.jsp" %>
        <link rel="stylesheet" href="http://netdna.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css">

        <style>
            @import url(http://fonts.googleapis.com/css?family=Roboto:500,100,300,700,400);

            * {
                margin: 0;
                padding: 0;
                font-family: roboto;
            }

            body { background: #000; }

            .cont {
                width: 93%;
                max-width: 350px;
                text-align: center;
                margin: 4% auto;
                padding: 30px 0;
                background: #111;
                color: #EEE;
                border-radius: 5px;
                border: thin solid #444;
                overflow: hidden;
            }

            hr {
                margin: 20px;
                border: none;
                border-bottom: thin solid rgba(255,255,255,.1);
            }

            div.title { font-size: 2em; }

            h1 span {
                font-weight: 300;
                color: #Fd4;
            }

            div.stars {
                width: 270px;
                display: inline-block;
            }

            input.star { display: none; }

            label.star {
                float: right;
                padding: 10px;
                font-size: 36px;
                color: #444;
                transition: all .2s;
            }

            input.star:checked ~ label.star:before {
                content: '\f005';
                color: #FD4;
                transition: all .25s;
            }

            input.star-5:checked ~ label.star:before {
                color: #FE7;
                text-shadow: 0 0 20px #952;
            }

            input.star-1:checked ~ label.star:before { color: #F62; }

            label.star:hover { transform: rotate(-15deg) scale(1.3); }

            label.star:before {
                content: '\f006';
                font-family: FontAwesome;
            }
        </style>
        <link href="http://www.cssscript.com/wp-includes/css/sticky.css" rel="stylesheet" type="text/css">
    </head>

    <body>

        <!-- Start Main-Wrapper -->
        <div id="main-wrapper">

            <%@ include file="../partial/include-header.jsp" %>


            <!-- Start Header-Section -->
            <section class="header-section room" style="background: url(${booking.room.iconURL})   ;  background-size: cover">
                <div id="gradient"></div>
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <h3 class="title-section pull-left">
                                ${booking.room.hotel.name} - ${booking.room.roomNumber} - ${booking.room.purpose}
                            </h3>
                            <ul class="breadcrumbs custom-list list-inline pull-right">

                            </ul>
                        </div>
                    </div>
                </div>
            </section>
            <!-- End Header-Section -->

            <!-- Start Room -->
            <section id="room">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="room-wrapper negative-margin">
                                <div class="sidebar col-md-3">
                                    <c:if test="${booking.room.hotel.latitude>0 && booking.room.hotel.longitude>0 }">
                                <!--<iframe class="col-md-12"  frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.com/maps?q='+${room.hotel.latitude}+','+${room.hotel.longitude}+'&hl=es;z=5&amp;output=embed"></iframe><br /><small><a href="https://maps.google.com/maps?q='+${room.hotel.latitude}+','+${room.hotel.longitude}+'&hl=es;z=14&amp;output=embed" style="color:#0000FF;text-align:left" target="_blank">See map bigger</a></small>-->
                                        <iframe
                                            class="col-md-12"
                                            width="600"
                                            height="450"
                                            frameborder="0" style="border:0"
                                            src="https://www.google.com/maps/embed/v1/place?key=AIzaSyAT-e2GjPZMh1Lfqe7wyFYVDwmfnZSHv34
                                            &center=${booking.room.hotel.latitude},${booking.room.hotel.longitude}
                                            &zoom=18&q=record+stores+in+Seattle" allowfullscreen>
                                        </iframe>
                                    </c:if>
                          

                                    <%@include file="../partial/include-suggestList-vertical-wraped-content.jsp" %>

                                </div>
                                <div class="room-content col-md-9">
                                    <div class="room-general">
                                        <img src="${booking.room.iconURL}" alt="" class="img-responsive">
                                        <header>
                                            <div class="pull-left">
                                                <ul class="tags custom-list list-inline pull-left">
                                                    <li>${booking.room.noOfBed} Bed</li>
                                                    <li>${booking.room.maxNoOfAdult} Adult</li>
                                                    <li>${booking.room.maxNoOfChildren} Child</li>
                                                </ul>
                                            </div>
                                            <div class="pull-right">
                                                <span class="price">
                                                    from ${booking.room.price}/day
                                                </span>
                                            </div>
                                        </header>
                                    </div>
                                    <div class="room-about">
                                        <h5 class="title-section">     ${booking.room.roomNumber} - ${booking.room.purpose}</h5>
                                        <span>You have booked: ${booking.startDate} - ${booking.endDate}</span>
                                        <br/>
                                        <c:if test="${empty booking.payment}">
                                            <a href="pay?bookingId=${booking.id}&priceId=${booking.room.price.id}" class="btn btn-transparent-gray" >Pay</a>
                                            <!--                                            <form action="pay" method="post">
                                                                                            <input type="submit" class="btn btn-transparent-gray" value="Pay">
                                                                                            <input type="hidden" value="${booking.id}">
                                                                                            </input>
                                                                                        </form>-->


                                        </c:if>

                                        <c:if test="${not empty booking.payment  }">
                                            Payed: total ${booking.payment.noOfPpl} people,  payed at ${booking.payment.paymentDate}

                                            <c:if test="${not rated}">
                                                <form action="roomRating" method="post">
                                                    <input type="hidden" name="roomId" value="${booking.room.id}">

                                                    <div class="stars">
                                                        <!--<form action="">-->
                                                        <input class="star star-5" id="star-5-2" type="radio" name="roomRating" value="5">
                                                        <label class="star star-5" for="star-5-2"></label>
                                                        <input class="star star-4" id="star-4-2" type="radio" name="roomRating" value="4">
                                                        <label class="star star-4" for="star-4-2"></label>
                                                        <input class="star star-3" id="star-3-2" type="radio" name="roomRating" value="3">
                                                        <label class="star star-3" for="star-3-2"></label>
                                                        <input class="star star-2" id="star-2-2" type="radio" name="roomRating" value="2">
                                                        <label class="star star-2" for="star-2-2"></label>
                                                        <input class="star star-1" id="star-1-2" type="radio" name="roomRating" value="1">
                                                        <label class="star star-1" for="star-1-2"></label>
                                                        <!--</form>-->
                                                    </div>
                                                    <input type="submit" class="btn btn-transparent-gray" value="Rate">
                                                </form>
                                            </c:if>

                                            <c:if test="${ rated}">
                                                <br/>Rating:${booking.room.rating }
                                            </c:if>

                                            <c:if test="${not reviewed}">

                                                <form action="review" method="post">
                                                    <input type="hidden" name="bookingId" value="${booking.id}">
                                                    <br>
                                                    <p class="form-row col-md-12">
                                                        <textarea name="message" placeholder="You message here"></textarea>
                                                    </p>
                                                    <input type="submit" class="btn btn-transparent-gray" value="Post Your Feeling">
                                                </form>

                                            </c:if>

                                            <c:if test="${ reviewed}">
                                                <br/>
                                                Your Review: ${reviewMessage}

                                            </c:if>

                                        </c:if>
                                    </div>



                                    <div class="room-tabs">
                                        <ul class="nav nav-tabs">
                                            <li class="active"><a data-toggle="tab" href="#amenities">Amenities</a></li>
                                            <li><a data-toggle="tab" href="#description">Description</a></li>
                                            <!--<li><a data-toggle="tab" href="#images">Images</a></li>-->
                                            <li><a data-toggle="tab" href="#reviews">Reviews</a></li>
                                        </ul>

                                        <div class="tab-content">
                                            <div id="amenities" class="tab-pane fade in active">
                                                <div class="listing-facitilities">
                                                    <div class="row">
                                                        <% int count = 0;
                                                                       if( room.getInRoomFacilities().size()>0){
                                                                           for (Facility f : room.getInRoomFacilities()) {
                                                            System.out.println("facility");
                                                                               if (count % 3 == 0) {%>

                                                        <%
                                                            }
                                                        %>
                                                        <!--<div class="row">-->
                                                        <div class="col-md-3 col-sm-3">
                                                            <ul class="facilities-list custom-list">
                                                                <li><%= f.getIconURL()%><span><%= f.getName()%></span></li>
                                                            </ul>
                                                        </div>
                                                        <!--</div>-->
                                                        <% //if (count % 3 == 0) {%>
                                                        <!--</div>-->
                                                        <% //}
                                                            count++;
                                                        
                                                        
                                                        }}else{ %>
                                                        No Facilities Added.
                                                        <%}%>

                                                        <!--                                                        <div class="col-md-4 col-sm-4">
                                                                                                                    <ul class="facilities-list custom-list">
                                                                                                                        <li><i class="fa fa-leaf"></i><span>AC</span></li>
                                                                                                                        <li><i class="fa fa-heart-o"></i><span>Hotbath</span></li>
                                                                                                                        <li><i class="fa fa-car"></i><span>Parking</span></li>
                                                                                                                    </ul>
                                                                                                                </div>
                                                                                                                <div class="col-md-4 col-sm-4">
                                                                                                                    <ul class="facilities-list custom-list">
                                                                                                                        <li><i class="fa fa-female"></i><span>Room service</span></li>
                                                                                                                        <li><i class="fa fa-key"></i><span>Free safe</span></li>
                                                                                                                        <li><i class="fa fa-desktop"></i><span>TV & Audio</span></li>
                                                                                                                    </ul>
                                                                                                                </div>
                                                                                                                <div class="col-md-4 col-sm-4">
                                                                                                                    <ul class="facilities-list custom-list">
                                                                                                                        <li><i class="fa fa-wifi"></i><span>WIFI access</span></li>
                                                                                                                        <li><i class="fa fa-clock-o"></i><span>24/7 Service</span></li>
                                                                                                                    </ul>
                                                                                                                </div>-->
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="description" class="tab-pane fade">
                                                <p>${booking.room.description}</p>
                                            </div>
                                            <div id="images" class="tab-pane fade">
                                                <div class="images-gallery">
                                                    <div class="row">
                                                        <div class="img-single col-md-3">
                                                            <img src="img/listing-1.jpg" alt="" class="img-responsive">
                                                        </div>
                                                        <div class="img-single col-md-3">
                                                            <img src="img/listing-1.jpg" alt="" class="img-responsive">
                                                        </div>
                                                        <div class="img-single col-md-3">
                                                            <img src="img/listing-1.jpg" alt="" class="img-responsive">
                                                        </div>
                                                        <div class="img-single col-md-3">
                                                            <img src="img/listing-1.jpg" alt="" class="img-responsive">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="reviews" class="tab-pane fade">
                                                <ul class="reviews-list custom-list">

                                                    <!--                                                        <div class="thumbnail">
                                                                                                                <img src="img/avatar.jpg" alt="">
                                                                                                            </div>-->
                                                    <c:forEach items="${bookingReviews}" var="bookingReview">
                                                        <li>
                                                            <div class="review-content">
                                                                <header>
                                                                    <h5>${bookingReview.email}</h5>
                                                                    <!--                                                                <ul class="stars custom-list list-inline">
                                                                                                                                        <li><i class="fa fa-star"></i></li>
                                                                                                                                        <li><i class="fa fa-star"></i></li>
                                                                                                                                        <li><i class="fa fa-star"></i></li>
                                                                                                                                        <li><i class="fa fa-star"></i></li>
                                                                                                                                        <li><i class="fa fa-star"></i></li>
                                                                                                                                    </ul>-->
                                                                    ${bookingReview.startDate} - ${bookingReview.endDate}
                                                                </header>
                                                                <p>${bookingReview.message}</p>
                                                                <br/>
                                                            </div>
                                                        </li>
                                                    </c:forEach>


                                                </ul>
                                            </div>
                                        </div>
                                    </div>

                                    <%@include file="../partial/include-relatedList.jsp" %>



                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- End Room -->


            <%@include file="../partial/include-footer.jsp" %>

        </div>
        <!-- End Main-Wrapper -->

        <%@ include file="../partial/include-js.jsp" %>


    </body>

    <!-- Mirrored from 188.226.241.135/hotelpetra/homepage_v1/room.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 24 Mar 2016 07:45:10 GMT -->
</html>