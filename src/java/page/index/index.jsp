<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="ex" uri="/WEB-INF/custom.tld"%>

<%--<%@include file="jspHeadTag.jsp" %>--%>
<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<!doctype html>
<html lang="en">

    <head>
        <title><ex:TranslateTag  lang="${lang}"> Hotel</ex:TranslateTag></title>

        <%@ include file="../partial/include-head.jsp"%>
        <style><%@ include file="index.css"%></style>
    </head>

    <body>

        <!-- Start Main-Wrapper -->
        <div id="main-wrapper">

            <%@ include file="../partial/include-header.jsp"%>

            <!-- Start Banner -->
            <section id="banner">

                <div class="banner-bg">
                    <div class="banner-bg-item"><img src="img/banner-bg-1.jpg" alt=""></div>
                    <div class="banner-bg-item"><img src="img/banner-bg-2.jpg" alt=""></div>
                    <div class="banner-bg-item"><img src="img/banner-bg-3.jpg" alt=""></div>
                    <div class="banner-bg-item"><img src="img/banner-bg-0.jpg" alt=""></div>
                </div>

                <div class="css-table">
                    <div class="css-table-cell">

                        <!-- Start Banner-Search -->
                        <div class="banner-search">
                            <div class="container">
                                <div id="hero-tabs" class="banner-search-inner">
                                    <ul class="custom-list tab-title-list clearfix">
                                        <li class="tab-title active"><a href="#hotel"><ex:TranslateTag  lang="${lang}">Hotel</ex:TranslateTag></a></li>
                                            <!--                                        <li class="tab-title"><a href="#resorts">Resorts</a></li>
                                                                                    <li class="tab-title"><a href="#suites">Private suites</a></li>
                                                                                    <li class="tab-title"><a href="#bb">Bed & breakfast</a></li>-->
                                        <%--<c:forEach var="asd" items="${requestScope.myList}">--%>
                                        <%-- This calls the getId() method on your asd object --%>
                                        <%--<c:out value="${asd.id}"/>--%>
                                        <%-- This calls the getName() method on your asd object --%>
                                        <%--<c:out value="${asd.userName}"/>--%>
                                        <%-- This calls the getAge() method on your asd object --%>
                                        <%--<c:out value="${asd.phone}"/>--%>
                                        <%--</c:forEach>--%>
                                        <%--<%=request.getContextPath()%>--%>
                                    </ul>
                                    <ul class="custom-list tab-content-list">

                                        <!-- Start Hotel -->
                                        <li class="tab-content active">
                                            <form action="roomList" class="default-form" method="get">
                                                <span class="arrival calendar">
                                                    <input type="text" name="start" placeholder="<ex:TranslateTag  lang="${lang}">Start</ex:TranslateTag>"
                                                           data-dateformat="yy/mm/dd">
                                                    </span>
                                                    <span class="departure calendar">
                                                        <input type="text" name="end" placeholder="<ex:TranslateTag  lang="${lang}">End</ex:TranslateTag>"
                                                               data-dateformat="yy/mm/dd">
                                                    </span>
                                                    <span class="adults select-box">
                                                        <select name="adults" data-placeholder="<ex:TranslateTag  lang="${lang}">Adults</ex:TranslateTag>">
                                                        <option><ex:TranslateTag  lang="${lang}">Adults</ex:TranslateTag></option>
                                                            <option value="1">1 adult</option>
                                                            <option value="2">2 adults</option>
                                                            <option value="3">3 adults</option>
                                                            <option value="4">4 adults</option>
                                                        </select>
                                                    </span>
                                                    <span class="children select-box">
                                                        <select name="children" data-placeholder="<ex:TranslateTag  lang="${lang}">Children</ex:TranslateTag>">
                                                            <option>Children</option>
                                                            <option value="1">1 children</option>
                                                            <option value="2">2 childrens</option>
                                                            <option value="3">3 childrens</option>
                                                            <option value="4">4 childrens</option>
                                                        </select>
                                                    </span>
                                                    <span class="submit-btn">
                                                        <button class="btn btn-transparent"><ex:TranslateTag  lang="${lang}">Search</ex:TranslateTag></button>
                                                        <!--<a href="#" class="advanced">Advanced Search</a>-->
                                                    </span>
                                                </form>
                                            </li>
                                            <!-- End Hotel -->

                                            <!-- Start Resorts -->
                                            <!--                                        <li class="tab-content">
                                                                                        <form action="http://188.226.241.135/hotelpetra/homepage_v1/index.html" class="default-form">
                                                                                            <span class="arrival calendar">
                                                                                                <input type="text" name="arrival" placeholder="Arrival" data-dateformat="m/d/y">
                                                                                                <i class="fa fa-calendar"></i>
                                                                                            </span>
                                                                                            <span class="departure calendar">
                                                                                                <input type="text" name="departure" placeholder="Departure" data-dateformat="m/d/y">
                                                                                                <i class="fa fa-calendar"></i>
                                                                                            </span>
                                                                                            <span class="adults select-box">
                                                                                                <select name="adults" data-placeholder="Adults">
                                                                                                    <option>Adults</option>
                                                                                                    <option value="1">2</option>
                                                                                                    <option value="2">3</option>
                                                                                                    <option value="3">4</option>
                                                                                                </select>
                                                                                            </span>
                                                                                            <span class="children select-box">
                                                                                                <select name="children" data-placeholder="Children">
                                                                                                    <option>Children</option>
                                                                                                    <option value="1">2</option>
                                                                                                    <option value="2">3</option>
                                                                                                    <option value="3">4</option>
                                                                                                </select>
                                                                                            </span>
                                                                                            <span class="submit-btn">
                                                                                                <button class="btn btn-transparent">Book Now</button>
                                                                                                <a href="#" class="advanced">Advanced Search</a>
                                                                                            </span>
                                                                                        </form>
                                                                                    </li>-->
                                            <!-- End Resorts -->

                                            <!-- Start Private Suites -->
                                            <!--                                        <li class="tab-content">
                                                                                        <form action="http://188.226.241.135/hotelpetra/homepage_v1/index.html" class="default-form">
                                                                                            <span class="arrival calendar">
                                                                                                <input type="text" name="arrival" placeholder="Arrival" data-dateformat="m/d/y">
                                                                                                <i class="fa fa-calendar"></i>
                                                                                            </span>
                                                                                            <span class="departure calendar">
                                                                                                <input type="text" name="departure" placeholder="Departure" data-dateformat="m/d/y">
                                                                                                <i class="fa fa-calendar"></i>
                                                                                            </span>
                                                                                            <span class="adults select-box">
                                                                                                <select name="adults" data-placeholder="Adults">
                                                                                                    <option>Adults</option>
                                                                                                    <option value="1">2 adults</option>
                                                                                                    <option value="2">3 adults</option>
                                                                                                    <option value="3">4 adults</option>
                                                                                                </select>
                                                                                            </span>
                                                                                            <span class="children select-box">
                                                                                                <select name="children" data-placeholder="Children">
                                                                                                    <option>Children</option>
                                                                                                    <option value="1">2</option>
                                                                                                    <option value="2">3</option>
                                                                                                    <option value="3">4</option>
                                                                                                </select>
                                                                                            </span>
                                                                                            <span class="submit-btn">
                                                                                                <button class="btn btn-transparent">Book Now</button>
                                                                                                <a href="#" class="advanced">Advanced Search</a>
                                                                                            </span>
                                                                                        </form>
                                                                                    </li>-->
                                            <!-- End Private Suites -->

                                            <!-- Start Bed & Breakfast -->
                                            <!--                                        <li class="tab-content">
                                                                                        <form action="http://188.226.241.135/hotelpetra/homepage_v1/index.html" class="default-form">
                                                                                            <span class="arrival calendar">
                                                                                                <input type="text" name="arrival" placeholder="Arrival" data-dateformat="m/d/y">
                                                                                                <i class="fa fa-calendar"></i>
                                                                                            </span>
                                                                                            <span class="departure calendar">
                                                                                                <input type="text" name="departure" placeholder="Departure" data-dateformat="m/d/y">
                                                                                                <i class="fa fa-calendar"></i>
                                                                                            </span>
                                                                                            <span class="adults select-box">
                                                                                                <select name="adults" data-placeholder="Adults">
                                                                                                    <option>Adults</option>
                                                                                                    <option value="1">2</option>
                                                                                                    <option value="2">3</option>
                                                                                                    <option value="3">4</option>
                                                                                                </select>
                                                                                            </span>
                                                                                            <span class="children select-box">
                                                                                                <select name="children" data-placeholder="Children">
                                                                                                    <option>Children</option>
                                                                                                    <option value="1">2</option>
                                                                                                    <option value="2">3</option>
                                                                                                    <option value="3">4</option>
                                                                                                </select>
                                                                                            </span>
                                                                                            <span class="submit-btn">
                                                                                                <button class="btn btn-transparent">Book Now</button>
                                                                                                <a href="#" class="advanced">Advanced Search</a>
                                                                                            </span>
                                                                                        </form>
                                                                                    </li>-->
                                            <!-- End Bed & Breakfast -->

                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <!-- End Banner-Search -->

                        </div>
                    </div>
                </section>
                <!-- End Banner -->

                <!-- Start About -->
<!--                <section class="about">
                    <div class="container">
                        <div class="row">
                            <div class="preamble col-md-12">
                                <h3>About Hotel Petra</h3>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nostrum eius exercitationem ab aspernatur, ratione officia animi. Rerum necessitatibus, culpa. Tenetur incidunt quasi unde libero molestias dicta doloremque, non aliquam ipsam!</p>
                            </div>
                            <div class="col-md-3 col-sm-6 feature text-center">
                                <div class="overlay">
                                    <img src="img/about1.jpg" alt="" class="img-responsive">
                                    <div class="overlay-shadow">
                                        <div class="overlay-content">
                                            <a href="#" class="btn btn-transparent-white">Read More</a>
                                        </div>
                                    </div>
                                </div>
                                <h4>Our Services</h4>
                                <p>Lorem ipsum dolor sit amet, consect adipisicing elit. Proin nibh augue, suscipit a, scelerisque sed.</p>
                            </div>

                            <div class="col-md-3 col-sm-6 feature text-center">
                                <div class="overlay">
                                    <img src="img/about2.jpg" alt="" class="img-responsive">
                                    <div class="overlay-shadow">
                                        <div class="overlay-content">
                                            <a href="#" class="btn btn-transparent-white">Read More</a>
                                        </div>
                                    </div>
                                </div>
                                <h4>Memberships</h4>
                                <p>Lorem ipsum dolor sit amet, consect adipisicing elit. Proin nibh augue, suscipit a, scelerisque sed.</p>
                            </div>

                            <div class="col-md-3 col-sm-6 feature text-center">
                                <div class="overlay">
                                    <img src="img/about3.jpg" alt="" class="img-responsive">
                                    <div class="overlay-shadow">
                                        <div class="overlay-content">
                                            <a href="#" class="btn btn-transparent-white">Read More</a>
                                        </div>
                                    </div>
                                </div>
                                <h4>Our Agents</h4>
                                <p>Lorem ipsum dolor sit amet, consect adipisicing elit. Proin nibh augue, suscipit a, scelerisque sed.</p>
                            </div>

                            <div class="col-md-3 col-sm-6 feature text-center">
                                <div class="overlay">
                                    <img src="img/about4.jpg" alt="" class="img-responsive">
                                    <div class="overlay-shadow">
                                        <div class="overlay-content">
                                            <a href="#" class="btn btn-transparent-white">Read More</a>
                                        </div>
                                    </div>
                                </div>
                                <h4>Our Offices</h4>
                                <p>Lorem ipsum dolor sit amet, consect adipisicing elit. Proin nibh augue, suscipit a, scelerisque sed.</p>
                            </div>
                        </div>
                    </div>
                </section>
                 End About 

                 Start Testimonials 
                <section class="testimonials">

                     Start Video Background 
                <%--<div id="video_testimonials" data-vide-bg="video/ocean" data-vide-options="position: 0% 50%"></div>--%>
                <div class="video_gradient"></div>
                 End Video Background 

                <div class="container">
                    <div class="row">
                        <div class="preamble light col-md-12">
                            <h3>Testimonials</h3>
                        </div>

                        <div class="col-md-12">
                            <div id="owl-testimonials" class="owl-carousel owl-theme">

                                 Start Container Item 
                                <div class="item">
                                    <div class="col-lg-12">
                                        <blockquote class="quote">
                                            <cite>John Doe<span class="job">CEO - UOUapps</span></cite>
                                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Proin nibh augue, suscipit a, scelerisque sed, lacinia in, mi. Cras vel lorem. Etiam pellentesque aliquet tellus. Phasellus pharetra nulla ac diam. Quisque semper justo at risus. Donec venenatis, turpis vel hendrerit interdum, dui ligula ultricies purus, sed posuere libero dui.
                                        </blockquote>
                                    </div>
                                </div>
                                 End Container Item 

                                 Start Container Item 
                                <div class="item">
                                    <div class="col-lg-12">
                                        <blockquote class="quote">
                                            <cite>John Kowalski<span class="job">CEO - UOUapps</span></cite>
                                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Proin nibh augue, suscipit a, scelerisque sed, lacinia in, mi. Cras vel lorem. Etiam pellentesque aliquet tellus. Phasellus pharetra nulla ac diam. Quisque semper justo at risus. Donec venenatis, turpis vel hendrerit interdum, dui ligula ultricies purus, sed posuere libero dui.
                                        </blockquote>
                                    </div>
                                </div>
                                 End Container Item 

                                 Start Container Item 
                                <div class="item">
                                    <div class="col-lg-12">
                                        <blockquote class="quote">
                                            <cite>John Doe<span class="job">CEO - UOUapps</span></cite>
                                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Proin nibh augue, suscipit a, scelerisque sed, lacinia in, mi. Cras vel lorem. Etiam pellentesque aliquet tellus. Phasellus pharetra nulla ac diam. Quisque semper justo at risus. Donec venenatis, turpis vel hendrerit interdum, dui ligula ultricies purus, sed posuere libero dui.
                                        </blockquote>
                                    </div>
                                </div>
                                 End Container Item 

                            </div>
                        </div>
                    </div>
                </div>
            </section>
             End Testimonials 

             Start Supertabs 
            <section class="supertabs">
                <div class="container">
                    <div class="row">

                         Start Tab-Navigation 
                        <div class="col-md-4 tab-navigation">
                            <ul class="custom-list">
                                <li class="active">
                                    <a data-toggle="tab" href="#fleet">
                                        <h5 class="title">Highest Standards</h5>
                                        <h6 class="subtitle">No compromises</h6>
                                    </a>
                                </li>
                                <li>
                                    <a data-toggle="tab" href="#drivers">
                                        <h5 class="title">Qualified Staff</h5>
                                        <h6 class="subtitle">You are in good hands</h6>
                                    </a>
                                </li>
                                <li>
                                    <a data-toggle="tab" href="#location">
                                        <h5 class="title">Around the World</h5>
                                        <h6 class="subtitle">Meet us everywhere</h6>
                                    </a>
                                </li>
                            </ul>
                        </div>
                         End Tab-Navigation 

                         Start Tab-Content 
                        <div class="col-md-8 tab-content">
                            <div id="fleet" class="tab-pane fade in active">
                                <img src="img/supertabs.jpg" alt="">
                                <header>
                                    <h5 class="title">Around the world</h5>
                                    <h6 class="subtitle">We are available</h6>
                                </header>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Repudiandae a aut consequatur similique tempora excepturi iusto provident eum quam aperiam id est, voluptas recusandae quisquam, pariatur rerum tempore atque illo!</p>
                            </div>
                            <div id="drivers" class="tab-pane fade">
                                <img src="img/supertabs-2.jpg" alt="">
                                <header>
                                    <h5 class="title">Our location</h5>
                                    <h6 class="subtitle">We are available</h6>
                                </header>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Repudiandae a aut consequatur similique tempora excepturi iusto provident eum quam aperiam id est, voluptas recusandae quisquam, pariatur rerum tempore atque illo!</p>
                            </div>
                            <div id="location" class="tab-pane fade">
                                <img src="img/supertabs-3.jpg" alt="">
                                <header>
                                    <h5 class="title">Our location</h5>
                                    <h6 class="subtitle">We are available</h6>
                                </header>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Repudiandae a aut consequatur similique tempora excepturi iusto provident eum quam aperiam id est, voluptas recusandae quisquam, pariatur rerum tempore atque illo!</p>
                            </div>
                        </div>
                         End Tab-Content 

                    </div>
                </div>
            </section>-->
            <!-- End Supertabs -->

            <!-- Start Partners -->
            <section class="partners">
                <div class="container">
                    <div class="row">
                        <div class="preamble col-md-12">
                            <h3>Our Partners</h3>
                        </div>
                        <div class="col-md-12">
                            <div id="clients-slider" class="owl-carousel">
                                <img src="img/company1.png" alt="" class="img-responsive">
                                <img src="img/company2.png" alt="" class="img-responsive">
                                <img src="img/company3.png" alt="" class="img-responsive">
                                <img src="img/company1.png" alt="" class="img-responsive">
                                <img src="img/company2.png" alt="" class="img-responsive">
                                <img src="img/company3.png" alt="" class="img-responsive">
                                <img src="img/company1.png" alt="" class="img-responsive">
                                <img src="img/company2.png" alt="" class="img-responsive">
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- End Partners -->

            <%@ include file="../partial/include-footer.jsp" %>

        </div>
        <!-- End Main-Wrapper -->

        <%@ include file="../partial/include-js.jsp"%>
        <script><%@ include file="index.js"%></script>
    </body>

</html>
