/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package page.pay;

import exception.CannotPayException;
import exception.ParseErrorException;
import exception.UnauthorizedException;
import exception.UserNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Booking;
import model.Payment;
import model.User;
import page.BaseServlet;
import page.bookingDetail3.BookingDetail3;
import page.bookingList.BookingListServlet;
import utils.Hash;

/**
 *
 * @author RaymondMa
 */
public class PayServlet extends BaseServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        try {
            this.checkUserLogon(request);
        } catch (UnauthorizedException ex) {
            Logger.getLogger(BookingDetail3.class.getName()).log(Level.SEVERE, null, ex);
            this.redirectError(response, 401);
            return;
        }

        User user;
        try {
            user = this.getCurrentUser(request);
        } catch (UserNotFoundException ex) {
            Logger.getLogger(BookingDetail3.class.getName()).log(Level.SEVERE, null, ex);
            this.redirectError(response, 401);
            return;
        }

//        String bookingId = request.getParameter("bookingId");
//        Booking booking = null;
//        try {
//            System.out.println("==================" + booking);
//            booking = Booking.getOneBookingFromDB(bookingId);
//            System.out.println("==================" + booking);
//            request.setAttribute("room", booking.getRoom());
//            request.setAttribute("booking", booking);
//        this.dispatch("/WEB-INF/classes/page/bookingDetail/bookingDetail.jsp", request, response);
        dispatch("/WEB-INF/classes/page/pay/pay.jsp", request, response);

//        } catch (SQLException ex) {
//            Logger.getLogger(BookingListServlet.class.getName()).log(Level.SEVERE, null, ex);
//            this.redirectError(response, 404);
//        } catch (ParseErrorException ex) {
//            Logger.getLogger(BookingDetail3.class.getName()).log(Level.SEVERE, null, ex);
//            this.redirectError(response, 404);
//        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
//        processRequest(request, response);

        try {
            this.checkUserLogon(request);
        } catch (UnauthorizedException ex) {
            Logger.getLogger(BookingDetail3.class.getName()).log(Level.SEVERE, null, ex);
            this.redirectError(response, 401);
            return;
        }

        User user;
        try {
            user = this.getCurrentUser(request);
        } catch (UserNotFoundException ex) {
            Logger.getLogger(BookingDetail3.class.getName()).log(Level.SEVERE, null, ex);
            this.redirectError(response, 401);
            return;
        }
        String bookingId = request.getParameter("bookingId");
        String priceId = request.getParameter("priceId");
        String noOfPplString = request.getParameter("noOfPpl");
        System.out.println("pay param:" + bookingId + " " + priceId + " " + noOfPplString);
        int noOfPpl = Integer.parseInt(noOfPplString);

        String password = request.getParameter("password");

        try {
            if (!User.passwordCorrect(user.getId(), Hash.hash(password))) {
                request.setAttribute("error_message", "Wrong password!");
                dispatch("/WEB-INF/classes/page/pay/pay.jsp", request, response);
            }
        } catch (SQLException ex) {
            Logger.getLogger(PayServlet.class.getName()).log(Level.SEVERE, null, ex);
            request.setAttribute("error_message", ex.getMessage());
            dispatch("/WEB-INF/classes/page/pay/pay.jsp", request, response);
        }

        try {
            Payment.pay(bookingId, priceId, noOfPpl);
            response.sendRedirect("bookingDetail?bookingId=" + bookingId);
        } catch (CannotPayException ex) {
            Logger.getLogger(PayServlet.class.getName()).log(Level.SEVERE, null, ex);
            response.sendRedirect((String) request.getAttribute("javax.servlet.forward.request_uri"));
        }

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
