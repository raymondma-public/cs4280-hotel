<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:if test="${not empty success_message || not empty param.success_message}">
    <div class="page-success-message page-message">
        <a href="javascript:;" class="pull-right" onclick="$(this).parent().fadeOut()">x</a>
        <c:out value="${param.success_message}" default="${success_message}"></c:out>
    </div>
</c:if>
<c:if test="${not empty error_message || not empty param.error_message}">
    <div class="page-error-message page-message">
        <a href="javascript:;" class="pull-right" onclick="$(this).parent().fadeOut()">x</a>
        <c:out value="${param.error_message}" default="${error_message}"></c:out>
    </div>
</c:if>
<c:if test="${not empty error_message || not empty param.error_message|| not empty success_message || not empty param.success_message}">
    <script>
        (function () {
            var pageErrMsg = document.querySelector('.page-message');
            var t;

            function autoCloseErrMsg() {
                t = setTimeout(function () {
                    $(pageErrMsg).fadeOut();
                }, 4000);
            }

            autoCloseErrMsg();
            pageErrMsg.onmouseover = function (e) {
                t && clearTimeout(t);
            };
            pageErrMsg.onmouseout = function (e) {
                autoCloseErrMsg();
            };
        })();
    </script>
</c:if>
