

<%@ page import="model.User" %>
<%@ page import="utils.Is" %>
<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<%
    User user = (User) session.getAttribute("user");
%>
<%@ include file="../partial/include-server-message.jsp"%>
<!-- Start Header -->
<header id="header">
    <div class="header-top">
        <div class="container">
            <div class="row">
                <div class="col-lg-5 col-md-5 col-sm-12">
                    <ul class="contact-info custom-list list-inline">
                        <% if (user != null && (user.isChiefManager() || user.isManager())) { %>
                        <li><i class="fa fa-user"></i><span><a href="management"><ex:TranslateTag  lang="${lang}">Management</ex:TranslateTag></a></span></li>
                                <% } %>
                    </ul>
                </div>
                <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12 text-right pull-right">
                    <div class="contact-right">
                        <!--                        <ul class="social custom-list list-inline">
                                                    <li><a href="#"><i class="fa fa-facebook-square"></i></a></li>
                                                    <li><a href="#"><i class="fa fa-twitter-square"></i></a></li>
                                                    <li><a href="#"><i class="fa fa-linkedin-square"></i></a></li>
                                                </ul>-->

                        <% if (user == null) {%>
                        <div class="header-login">
                            <button class="login-toggle header-btn"><i class="fa fa-power-off"></i> <ex:TranslateTag  lang="${lang}">Login</ex:TranslateTag></button>
                                <div class="header-form">
                                    <form action="login" class="default-form" method="post">
                                        <p class="alert-message warning"><i class="ico fa fa-exclamation-circle"></i> All fields are required!</p>
                                        <p class="form-row">
                                            <input class="required email" type="email" placeholder="Email" name="email">
                                        </p>
                                        <p class="form-row">
                                            <input class="required" type="password" placeholder="Password" name="password">
                                        </p>
                                        <p class="form-row">
                                            <button class="submit-btn button btn-blue"><i class="fa fa-power-off"></i> <ex:TranslateTag  lang="${lang}">Login</ex:TranslateTag></button>
                                        </p>
<!--                                        <p class="form-row forgot-password">
                                            <a href="#">Forgot Password?</a>
                                        </p>-->
                                    <%--<p class="form-row register">--%>
                                    <%--<a href="#">Register</a>--%>
                                    <%--</p>--%>
                                </form>
                            </div>
                        </div>


                        <div class="header-login">
                            <button class="login-toggle header-btn"><i class="fa fa-user-plus"></i></i> <ex:TranslateTag  lang="${lang}">Register</ex:TranslateTag></button>
                                <div class="header-form">
                                    <form action="register" class="default-form" method="post">
                                        <p class="alert-message warning"><i class="ico fa fa-exclamation-circle"></i> <ex:TranslateTag  lang="${lang}">All fields are required</ex:TranslateTag>! <i class="fa fa-times close"></i></p>
                                        <%--<div class="row">--%>
                                        <%--<div class="col-xs-4">--%>
                                        <%--<input type="radio" name="role" value="customer"> Customer<br>--%>
                                        <%--</div>--%>
                                        <%--<div class="col-xs-4">--%>
                                        <%--<input type="radio" name="role" value="manager"> MANAGER<br>--%>
                                        <%--</div>--%>
                                        <%--<div class="col-xs-4">--%>
                                        <%--<input type="radio" name="role" value="chiefManager"> Chief Manager<br>--%>
                                        <%--</div>--%>
                                        <%--</div>--%>


                                    <!--<span class="radio-input active">-->
                                    <!--                                        <input id="additional-filter-ac" checked="checked" type="radio">
                                                                            <label for="additional-filter-ac">CUSTOMER</label>
                                                                            </span>
                                                                            <span class="radio-input active">
                                                                            <input id="additional-filter-ac" checked="checked" type="radio">
                                                                            <label for="additional-filter-ac">MANAGER</label>-->
                                    <!--</span>-->
                                    <!--</p>-->
                                    <p class="form-row">
                                        <input class="required email" type="text" placeholder="Email" name="email">
                                    </p>
                                    <p class="form-row">
                                        <input class="required" type="password" placeholder="Password" name="password" pattern=".{8,}" title="8 or more characters">
                                    </p>

                                    <p class="form-row">
                                        <button class="submit-btn button btn-blue"><i class="fa fa-user-plus"></i></i> <ex:TranslateTag  lang="${lang}">Register</ex:TranslateTag></button>
                                    </p>
                                    <!--                                    <p class="form-row forgot-password">
                                                                            <a href="#">Forgot Password?</a>
                                                                        </p>
                                                                        <p class="form-row register">
                                                                            <a href="#">Register</a>
                                                                        </p>-->
                                </form>
                            </div>
                        </div>
                        <%} else {%>
                        <%--<%=userId%>--%>
                        <%--<%if (user != null) {%>--%>
                        <span><a href="userDetail"><%=user.getEmail()%></span>

                        <%--<%}%>--%>
                        <div class="header-login">

                            <form action="logout" class="" method="get">
                                <button type="submit" class="header-btn"><i class="fa fa-power-off"></i> <ex:TranslateTag  lang="${lang}">Logout</ex:TranslateTag></button>
                            </form>
                            <!--                            <div class="header-form">
                                                            <form action="<%=request.getContextPath()%>/page.logout.LogoutServlet" class="default-form" method="post">
                                                                <p class="alert-message warning"><i class="ico fa fa-exclamation-circle"></i> All fields are required! <i class="fa fa-times close"></i></p>
                                                                <p class="form-row">
                                                                    <input class="required email" type="text" placeholder="Email" name="email">
                                                                </p>
                                                                <p class="form-row">
                                                                    <input class="required" type="password" placeholder="Password" name="password">
                                                                </p>
                                                                <p class="form-row">
                                                                    <button class="submit-btn button btn-blue"><i class="fa fa-power-off"></i> Login</button>
                                                                </p>
                                                                <p class="form-row forgot-password">
                                                                    <a href="#">Forgot Password?</a>
                                                                </p>
                                                                <p class="form-row register">
                                                                    <a href="#">Register</a>
                                                                </p>
                                                            </form>
                                                        </div>-->
                        </div>
                        <% }%>


                        <div class="header-language">
                            <button class="header-btn"><i class="fa fa-globe"></i>EN</button>
                            <nav class="header-nav">
                                <ul class="custom-list">
                                    <c:forEach items="${['en','zh','fr','it']}" var="langSelection">
                                        <li class="active"><a href="setLang?lang=${langSelection}">${langSelection}</a></li>
                                        </c:forEach>

                                    <!--                                    </
                                                                        <li><a href="#">DE</a></li>
                                                                        <li><a href="#">FR</a></li>
                                                                        <li><a href="#">IT</a></li>-->
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="header-navi">
        <div class="container">
            <div class="row">
                <div class="col-md-12" id="bs-example-navbar-collapse-1">
                    <div class="row">
                        <div class="col-lg-5 col-md-5 col-sm-12">
                            <ul class="nav navbar-nav main-nav">
                                <li><a href="home"><ex:TranslateTag  lang="${lang}">Home</ex:TranslateTag></a></li>
                                <li><a href="hotelList"><ex:TranslateTag  lang="${lang}">Hotels</ex:TranslateTag></a></li>
                                <li><a href="roomList"><ex:TranslateTag  lang="${lang}">Rooms</ex:TranslateTag></a></li>
                                <!--                                <li class="has-submenu">
                                                                    <a href="#">Rooms</a>
                                                                    <ul class="sub-menu custom-list">
                                                                        <li><a href="hotelList">Hotels List View</a></li>
                                                                        <li><a href="roomList">Rooms List View</a></li>
                                                                        <li><a href="listing2.html">Rooms Grid View</a>
                                                                        </li>
                                                                        <li class="has-submenu">
                                                                            <a href="#">Apartments</a>
                                                                            <ul class="sub-menu custom-list">
                                                                                <li><a href="room4.html">Luxury Apartment 1</a></li>
                                                                                <li><a href="room.html">Luxury Apartment 2</a></li>
                                                                            </ul>
                                                                        </li>
                                                                        <li class="has-submenu">
                                                                            <a href="#">Deluxe Suites</a>
                                                                            <ul class="sub-menu custom-list">
                                                                                <li><a href="room2.html">Deluxe Suite 1</a></li>
                                                                                <li><a href="room3.html">Deluxe Suite 2</a></li>
                                                                            </ul>
                                                                        </li>
                                                                    </ul>
                                                                </li>-->
                                <!--<li><a href="staff.html">Our Staff</a></li>-->
                                <!--<li><a href="locations.html">Locations</a></li>-->
                            </ul>
                        </div>
                        <%--<div class="col-lg-5 col-lg-offset-2 col-md-5 col-md-offset-2 col-sm-12 col-sm-offset-0">--%>
                        <%--<ul class="nav navbar-nav main-nav pull-right text-right">--%>
                        <%--<li><a href="about.html">About Us</a></li>--%>
                        <%--<li><a href="testimonials.html">Testimonials</a></li>--%>
                        <%--<li class="has-submenu">--%>
                        <%--<!--<a href="news.html">News</a>-->--%>
                        <%--<ul class="sub-menu custom-list">--%>
                        <%--<li><a href="single_post.html">Single Post</a></li>--%>
                        <%--</ul>--%>
                        <%--</li>--%>
                        <%--<li><a href="contact.html">Contact</a></li>--%>
                        <%--</ul>--%>
                        <%--</div>--%>
                    </div>
                    <script>
                        (function () {
                            try {
                                var contextPath = '<%= request.getContextPath()%>/';
                                var page = location.pathname.replace(contextPath, '');
                                document.querySelector('.nav.navbar-nav.main-nav a[href="' + page + '"]').parentNode.classList.add('active');
                            } catch (e) {
                            }
                        })();
                    </script>
                </div>
            </div>
        </div>
    </div>
</header>
<!-- End Header -->

<!-- Start Header-Toggle -->
<div id="header-toggle">
    <i class="fa fa-bars"></i>
</div>
<!-- End Header-Toggle -->

<!-- Start Header-Logo -->
<div class="header-logo">
    <div class="header-logo-inner">
        <div class="css-table">
            <div class="css-table-cell">
                <a href="#">
                    <img src="img/logo.png" alt="" class="img-responsive center-block">
                </a>
            </div>
        </div>
    </div>
</div>
<!-- End Header-Logo -->