<%-- 
    Document   : include-userPageMenu
    Created on : Apr 14, 2016, 6:21:30 PM
    Author     : RaymondMa
--%>

<div class="sidebar col-md-3">
    <div class="fleets-filters toggle-container">
        <div class="special-offers sidebar">
            <a href="userDetail"><h5 class="title-section"><ex:TranslateTag  lang="${lang}">User Detail</ex:TranslateTag></h5></a>
        </div>
        <div class="special-offers sidebar">
            <a href="bookingList"><h5 class="title-section"><ex:TranslateTag  lang="${lang}">Booking List</ex:TranslateTag></h5></a>
        </div>
        <div class="special-offers sidebar">
            <a href="bookmarkList"><h5 class="title-section"><ex:TranslateTag  lang="${lang}">Bookmark List</ex:TranslateTag></h5></a>
        </div>
        <div class="special-offers sidebar">
            <a href="viewHistoryList"><h5 class="title-section"><ex:TranslateTag  lang="${lang}">View History</ex:TranslateTag></h5></a>
        </div>
    </div>

</div>