<div class="sidebar-widget offers">
    <h5 class="widget-title">Suggested Offer</h5>
    <div class="widget-content offers-content">
        <ul class="custom-list">
            <c:forEach items="${suggestRoomList}" var="suggestRoom">
                <li>
                    <div class="thumbnail"><img src="${suggestRoom.iconURL}" alt="" class="img-responsive"> </div>
                    <h5 class="title"><a href="room/${suggestRoom.id}">${suggestRoom.hotel.name} - ${suggestRoom.roomNumber}</a></h5>
                    <span class="price">from $${suggestRoom.price.price}/day</span>
                </li>
            </c:forEach>
        </ul>
    </div>
</div>


