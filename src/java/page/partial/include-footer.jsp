
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!-- Start Footer -->

<%@ page import="javax.servlet.ServletRequest"%>
<footer id="footer">

    <!-- Start Footer-Top -->
    <div class="footer-top">
        <div class="container">
            <div class="row">
                <div class="col-md-12 logo-footer">
                    <img src="img/logo-footer.png" alt="" class="img-responsive">
                </div>
                <div class="col-md-12 widget widget-about">
                    <h5 class="title">
                        About
                    </h5>
                    <p><ex:TranslateTag  lang="${lang}">CS4280 Advanced Internet Applications Development Assignment 2 Hotel Website</ex:TranslateTag></p>
                </div>
                <!--                <div class="col-md-4 widget widget-news">
                                    <h5 class="title">
                                        Latest News
                                    </h5>
                                    <ul class="custom-list">
                                        <li><a href="#">Lorem ipsum dolor</a></li>
                                        <li><a href="#">Proin nibh augue suscipit scelerisque</a></li>
                                        <li><a href="#">Etiam pellentesque aliquet tellus</a></li>
                                    </ul>
                                </div>
                                <div class="col-md-4 widget widget-newsletter">
                                    <h5 class="title">
                                        Newsletter
                                    </h5>
                                    <form action="#" class="default-form">
                                        <input type="text" placeholder="Your email" name="email">
                                        <button class="btn btn-transparent pull-right">Sign Up</button>
                                    </form>
                                    <div class="social">
                                        <h5 class="title pull-left">
                                            Stay in touch
                                        </h5>
                                        <ul class="custom-list list-inline pull-right">
                                            <li><a href="#"><i class="fa fa-facebook-square"></i></a></li>
                                            <li><a href="#"><i class="fa fa-twitter-square"></i></a></li>
                                            <li><a href="#"><i class="fa fa-linkedin-square"></i></a></li>
                                        </ul>
                                    </div>
                                </div>-->
            </div>
        </div>
    </div>
    <!-- End Footer-Top -->

    <!-- Start Footer-Copyrights -->
    <div class="footer-copyrights">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <p>Copyright 2016 © PetraHotel. All rights reserved. Powered by <a href="#">CS4280</a></p>
                    <p>This web site exists to fulfill the coursework requirement of CS4280.<br/>
                        <ex:TranslateTag  lang="${lang}">Do not use your real personal data as input.</ex:TranslateTag>
                    </p>
                </div>
            </div>
        </div>
    </div>
    <!-- End Footer-Copyrights -->
    <script src="http://ajax.aspnetcdn.com/ajax/jquery/jquery-1.9.0.min.js"></script>
    <script>
//            var ws = new WebSocket("ws://localhost:8084/Hotel/wschat/WsChatServlet");
        var ws = new WebSocket("ws://<%=request.getServerName()%>:<%=request.getServerPort()%><%=request.getContextPath()%>/chat");
        ws.onopen = function () {
        };
        ws.onmessage = function (message) {
            document.getElementById("chatlog").textContent += message.data + "\n";
        };
        function postToServer() {
            ws.send(document.getElementById("msg").value);
            document.getElementById("msg").value = "";
        }
        function closeConnect() {
            ws.close();
        }

        function toggleChatDetail() {
            $("#chatDetail").slideToggle();
//            alert();
        }


    </script>

    <div class="col-xs-12 col-md-3" style="position:fixed; right:0px;bottom:0px; ">
        <div class="col-sm-12 " onClick="toggleChatDetail();" style="background-color: rgba(0, 0, 0, 0.6) ; color: white;">Chat</div>
        <div id="chatDetail"  hidden>
            <textarea class="//col-sm-12"  style="width:100%" id="chatlog" readonly rows="6"></textarea><br/>

            <input id="msg" type="text" />
            <button class="btn btn-primary" type="submit" id="sendButton" onClick="postToServer()">Send!</button>
            <!--<button class="btn btn-primary" type="submit" id="sendButton" onClick="closeConnect()">End</button>-->
        </div>
    </div>
    <script>
        $("#msg").keyup(function (event) {
            if (event.keyCode == 13) {
                $("#sendButton").click();
            }
        });
    </script>
</footer>
<!-- End Footer -->