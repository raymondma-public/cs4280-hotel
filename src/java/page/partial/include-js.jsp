<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!-- Scripts -->
<script src="vendor/jquery-2.1.4.min.js"></script>
<script src="http://maps.googleapis.com/maps/api/js?sensor=false" type="text/javascript"></script>
<script src="vendor/gmap3.min.js"></script>
<script src="vendor/owl.carousel.min.js"></script>
<script src="vendor/AmaranJS/js/jquery.amaran.min.js"></script>
<script src="vendor/jquery-ui-1.10.4.custom.min.js"></script>
<script src="vendor/jquery.ba-outside-events.min.js"></script>
<script src="vendor/jqueryui.js"></script>
<script src="vendor/jquery.vide.min.js"></script>
<script src="vendor/jquery.matchHeight-min.js"></script>
<script src="vendor/bootstrap-3.3.6/js/bootstrap.min.js"></script>
<script src="vendor/lodash.min.js"></script>
<script src="vendor/is.min.js"></script>
<script src="js/scripts.js"></script>
<script src="js/utilities.js"></script>