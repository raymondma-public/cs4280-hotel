<div class="room-related">
    <h5 class="title-section">Related Offers</h5>

    <c:forEach items="${relatedRoomList}" var="relatedRoom">
        <div class="col-md-4">
            <div class="listing-room-grid">
                <div class="thumbnail">
                    <img src="${relatedRoom.iconURL}" alt="" class="img-responsive">
                </div>
                <div class="listing-room-content">
                    <div class="row">
                        <div class="col-md-12">
                            <header>
                                <h5 class="title">
                                    <a href="room/${relatedRoom.id}">${relatedRoom.hotel.name} - ${relatedRoom.roomNumber}</a>
                                </h5>
                                <ul class="tags custom-list list-inline">
                                    <li>${relatedRoom.noOfBed} Bed</li>
                                    <li>${relatedRoom.maxNoOfAdult} Adults</li>
                                    <li>${relatedRoom.maxNoOfChildren} Children</li>
                                    <!--<li><a href="#">Sea View</a></li>-->
                                </ul>
                            </header>
                            <span class="price">
                                $${relatedRoom.price.price}/<ex:TranslateTag  lang="${lang}">day</ex:TranslateTag>
                            </span>
<!--                            <button class="btn btn-transparent-gray">
                                Read More
                            </button>-->
                            <a href="room/${relatedRoom.id}"  class="btn btn-transparent-gray"> <ex:TranslateTag  lang="${lang}">Read More</ex:TranslateTag></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </c:forEach>

</div>
