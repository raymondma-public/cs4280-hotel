<div class="special-offers sidebar">
    <h5 class="title-section">Suggested Rooms</h5>
    <div class="offers-content">
        <ul class="custom-list">
            <c:forEach items="${suggestRoomList}" var="suggestRoom">
                <li>
                    <div class="thumbnail"><img src="${suggestRoom.iconURL}" alt="" class="img-responsive">
                    </div>
                    <h5 class="title"><a href="room/${suggestRoom.id}">${suggestRoom.hotel.name} - ${suggestRoom.roomNumber}</a></h5>
                    <span class="price">from $${suggestRoom.price.price}/<ex:TranslateTag  lang="${lang}">day</ex:TranslateTag></span>
                </li>
            </c:forEach>
        </ul>
    </div>
</div>
