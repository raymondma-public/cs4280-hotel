


<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="IE=edge">

<base href="<%= request.getContextPath()%>/">
<!--[if IE]>
<script type="text/javascript">
(function() {
var baseTag = document.getElementsByTagName('base')[0];
baseTag.href = baseTag.href;
})();
</script>
<![endif]-->

<!-- Stylesheets -->
<link rel="stylesheet" href="vendor/owl.carousel.css">
<link rel="stylesheet" href="vendor/font-awesome/css/font-awesome.min.css">
<link rel="stylesheet" href="vendor/bootstrap-3.3.6/css/bootstrap.min.css">
<link rel="stylesheet" href="vendor/AmaranJS/css/amaran.min.css">
<link rel="stylesheet" href="vendor/AmaranJS/css/animate.min.css">
<link rel="stylesheet" href="css/style.css">

<!-- Google Font -->
<link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Libre+Baskerville:400italic' rel='stylesheet' type='text/css'>

<!--[if IE 9]>
<script src="js/media.match.min.js"></script>
<![endif]-->