/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package page.editRoom;

import exception.ForbiddenException;
import exception.UnauthorizedException;
import exception.UserNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Facility;
import model.Hotel;
import model.Price;
import model.Room;
import model.User;
import org.apache.commons.validator.ValidatorException;
import page.BaseServlet;
import page.management.createRoom.createRoomServlet;

/**
 *
 * @author RaymondMa
 */
public class EditRoomServlet extends BaseServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            checkUserPermission("manager", request);

            request.setAttribute("basePath", "management");

            ArrayList<Facility> facilities = Facility.getFacilities();

            User user = this.getCurrentUser(request);

            String roomId = request.getParameter("roomId");

            Room room = Room.getRoom(Integer.parseInt(roomId));

            request.setAttribute("room", room);
            if (room.getHotel().getId() != user.getManagingHotel().getId()) {
                redirectError(response, 401);
                return;
            }
//            ArrayList<Boolean> containF = new ArrayList();
            boolean[] containF = new boolean[facilities.size()];
            int count = 0;
            System.out.println("Size: " + room.getInRoomFacilities().size());

            for (Facility f : facilities) {
                containF[count] = false;

                System.out.println("===========" + f.getId());
                for (Facility fRoom : room.getInRoomFacilities()) {
                    System.out.println("Compare : " + f.getId() + " " + fRoom.getId());
                    if (f.getId() == fRoom.getId()) {

                        containF[count] = true;
                        continue;
                    }
                }
                count++;

            }
            request.setAttribute("containF", containF);
            request.setAttribute("facilities", facilities);

//            request.setAttribute("success_message", "Changed");
            dispatch("/WEB-INF/classes/page/editRoom/editRoom.jsp", request, response);
        } catch (UnauthorizedException e) {
            redirectError(response, 401);
        } catch (ForbiddenException e) {
            redirectError(response, 403);
        } catch (SQLException e) {
            Logger.getLogger(createRoomServlet.class.getName()).log(Level.WARNING, null, e);
            redirectError(response, 500);
        } catch (UserNotFoundException ex) {
            Logger.getLogger(EditRoomServlet.class.getName()).log(Level.SEVERE, null, ex);
            redirectError(response, 401);
            return;
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
//        processRequest(request, response);

//        super.doPost(request, response);
        try {
            checkUserPermission("manager", request);
//            User user = (User) request.getAttribute("user");

            User user = this.getCurrentUser(request);

            Hotel hotel = user.getManagingHotel();
            int hotelId = hotel.getId();

            String roomId = request.getParameter("roomId");
            String purpose = request.getParameter("purpose");
            String roomNumber = request.getParameter("roomNumber");
            String description = request.getParameter("description");
            String noOfBed = request.getParameter("noOfBed");
            String maxNoOfAdult = request.getParameter("maxNoOfAdult");
            String maxNoOfChildren = request.getParameter("maxNoOfChildren");
            String priceForChild = request.getParameter("priceForChild");
            String priceForAdult = request.getParameter("priceForAdult");
            String[] facilities = request.getParameterValues("facilities");
            String icon = request.getParameter("icon");

            String priceId = request.getParameter("priceId");

//            String icon = request.getParameter("icon");
            Price.updatePrice(priceId, priceForAdult);

            Room.updateRoom(roomId, purpose, roomNumber, description, noOfBed, maxNoOfAdult, maxNoOfChildren, hotelId);

            Facility.removeFacilities(roomId);
            for (String fString : facilities) {
                Facility.addFacility(roomId, fString);
            }

            System.out.println("=========== editRoomServlet Redirect");
            response.setStatus(HttpServletResponse.SC_ACCEPTED);
            request.setAttribute("success_message", "Room Added");
//            dispatch("/WEB-INF/classes/page/management/createRoom/createRoom.jsp", request, response);




          ArrayList<Facility> realFacilities = Facility.getFacilities();



//            String roomId = request.getParameter("roomId");

            Room room = Room.getRoom(Integer.parseInt(roomId));

            request.setAttribute("room", room);
            if (room.getHotel().getId() != user.getManagingHotel().getId()) {
                redirectError(response, 401);
                return;
            }
//            ArrayList<Boolean> containF = new ArrayList();
            boolean[] containF = new boolean[realFacilities.size()];
            int count = 0;
            System.out.println("Size: " + room.getInRoomFacilities().size());

            for (Facility f : realFacilities) {
                containF[count] = false;

                System.out.println("===========" + f.getId());
                for (Facility fRoom : room.getInRoomFacilities()) {
                    System.out.println("Compare : " + f.getId() + " " + fRoom.getId());
                    if (f.getId() == fRoom.getId()) {

                        containF[count] = true;
                        continue;
                    }
                }
                count++;

            }
            request.setAttribute("containF", containF);
            request.setAttribute("facilities", realFacilities);

            
            
            request.setAttribute("success_message", "Changed");
            dispatch("/WEB-INF/classes/page/editRoom/editRoom.jsp", request, response);
//            response.sendRedirect(request.getHeader("referer"));
//            return;
//            BaseServlet.redirectHome(response);
//            BaseServlet.redirectError(response, 404);

        } catch (UnauthorizedException e) {
            redirectError(response, 401);
        } catch (ForbiddenException e) {
            redirectError(response, 403);
//        } catch (ValidatorException e) {
//            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
//            request.setAttribute("error_message", e.getMessage());
//            dispatch("/WEB-INF/classes/page/management/createRoom/createRoom.jsp", request, response);
        } catch (SQLException e) {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            Logger.getLogger(createRoomServlet.class.getName()).log(Level.SEVERE, null, e);
            request.setAttribute("error_message", "SQL: " + e.getMessage());
            response.sendRedirect(request.getHeader("referer"));
        } catch (UserNotFoundException ex) {
            Logger.getLogger(EditRoomServlet.class.getName()).log(Level.SEVERE, null, ex);
            redirectError(response, 401);
        }

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
