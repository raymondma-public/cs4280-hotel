<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="ex" uri="/WEB-INF/custom.tld"%>

<%@ page contentType="text/html" pageEncoding="UTF-8" %>

<!doctype html>
<html lang="en">

    <head>
        <title><ex:TranslateTag  lang="${lang}">Edit Room</ex:TranslateTag></title>
        <%@ include file="../partial/include-head.jsp" %>
        <style>
            <%@ include file="editRoom.css" %>
        </style>
    </head>

    <body>

        <!-- Start Main-Wrapper -->
        <div id="main-wrapper">

            <%@ include file="../partial/include-header.jsp" %>

            <!-- Start Header-Section -->
            <section class="header-section listing">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <h3 class="title-section pull-left">
                                <ex:TranslateTag  lang="${lang}">Edit Room</ex:TranslateTag>
                            </h3>
                            <ul class="breadcrumbs custom-list list-inline pull-right">
                                <li><ex:TranslateTag  lang="${lang}">Home</ex:TranslateTag></li>
                                <li><ex:TranslateTag  lang="${lang}">Management</ex:TranslateTag></li>
                                <li><ex:TranslateTag  lang="${lang}">Edit Room</ex:TranslateTag></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </section>
            <!-- End Header-Section -->

            <section id="listing">
                <div class="container">
                    <div class="sidebar col-md-3">
                        <div class="fleets-filters toggle-container">
                            <div class="special-offers sidebar">
                                <a href="${basePath}/createRoom"><h5 class="title-section"><ex:TranslateTag  lang="${lang}">Create Room</ex:TranslateTag></h5></a>
                            </div>
                              <div class="special-offers sidebar">
                                <a href="${basePath}/manageRoomList"><h5 class="title-section"><ex:TranslateTag  lang="${lang}">Manage Room List</ex:TranslateTag></h5></a>
                            </div>
                        </div>

                    </div>

                    <div class="listing-content col-md-9">

                        <div class="listing-pagination">
                            <form action="editRoom" class="form-horizontal" method="POST">
                                <input type="hidden" name="roomId" value="${room.id}"/>
                                <div class="form-group">
                                    <label for="purpose" class="col-sm-2 control-label" ><ex:TranslateTag  lang="${lang}">Purpose</ex:TranslateTag></label>
                                    <div class="col-sm-10">
                                        <input type="text" name="purpose" class="form-control" id="purpose" placeholder="<ex:TranslateTag  lang="${lang}">Purpose</ex:TranslateTag>" value="${room.purpose}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="roomNumber" class="col-sm-2 control-label" ><ex:TranslateTag  lang="${lang}">Room Number</ex:TranslateTag></label>
                                    <div class="col-sm-10">
                                        <input type="text" name="roomNumber" class="form-control" id="roomNumber" placeholder="<ex:TranslateTag  lang="${lang}">Room Number</ex:TranslateTag>" value="${room.roomNumber}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="description" class="col-sm-2 control-label"><ex:TranslateTag  lang="${lang}">Description</ex:TranslateTag></label>
                                    <div class="col-sm-10">
                                        <textarea name="description" class="form-control" id="description" placeholder="<ex:TranslateTag  lang="${lang}">Description</ex:TranslateTag>"  >${room.description}</textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="noOfBed" class="col-sm-2 control-label"><ex:TranslateTag  lang="${lang}">Number of bed</ex:TranslateTag></label>
                                    <div class="col-sm-10">
                                        <input type="number" name="noOfBed" class="form-control" id="noOfBed" placeholder="<ex:TranslateTag  lang="${lang}">Number of bed</ex:TranslateTag>" value="${room.noOfBed}">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="maxNoOfAdult" class="col-sm-2 control-label"><ex:TranslateTag  lang="${lang}">Number of Adult</ex:TranslateTag></label>
                                    <div class="col-sm-10">
                                        <input type="number" name="maxNoOfAdult" class="form-control" id="maxNoOfAdult" placeholder="<ex:TranslateTag  lang="${lang}">Number of Adult</ex:TranslateTag>" value="${room.maxNoOfAdult}">
                                    </div>
                                </div>


                                <div class="form-group">
                                    <label for="maxNoOfChildren" class="col-sm-2 control-label"><ex:TranslateTag  lang="${lang}">Number of Children</ex:TranslateTag></label>
                                    <div class="col-sm-10">
                                        <input type="number" name="maxNoOfChildren" class="form-control" id="maxNoOfChildren" placeholder="<ex:TranslateTag  lang="${lang}">Number of Children</ex:TranslateTag>" value="${room.maxNoOfChildren}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <!--                            <label for="priceForChild" class="col-sm-2 control-label">Price for Child</label>
                                                                <div class="col-sm-4">
                                                                    <input type="number" name="priceForChild" class="form-control" id="priceForChild" placeholder="Price for Child">
                                                                </div>-->


                                    <label for="priceForAdult" class="col-sm-2 control-label"><ex:TranslateTag  lang="${lang}">Price</ex:TranslateTag></label>
                                    <div class="col-sm-4">
                                        <input type="number" name="priceForAdult" class="form-control" id="priceForAdult" placeholder="<ex:TranslateTag  lang="${lang}">Price</ex:TranslateTag>" value="${room.price.price}">
                                    </div>

                                    <input type="hidden" name="priceId" value="${room.price.id}"/>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label"><ex:TranslateTag  lang="${lang}">Facilities</ex:TranslateTag></label>
                                    <div class="col-sm-10">
                                        <c:forEach var="facility" items="${facilities}" varStatus="loop">
                                            <!--${loop.index}-->
                                            <label class="control-label">
                                                <input type="checkbox" name="facilities" value="${facility.name}"

                                                       <c:if test="${not not containF[loop.index]}">
                                                           checked
                                                       </c:if>
                                                       >${facility.name}&nbsp;&nbsp;&nbsp;
                                            </label>
                                        </c:forEach>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-offset-2 col-sm-10">
                                        <button type="submit" class="btn btn-default"><ex:TranslateTag  lang="${lang}">Edit</ex:TranslateTag></button>
                                    </div>
                                </div>
                            </form>
                        </div>

                    </div>
                    <!--</form>-->
                </div>
            </section>

            <%@include file="../partial/include-footer.jsp" %>

        </div>
        <!-- End Main-Wrapper -->

        <%@ include file="../partial/include-js.jsp" %>
        <script>
            <%@ include file="editRoom.js" %>
        </script>

    </body>

</html>