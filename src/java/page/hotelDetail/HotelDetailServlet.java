/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package page.hotelDetail;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Hotel;
import model.Room;
import page.BaseServlet;
import utils.Is;

/**
 *
 * @author RaymondMa
 */
public class HotelDetailServlet extends BaseServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//        String hotelIdStr = request.getPathInfo().replaceFirst("/", "");
        String hotelIdStr = request.getParameter("hotelId");
        Hotel hotel = null;
        if (Is.PositiveInteger(hotelIdStr)) {
            Integer hotelId = Integer.parseInt(hotelIdStr);
            hotel = Hotel.getHotelFromDB(hotelId);

            String hotelImage = Hotel.getHotelImage("" + hotelId);
            request.setAttribute("hotelImage", hotelImage);
            if (hotel == null) {
                request.setAttribute("error_message", "Room Not Found");
                dispatch("/WEB-INF/classes/page/error/404.jsp", request, response);
                return;
            }
        } else {
            request.setAttribute("error_message", hotelIdStr + " is not a valid room id");
        }

        request.setAttribute("hotel", hotel);

        dispatch("/WEB-INF/classes/page/hotelDetail/hotelDetail.jsp", request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
