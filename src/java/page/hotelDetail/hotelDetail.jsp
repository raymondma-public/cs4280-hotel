<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="ex" uri="/WEB-INF/custom.tld"%>

<%-- 
    Document   : hotelDetail
    Created on : Apr 13, 2016, 4:10:34 AM
    Author     : RaymondMa
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!doctype html>
<html lang="en">

    <!-- Mirrored from 188.226.241.135/hotelpetra/homepage_v1/single_post.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 24 Mar 2016 07:46:02 GMT -->
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <title><ex:TranslateTag  lang="${lang}">Hotel Detail</ex:TranslateTag></title>
        <%@ include file="../partial/include-head.jsp" %>
    </head>

    <body>

        <!-- Start Main-Wrapper -->
        <div id="main-wrapper">

            <%@ include file="../partial/include-header.jsp" %>

            <!-- Start Header-Section -->
            <section class="header-section contact">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <h3 class="title-section pull-left">
                                <ex:TranslateTag  lang="${lang}">Hotel</ex:TranslateTag>
                                </h3>
                                <ul class="breadcrumbs custom-list list-inline pull-right">
                                    <li><ex:TranslateTag  lang="${lang}">Home</ex:TranslateTag></li>
                                <li><ex:TranslateTag  lang="${lang}">Hotel Detail</ex:TranslateTag></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </section>
                <!-- End Header-Section -->

                <!-- Start News -->
                <section class="news">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="post">
                                    <div class="post-thumbnail">
                                        <img src="${hotelImage}" alt="" class="img-responsive">
                                    </div>
                                    <div class="post-content">
                                        <header class="post-header">
                                            <h5 class="pull-left title">
                                            ${hotel.name}
                                        </h5>
                                        <!--<span class="pull-right date">21 July 2014</span>-->
                                    </header>
                                    <div class="post-meta">
                                        <span class="category">
                                            <i class="fa fa-tags"></i>
                                            <a href="#">${hotel.hotelType}</a>
                                        </span>
                                        <!--                                        <span class="author">
                                                                                    <i class="fa fa-user"></i>
                                                                                    <a href="#">Admin</a>
                                                                                </span>-->
                                        <!--                                        <span class="comments">
                                                                                    <i class="fa fa-comment"></i>
                                                                                    <a href="#">2 Comments</a>
                                                                                </span>-->
                                    </div>
                                    <p>${hotel.description}</p>

                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </section>
            <!-- End News -->

            <!-- Start Partners -->
            <section class="partners partners-white">
                <div class="container">
                    <div class="row">
                        <div class="preamble col-md-12">
                            <h3>Our Partners</h3>
                        </div>
                        <div class="col-md-12">
                            <div id="clients-slider" class="owl-carousel">
                                <img src="img/company1.png" alt="" class="img-responsive">
                                <img src="img/company2.png" alt="" class="img-responsive">
                                <img src="img/company3.png" alt="" class="img-responsive">
                                <img src="img/company1.png" alt="" class="img-responsive">
                                <img src="img/company2.png" alt="" class="img-responsive">
                                <img src="img/company3.png" alt="" class="img-responsive">
                                <img src="img/company1.png" alt="" class="img-responsive">
                                <img src="img/company2.png" alt="" class="img-responsive">
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- End Partners -->

            <%@include file="../partial/include-footer.jsp" %>

        </div>
        <!-- End Main-Wrapper -->

        <%@ include file="../partial/include-js.jsp" %>
    </body>

    <!-- Mirrored from 188.226.241.135/hotelpetra/homepage_v1/single_post.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 24 Mar 2016 07:46:06 GMT -->
</html>