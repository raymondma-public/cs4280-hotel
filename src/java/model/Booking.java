/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import database.DatabaseOperator;
import exception.BookingRangeNotOkException;
import exception.CannotRemoveException;
import exception.CannotUpdateException;
import exception.ParseErrorException;
import exception.UserNotFoundException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import utils.DatetimeUtil;

/**
 *
 * @author RaymondMa
 */
public class Booking {

    private static boolean bookingRangeOK(String roomId, String start, String end) throws SQLException {

        String sql = "Select \n"
                + "Count(*) as [count]\n"
                + "From room\n"
                + "Where room.id not in(\n"
                + "  select [roomId]\n"
                + "  from [booking]\n"
                + "  where ((startDate>=" + start + ")and (endDate<=" + end + ")) or --start,end\n"
                + "((startDate>=" + start + ")and (endDate<=" + start + ")) or --start,start\n"
                + "((startDate>=" + end + ")and (endDate<=" + end + ")) or --end,end\n"
                + "((startDate<=" + start + ")and (startDate>=" + end + ") and (endDate>=" + end + ") and room.id=" + roomId + " )  --start,end,end\n"
                + "\n"
                + "\n"
                + ") and room.id=" + roomId + "";

        System.out.println(sql);
        Connection con = DatabaseOperator.getInstance().getConnection();

        PreparedStatement ps = con.prepareStatement(sql);
//            ps.setString(1, paymentId);
        ResultSet rs = ps.executeQuery();
        if (rs.next()) {
            int countOK = rs.getInt("count");

            if (countOK > 0) {
                return true;
            } else {
                return false;
            }

//            return tempPayment;
//        }
//        return true;
        }
        return false;
    }

    public static ArrayList<Booking> getBookingsFromDB(String userId) throws SQLException {
        String sql = "select [id],[customerId],[roomId],[paymentId],[valid],[startDate],[endDate],[bookDate]"
                + "from [dbo].[Booking] where [customerId]=?";
        Connection con = DatabaseOperator.getInstance().getConnection();
        PreparedStatement ps = con.prepareStatement(sql);
        ps.setString(1, userId);
        ResultSet rs = ps.executeQuery();
        ArrayList<Booking> bookings = new ArrayList();
        while (rs.next()) {
            Booking tempBooking = null;

            try {
                tempBooking = parseBooking(rs);
                bookings.add(tempBooking);
            } catch (UserNotFoundException ex) {
                Logger.getLogger(Booking.class.getName()).log(Level.SEVERE, null, ex);
            } catch (ParseException ex) {
                Logger.getLogger(Booking.class.getName()).log(Level.SEVERE, null, ex);
            } catch (ParseErrorException ex) {
                Logger.getLogger(Booking.class.getName()).log(Level.SEVERE, null, ex);
            }

        }

        return bookings;
    }

    private static Booking parseBooking(ResultSet rs) throws SQLException, UserNotFoundException, ParseException, ParseErrorException {

        try {
            int id = rs.getInt("id");
            String customerId = rs.getString("customerId");
            int roomId = rs.getInt("roomId");
            String paymentId = rs.getString("paymentId");
            boolean validInt = rs.getBoolean("valid");
            String startDateString = rs.getString("startDate");
            String endDateString = rs.getString("endDate");
            String bookingDateString = rs.getString("bookDate");

            User user = User.getUserById(customerId);
            Room room = Room.getRoom(roomId);
            Payment payment = Payment.getPayment(paymentId);
            boolean valid = false;
//            if (validInt == 1) {
//                valid = true;
//            }
            Calendar startDate = DatetimeUtil.getCalendarFromDatetimeString(startDateString);
            Calendar endDate = DatetimeUtil.getCalendarFromDatetimeString(endDateString);
            Calendar bookingDate = DatetimeUtil.getCalendarFromDatetimeString(bookingDateString);

            Booking resultBooking = new Booking(id, user, room, payment, valid, startDate, endDate, bookingDate);
            return resultBooking;
        } catch (ParseErrorException ex) {
            Logger.getLogger(Booking.class.getName()).log(Level.SEVERE, null, ex);
            throw new ParseErrorException();
        }
    }

    public static Booking getOneBookingFromDB(String bookingId) throws SQLException, ParseErrorException {//

        String sql = "select [id],[customerId],[roomId],[paymentId],[valid],[startDate],[endDate],[bookDate]"
                + "from [dbo].[Booking] where [id]=?";
        Connection con = DatabaseOperator.getInstance().getConnection();
        PreparedStatement ps = con.prepareStatement(sql);
        ps.setString(1, bookingId);
        ResultSet rs = ps.executeQuery();
        Booking booking = null;
        if (rs.next()) {
//            Booking tempBooking = null;

            try {
                booking = parseBooking(rs);
//                bookings.add(tempBooking);
            } catch (UserNotFoundException ex) {
                Logger.getLogger(Booking.class.getName()).log(Level.SEVERE, null, ex);
                throw new ParseErrorException();
            } catch (ParseException ex) {
                Logger.getLogger(Booking.class.getName()).log(Level.SEVERE, null, ex);
                throw new ParseErrorException();
            }

        }

        return booking;
//        return null;
    }

    public static void updateBooking(String bookingId, String startDate, String endDate) throws CannotUpdateException {
        try {
            Connection con = DatabaseOperator.getInstance().getConnection();

            String sql = "UPDATE [dbo].[Booking]\n"
                    + "   SET \n"
                    + "      [startDate] =?\n"
                    + "      ,[endDate] = ?\n"
                    + "      \n"
                    + " WHERE [id]=?";

            PreparedStatement ps
                    = con.prepareStatement(sql);
            ps.setString(1, startDate);
            ps.setString(2, endDate);
            ps.setString(3, bookingId);

            ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(Booking.class.getName()).log(Level.SEVERE, null, ex);
            throw new CannotUpdateException();
        }

    }

    public static void removeBooking(String bookingId) throws CannotRemoveException {
        try {
            Connection con = DatabaseOperator.getInstance().getConnection();

            String sql = "DELETE FROM [dbo].[Booking]\n"
                    + " WHERE [id]=?";

            PreparedStatement ps
                    = con.prepareStatement(sql);
            ps.setString(1, bookingId);

            ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(Booking.class.getName()).log(Level.SEVERE, null, ex);
            throw new CannotRemoveException();
        }

    }

    public static boolean userHasReviewed(String id, String bookingId) throws SQLException {

        Connection con = DatabaseOperator.getInstance().getConnection();
        String sql = "SELECT count(*) as [count]\n"
                + "  FROM [dbo].[bookingReview]\n"
                + "  where creatorId=? and bookingId=?";

        PreparedStatement preparedStatement;
        int reviewed = 0;
//        try {
        preparedStatement = con.prepareStatement(sql);
        preparedStatement.setString(1, id);
        preparedStatement.setString(2, bookingId);

        ResultSet rs = preparedStatement.executeQuery();
        if (rs.next()) {
            reviewed = rs.getInt("count");
            if (reviewed > 0) {
                return true;
            } else {
                return false;
            }

        } else {
            return false;
        }

    }



    public static String getBookingReviewMessageFromDB(String bookingId) throws SQLException {
        String sql = "   SELECT \n"
                + "      [message]\n"
                + "  FROM [dbo].[bookingReview]\n"
                + "  where bookingId=?";
        Connection con = DatabaseOperator.getInstance().getConnection();
        PreparedStatement preparedStatement;
        preparedStatement = con.prepareStatement(sql);
        preparedStatement.setString(1, bookingId);

        ResultSet rs = preparedStatement.executeQuery();
        String message = null;
        if (rs.next()) {
            message = rs.getString("message");
        }
        return message;
    }

    private int id;

//    public Booking(int id, User customer, Room room, Payment payment, boolean valid, Calendar date) {
//        this.id = id;
//        this.customer = customer;
//        this.room = room;
//        this.payment = payment;
//        this.valid = valid;
//        this.bookingDate = date;
//    }
    public void setId(int id) {
        this.id = id;
    }

    public void setCustomer(User customer) {
        this.customer = customer;
    }

    public void setRoom(Room room) {
        this.room = room;
    }

    public void setPayment(Payment payment) {
        this.payment = payment;
    }

    public void setValid(boolean valid) {
        this.valid = valid;
    }

    public void setBookingDate(Calendar bookingDate) {
        this.bookingDate = bookingDate;
    }

    public int getId() {
        return id;
    }

    public User getCustomer() {
        return customer;
    }

    public Room getRoom() {
        return room;
    }

    public Payment getPayment() {
        return payment;
    }

    public boolean isValid() {
        return valid;
    }

    public String getBookingDate() {
        return DatetimeUtil.calendarToString(bookingDate);
    }
    private User customer;
    private Room room;
    private Payment payment;
    private boolean valid;
    private Calendar startDate;

    public String getStartDate() {

        return DatetimeUtil.calendarToString(startDate);
    }

    public void setStartDate(Calendar startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return DatetimeUtil.calendarToString(endDate);
    }

    public void setEndDate(Calendar endDate) {
        this.endDate = endDate;
    }
    private Calendar endDate;
    private Calendar bookingDate;

    public Booking(int id, User customer, Room room, Payment payment, boolean valid, Calendar startDate, Calendar endDate, Calendar bookingDate) {
        this.id = id;
        this.customer = customer;
        this.room = room;
        this.payment = payment;
        this.valid = valid;
        this.startDate = startDate;
        this.endDate = endDate;
        this.bookingDate = bookingDate;
    }

    public static void addBookingToDB(String userId, String roomId, String start, String end) throws SQLException, BookingRangeNotOkException {

        System.out.println("=======addBookingToDB :"+userId+" "+roomId+" "+start+" "+end);
        if (!bookingRangeOK(roomId, start, end)) {//未IMPLEMENT
            throw new BookingRangeNotOkException();
        }

        Connection con = DatabaseOperator.getInstance().getConnection();
        String sql = "INSERT INTO [dbo].[Booking]\n"
                + "           ([customerId]\n"
                + "           ,[roomId]\n"
                + "    \n"
                + "           ,[valid]\n"
                + "           ,[startDate]\n"
                + "           ,[endDate]\n"
                + "           ,[bookDate])\n"
                + "     VALUES\n"
                + "           (?\n"
                + "           ,?\n"
                + "        \n"
                + "           ,1\n"
                + "           ,?\n"
                + "           ,?\n"
                + "           ,GETDATE())";

        System.out.println(sql);
        PreparedStatement preparedStatement;

        //        try {
        preparedStatement = con.prepareStatement(sql);

//            preparedStatement.execute();
        preparedStatement.setString(1, userId);
        preparedStatement.setString(2, roomId);
        preparedStatement.setString(3, start);
        preparedStatement.setString(4, end);
//            preparedStatement.setString(3, roomId);
//            preparedStatement.setString(4, bookmark.getRoom().getHotel().getId());
//            throw new Exception();

        preparedStatement.execute();

    }

    public static int getLastModifiedId() throws SQLException {
        Connection con = DatabaseOperator.getInstance().getConnection();
        String getPaymentIdSQL = "SELECT @@IDENTITY AS [id]";
        PreparedStatement ps2 = con.prepareStatement(getPaymentIdSQL);
        ResultSet rs = ps2.executeQuery();
        int id = 0;
        if (rs.next()) {
            id = rs.getInt("id");
        } else {
            throw new SQLException();
        }
        return id;
    }

    public static void addBookingReview(String id, String bookingId, String message) throws SQLException {
        Connection con = DatabaseOperator.getInstance().getConnection();
        String sql = "INSERT INTO [dbo].[bookingReview]\n"
                + "           ([creatorId]\n"
                + "           ,[bookingId]\n"
                + "           ,[message])\n"
                + "     VALUES\n"
                + "           (?\n"
                + "           ,?\n"
                + "           ,?)";

        PreparedStatement preparedStatement;
        int total = 0;
//        try {
        preparedStatement = con.prepareStatement(sql);
        preparedStatement.setString(1, id);
        preparedStatement.setString(2, bookingId);
        preparedStatement.setString(3, message);

        preparedStatement.executeUpdate();
    }

}
