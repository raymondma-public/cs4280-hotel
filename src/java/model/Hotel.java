/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import database.DatabaseOperator;
import exception.CannotRemoveException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import utils.Hash;

/**
 *
 * @author RaymondMa
 */
public class Hotel {

    public static void removeHotel(String hotelId) throws CannotRemoveException {

        try {
            Connection con = DatabaseOperator.getInstance().getConnection();

            String sql = "DELETE FROM [dbo].[Hotel]\n"
                    + " WHERE [id]=?";

            PreparedStatement ps
                    = con.prepareStatement(sql);
            ps.setString(1, hotelId);

            ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(Booking.class.getName()).log(Level.SEVERE, null, ex);
            throw new CannotRemoveException();
        }

    }

    private double latitude;
    private double longitude;

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public static Hotel getHotelFromDB(int hotelId) {

        Hotel hotel = null;
        Connection con = DatabaseOperator.getInstance().getConnection();
        String selectSQL = "    SELECT [id]  ,[hotelType]  ,[name]  ,[phone]  ,[description]  ,[latitude],[longitude]\n"
                + "\n"
                + "  FROM [dbo].[hotel] where [id]=?";

        PreparedStatement preparedStatement;
        try {
            //        try {
            preparedStatement = con.prepareStatement(selectSQL);

            preparedStatement.setInt(1, hotelId);
//            preparedStatement.execute();
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {

                hotel = Hotel.parseHotelFromDBResultSet(rs);

            }

        } catch (SQLException ex) {
            Logger.getLogger(Hotel.class.getName()).log(Level.SEVERE, null, ex);
        }
        return hotel;
    }

    public static int getLastModifiedId() throws SQLException {
        Connection con = DatabaseOperator.getInstance().getConnection();
        String getPaymentIdSQL = "SELECT @@IDENTITY AS [id]";
        PreparedStatement ps2 = con.prepareStatement(getPaymentIdSQL);
        ResultSet rs = ps2.executeQuery();
        int id = 0;
        if (rs.next()) {
            id = rs.getInt("id");
        } else {
            throw new SQLException();
        }
        return id;
    }

    public static void updateHotel(String hotelId, String hotelType, String name, String phone, String description, double latitude, double longitude) throws SQLException {

        String sql = "UPDATE [dbo].[hotel]\n"
                + "   SET [hotelType] = ?\n"
                + "      ,[name] = ?\n"
                + "      ,[phone] = ?\n"
                + "      ,[description] = ?\n"
                + "      ,[latitude] = ?\n"
                + "      ,[longitude] = ?\n"
                + " WHERE [id]=?";
        Connection con = DatabaseOperator.getInstance().getConnection();
        PreparedStatement ps = con.prepareStatement(sql);
        ps.setString(1, hotelType);
        ps.setString(2, name);
        ps.setString(3, phone);
        ps.setString(4, description);
        ps.setDouble(5, latitude);
        ps.setDouble(6, longitude);
        ps.setString(7, hotelId);
        ps.executeUpdate();

    }

    public static void addHotel(String hotelType, String name, String phone, String description, double latitude, double longitude) throws SQLException {

        String sql = "INSERT INTO [dbo].[hotel]\n"
                + "           ([hotelType]\n"
                + "           ,[name]\n"
                + "           ,[phone]\n"
                + "           ,[description]\n"
                + "           ,[latitude]\n"
                + "           ,[longitude])\n"
                + "     VALUES\n"
                + "           (?\n"
                + "           ,?\n"
                + "           ,?\n"
                + "           ,?\n"
                + "           ,?\n"
                + "           ,?)";
        Connection con = DatabaseOperator.getInstance().getConnection();
        PreparedStatement ps = con.prepareStatement(sql);
        ps.setString(1, hotelType);
        ps.setString(2, name);
        ps.setString(3, phone);
        ps.setString(4, description);
        ps.setDouble(5, latitude);
        ps.setDouble(6, longitude);
        ps.executeUpdate();

    }

    private int id;
    private String hotelType;

    public Hotel(int id, String hotelType, String name, String phone, String description, double latitude, double longitude) {
        this.id = id;
        this.hotelType = hotelType;
        this.name = name;
        this.phone = phone;
        this.description = description;
//        this.location = location;
        this.latitude = latitude;
        this.longitude = longitude;
    }
    private String name;
    private String phone;
    private String description;
    private Location location;

    public void setId(int id) {
        this.id = id;
    }

    public void setHotelType(String hotelType) {
        this.hotelType = hotelType;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public int getId() {
        return id;
    }

    public String getHotelType() {
        return hotelType;
    }

    public String getName() {
        return name;
    }

    public String getPhone() {
        return phone;
    }

    public String getDescription() {
        return description;
    }

    public Location getLocation() {
        return location;
    }

    public static ArrayList<Hotel> getHotelListFromDB(int pageNo, int pageSize) {
        ArrayList<Hotel> hotelAList = new ArrayList();

        Connection con = DatabaseOperator.getInstance().getConnection();
//        String selectSQL = " SELECT top " + pageNo * pageSize + "\n"
//                + "[id]\n"
//                + "      ,[hotelType]\n"
//                + "      ,[name]\n"
//                + "      ,[phone]\n"
//                + "      ,[description]\n"
//                + "      ,[latitude]\n"
//                + ",[longitude]"
//                + "  FROM [dbo].[hotel]\n"
//                + "  where id not in\n"
//                + "  (SELECT top " + (pageNo - 1) * pageSize + "\n"
//                + "  [id]\n"
//                + "  FROM [dbo].[hotel])";

        String selectSQL = " SELECT top " + pageNo * pageSize + "\n"
                + "[id]\n"
                + "      ,[hotelType]\n"
                + "      ,[name]\n"
                + "      ,[phone]\n"
                + "      ,[description]\n"
                + "      ,[latitude]\n"
                + ",[longitude]  \n"
                + "FROM [dbo].[hotel]\n"
                + "\n"
                + "where [id] not in(\n"
                + "  SELECT top " + (pageNo - 1) * pageSize + "  [id]\n"
                + "  FROM [dbo].[hotel]\n"
                + " \n"
                + "   Order by [id]\n"
                + ")";

        System.out.println(selectSQL);
        PreparedStatement preparedStatement;
        try {
            //        try {
            preparedStatement = con.prepareStatement(selectSQL);

//            preparedStatement.execute();
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {

                Hotel tempHotel = Hotel.parseHotelFromDBResultSet(rs);
                hotelAList.add(tempHotel);
            }

        } catch (SQLException ex) {
            Logger.getLogger(Hotel.class.getName()).log(Level.SEVERE, null, ex);
        }
        return hotelAList;
    }

    private static Hotel parseHotelFromDBResultSet(ResultSet rs) {
        Hotel resultHotel = null;
        try {
            int id = rs.getInt("id");
            String hotelType = rs.getString("hotelType");
            String name = rs.getString("name");
            String phone = rs.getString("phone");
            String description = rs.getString("description");
            double latitude = rs.getDouble("latitude");
            double longitude = rs.getDouble("longitude");
//            String locationId = rs.getString("locationId");
//            Location location = Location.getLocation(locationId);
            resultHotel = new Hotel(id, hotelType, name, phone, description, latitude, longitude);
        } catch (SQLException ex) {
            Logger.getLogger(Hotel.class.getName()).log(Level.SEVERE, null, ex);
        }
        return resultHotel;
    }

    public static int getTotalPageNoFromDB(int noOfRecordPerPage) {
        int noOfRecord = Hotel.getTotalNumRecordFromDB();
        return (int) Math.ceil((double) ((double) noOfRecord / (double) noOfRecordPerPage));

    }

//    public static int getTotalPageNoFromDB(String hotelId, int noOfRecordPerPage) {
//        int noOfRecord = Hotel.getTotalNumRecordFromDB(hotelId);
//        return (int) Math.ceil((double) ((double) noOfRecord / (double) noOfRecordPerPage));
//
//    }
    public static int getTotalNumRecordFromDB() {
        System.out.println("=============getTotalNumRecordFromDB");
        Connection con = DatabaseOperator.getInstance().getConnection();
        String selectSQL = "SELECT count(*) as total\n"
                + "  FROM [dbo].[hotel]\n";
        PreparedStatement preparedStatement;

        int total = 0;
        try {
            preparedStatement = con.prepareStatement(selectSQL);
//            preparedStatement.setString(1, hotelId);
            ResultSet rs = preparedStatement.executeQuery();
            if (rs.next()) {
                total = rs.getInt("total");
            }
        } catch (SQLException ex) {
            Logger.getLogger(Hotel.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println("total:" + total);
        return total;

    }

    public static ArrayList<Hotel> getHotelListFromDB() {
        ArrayList<Hotel> hotelAList = new ArrayList();

        Connection con = DatabaseOperator.getInstance().getConnection();
        String selectSQL = " SELECT"
                + "[id]\n"
                + "      ,[hotelType]\n"
                + "      ,[name]\n"
                + "      ,[phone]\n"
                + "      ,[description]\n"
                + "      ,[latitude]\n"
                + ",[longitude]\n"
                + "  FROM [dbo].[hotel]\n";

        PreparedStatement preparedStatement;
        try {
            //        try {
            preparedStatement = con.prepareStatement(selectSQL);

//            preparedStatement.execute();
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {

                Hotel tempHotel = Hotel.parseHotelFromDBResultSet(rs);
                hotelAList.add(tempHotel);
            }

        } catch (SQLException ex) {
            Logger.getLogger(Hotel.class.getName()).log(Level.SEVERE, null, ex);
        }
        return hotelAList;
    }

//    public static int getTotalNumRecordFromDB(String hotelId) {
//      
//    }
    public static String getHotelImage(String hotelId) {
        try {
            String sql = "SELECT top 1 icon\n"
                    + "  FROM [dbo].[hotel],[room]\n"
                    + "  where room.hotelId=hotel.id\n"
                    + "  and hotel.id=?\n"
                    + "  and icon  is not NULL\n"
                    + "  and icon <> ''\n"
                    + "  order by NEWID()";

            PreparedStatement preparedStatement;
//        try {
//        try {
            Connection con = DatabaseOperator.getInstance().getConnection();
            preparedStatement = con.prepareStatement(sql);
            preparedStatement.setString(1, hotelId);

//            preparedStatement.execute();
            ResultSet rs = preparedStatement.executeQuery();
            if (rs.next()) {

                Hotel tempHotel = Hotel.parseHotelFromDBResultSet(rs);
                return rs.getString("icon");
            }

        } catch (SQLException ex) {
            Logger.getLogger(Hotel.class.getName()).log(Level.SEVERE, null, ex);
            return "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSvj78Q6M19k8GeCWnrkUdQyAhV2lMHruSG6GRiMgDMW9Tj66a_";
        }
        return "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSvj78Q6M19k8GeCWnrkUdQyAhV2lMHruSG6GRiMgDMW9Tj66a_";
    }
}
