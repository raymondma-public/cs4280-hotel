/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import database.DatabaseOperator;
import org.apache.commons.validator.ValidatorException;
import utils.Is;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author RaymondMa
 */
public class Price {

    static Price getPrice(int priceId) {

        Price resultPrice = null;
        Connection con = DatabaseOperator.getInstance().getConnection();
        String selectSQL = "        SELECT [id]\n"
                + "      ,[date]\n"
                + "      ,[isAdult]\n"
                + "      ,[price]\n"
                + "  FROM [dbo].[price] where [id]=?";

        PreparedStatement preparedStatement;
        try {
            //        try {
            preparedStatement = con.prepareStatement(selectSQL);

            preparedStatement.setInt(1, priceId);
//            preparedStatement.execute();
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {

                resultPrice = Price.parsePriceFromDBResultSet(rs);

            }

        } catch (SQLException ex) {
            Logger.getLogger(Hotel.class.getName()).log(Level.SEVERE, null, ex);
        }
        return resultPrice;
    }

    public static int addPrice(boolean isAdult, String price) throws ValidatorException, SQLException {
        if (!Is.PositiveFloat(price)) {
            throw new ValidatorException("Price should be an positive number");
        }
        return DatabaseOperator.getInstance().addPrice(Float.parseFloat(price));
    }

    private static Price parsePriceFromDBResultSet(ResultSet roomListResults) {

        Price resultPrice = null;
        try {
            int id = roomListResults.getInt("id");
            String date = roomListResults.getString("date");
            boolean isAdult = roomListResults.getBoolean("isAdult");
            float price = roomListResults.getFloat("price");

            resultPrice = new Price(id, null, isAdult, price); //呢到 date
        } catch (SQLException ex) {
            Logger.getLogger(Price.class.getName()).log(Level.SEVERE, null, ex);
        }
        return resultPrice;

    }

    public static void updatePrice(String priceId, String priceForAdult) throws SQLException {
        String sql = "UPDATE [dbo].[price]\n"
                + "   SET [price] = \n" + priceForAdult
                + " WHERE id=" + priceId;
        Connection con = DatabaseOperator.getInstance().getConnection();
        Statement statement = con.createStatement();
        statement.executeUpdate(sql);

    }
    private int id;
    private Calendar date;

    public Price(int id, Calendar date, boolean isAdult, float price) {
        this.id = id;
        this.date = date;
        this.isAdult = isAdult;
        this.price = price;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Calendar getDate() {
        return date;
    }

    public void setDate(Calendar date) {
        this.date = date;
    }

    public boolean isIsAdult() {
        return isAdult;
    }

    public void setIsAdult(boolean isAdult) {
        this.isAdult = isAdult;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }
    private boolean isAdult;
    private float price;

    public String toString() {
        return Float.toString(price);
    }

    public boolean getIsAdult() {
        return isAdult;
    }
}
