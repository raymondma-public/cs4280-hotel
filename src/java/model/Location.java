/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import database.DatabaseOperator;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author RaymondMa
 */
public class Location {

    static Location getLocation(String locationId) {

        Location resultLocation = null;
        Connection con = DatabaseOperator.getInstance().getConnection();
        String selectSQL = "       SELECT [id]\n"
                + "      ,[lat]\n"
                + "      ,[long]\n"
                + "      ,[country]\n"
                + "      ,[address]\n"
                + "  FROM [dbo].[location]\n"
                + "  where id=1";

        PreparedStatement preparedStatement;
        try {
            //        try {
            preparedStatement = con.prepareStatement(selectSQL);

//            preparedStatement.execute();
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {

                resultLocation = Location.parseLocationFromDBResultSet(rs);
//                hotelAList.add(tempHotel);

            }

        } catch (SQLException ex) {
            Logger.getLogger(Hotel.class.getName()).log(Level.SEVERE, null, ex);
        }
        return resultLocation;
    }

    private static Location parseLocationFromDBResultSet(ResultSet rs) {
        Location resultLocation = null;
        try {

            int id = rs.getInt("id");

            float latitude = rs.getFloat("lat");
            float longitude = rs.getFloat("long");
            String country = rs.getString("country");
            String fullAddress = rs.getString("address");

            resultLocation = new Location(id, latitude, longitude, country, fullAddress);

        } catch (SQLException ex) {
            Logger.getLogger(Location.class.getName()).log(Level.SEVERE, null, ex);
        }
        return resultLocation;
    }
    private int id;
    private float latitude;
    private float longitude;
    private String country;
    private String fullAddress;

    public Location(int id, float latitude, float longitude, String country, String fullAddress) {
        this.id = id;
        this.latitude = latitude;
        this.longitude = longitude;
        this.country = country;
        this.fullAddress = fullAddress;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public float getLatitude() {
        return latitude;
    }

    public void setLatitude(float latitude) {
        this.latitude = latitude;
    }

    public float getLongitude() {
        return longitude;
    }

    public void setLongitude(float longitude) {
        this.longitude = longitude;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getFullAddress() {
        return fullAddress;
    }

    public void setFullAddress(String fullAddress) {
        this.fullAddress = fullAddress;
    }

}
