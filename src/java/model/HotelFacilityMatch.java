/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author RaymondMa
 */
public class HotelFacilityMatch {

    private int id;
    private Hotel hotel;
    private Facility facility;

    public HotelFacilityMatch(int id, Hotel hotel, Facility facility) {
        this.id = id;
        this.hotel = hotel;
        this.facility = facility;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setHotel(Hotel hotel) {
        this.hotel = hotel;
    }

    public void setFacility(Facility facility) {
        this.facility = facility;
    }

    public int getId() {
        return id;
    }

    public Hotel getHotel() {
        return hotel;
    }

    public Facility getFacility() {
        return facility;
    }
}
