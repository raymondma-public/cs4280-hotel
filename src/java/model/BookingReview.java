/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import database.DatabaseOperator;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import utils.DatetimeUtil;

/**
 *
 * @author RaymondMa
 */
public class BookingReview {

    private static BookingReview parse(ResultSet rs) throws SQLException, ParseException {
        String message = rs.getString("message");
        String email = rs.getString("email");
        String startDateString = rs.getString("startDate");
        String endDateString = rs.getString("endDate");
        Calendar startDate = DatetimeUtil.getCalendarFromDatetimeString(startDateString);
        Calendar endDate = DatetimeUtil.getCalendarFromDatetimeString(endDateString);
        return new BookingReview(message, email, startDate, endDate);
    }

    String message;
    String email;
    Calendar startDate;
    Calendar endDate;

    public BookingReview(String message, String email, Calendar startDate, Calendar endDate) {
        this.message = message;
        this.email = email;
        this.startDate = startDate;
        this.endDate = endDate;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getStartDate() {
        return DatetimeUtil.calendarToString(startDate);
    }

    public void setStartDate(Calendar startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return DatetimeUtil.calendarToString(endDate);
    }

    public void setEndDate(Calendar endDate) {
        this.endDate = endDate;
    }

    public static ArrayList<BookingReview> getBookingReviewListMessageFromDB(String roomId) throws SQLException {
        String sql = "SELECT [bookingReview].[id]\n"
                + "      ,[creatorId]\n"
                + "      ,[bookingId]\n"
                + "      ,[message]\n"
                + "	  ,[roomId]\n"
                + "	  ,[user].[email]\n"
                + "	  ,startDate\n"
                + "	  ,endDate\n"
                + "  FROM [dbo].[bookingReview], [booking],[user]\n"
                + "      where [bookingReview].[bookingId]=[booking].[id]\n"
                + "	  and roomId=?\n"
                + "  and creatorId=[user].id";
        Connection con = DatabaseOperator.getInstance().getConnection();
        PreparedStatement preparedStatement;
        preparedStatement = con.prepareStatement(sql);
        preparedStatement.setString(1, roomId);

        ResultSet rs = preparedStatement.executeQuery();

        ArrayList<BookingReview> resultList = new ArrayList();
        String message = null;
        int count=0;
        while (rs.next()) {
            count++;
            System.out.println("count:"+count);
            BookingReview review;
            try {
                review = parse(rs);
                resultList.add(review);
            } catch (ParseException ex) {
                Logger.getLogger(BookingReview.class.getName()).log(Level.SEVERE, null, ex);
            }

        }
        return resultList;
    }
}
