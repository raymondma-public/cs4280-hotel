/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.Calendar;

/**
 *
 * @author RaymondMa
 */
public class Comment {

    public Comment(int id, Calendar date, String title, String content, Comment commment) {
        this.id = id;
        this.date = date;
        this.title = title;
        this.content = content;
        this.commment = commment;
    }
    private int id;
    private Calendar date;

    public void setId(int id) {
        this.id = id;
    }

    public void setDate(Calendar date) {
        this.date = date;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public void setCommment(Comment commment) {
        this.commment = commment;
    }

    public int getId() {
        return id;
    }

    public Calendar getDate() {
        return date;
    }

    public String getTitle() {
        return title;
    }

    public String getContent() {
        return content;
    }

    public Comment getCommment() {
        return commment;
    }
    private String title;
    private String content;
    private Comment commment;
}
