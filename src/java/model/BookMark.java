/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import database.DatabaseOperator;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import utils.DatetimeUtil;

/**
 *
 * @author RaymondMa
 */
public class BookMark {

    private int id;

    public BookMark(int id, User customer, Room room, Calendar date) {
        this.id = id;
        this.customer = customer;
        this.room = room;
        this.date = date;
    }
    private User customer;
    private Room room;
    private Calendar date;

    public void setId(int id) {
        this.id = id;
    }

    public void setCustomer(User customer) {
        this.customer = customer;
    }

    public void setRoom(Room room) {
        this.room = room;
    }

    public void setDate(Calendar date) {
        this.date = date;
    }

    public int getId() {
        return id;
    }

    public User getCustomer() {
        return customer;
    }

    public Room getRoom() {
        return room;
    }

    public String getDate() {
        Calendar cal = date;
//        cal.add(Calendar.DATE, 1);

        return DatetimeUtil.calendarToString(cal);
    }

    public static void addBookMarkToDB(BookMark bookmark) {
        Connection con = DatabaseOperator.getInstance().getConnection();
        String sql = "INSERT INTO [dbo].[BookMark]\n"
                + "           ([customerId]\n"
                + "           ,[hotelId]\n"
                + "           ,[roomId]\n"
                + "           ,[date])\n"
                + "     VALUES\n"
                + "           (?"
                + "           ,?"
                + "           ,?"
                + "           ,GETDATE())";

        PreparedStatement preparedStatement;
        try {
            //        try {
            preparedStatement = con.prepareStatement(sql);

//            preparedStatement.execute();
            preparedStatement.setString(1, bookmark.getCustomer().getId());
            preparedStatement.setInt(2, bookmark.getRoom().getHotel().getId());
            preparedStatement.setInt(3, bookmark.getRoom().getId());
//            preparedStatement.setString(4, bookmark.getRoom().getHotel().getId());
//            throw new Exception();

            preparedStatement.execute();

        } catch (SQLException ex) {
            Logger.getLogger(BookMark.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void addBookMarkToDB(String customerId, String roomId) {
        Connection con = DatabaseOperator.getInstance().getConnection();
        String sql = "INSERT INTO [dbo].[BookMark]\n"
                + "           ([customerId]\n"
                //                + "           ,[hotelId]\n"
                + "           ,[roomId]\n"
                + "           ,[date])\n"
                + "     VALUES\n"
                + "           (?"
                //                + "           ,?"
                + "           ,?"
                + "           ,GETDATE())";

        PreparedStatement preparedStatement;
        try {
            //        try {
            preparedStatement = con.prepareStatement(sql);

//            preparedStatement.execute();
            preparedStatement.setString(1, customerId);
            preparedStatement.setString(2, roomId);
//            preparedStatement.setString(3, roomId);
//            preparedStatement.setString(4, bookmark.getRoom().getHotel().getId());
//            throw new Exception();

            preparedStatement.execute();

        } catch (SQLException ex) {
            Logger.getLogger(BookMark.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void removeBookMarkFromDB(String customerId, String roomId) {
        Connection con = DatabaseOperator.getInstance().getConnection();
        String sql = "DELETE FROM [dbo].[BookMark]\n"
                + "      WHERE customerId=? and \n"
                + "	  roomId=?";

        PreparedStatement preparedStatement;
        try {
            //        try {
            preparedStatement = con.prepareStatement(sql);

//            preparedStatement.execute();
            preparedStatement.setString(1, customerId);
            preparedStatement.setString(2, roomId);
//            preparedStatement.setString(3, roomId);
//            preparedStatement.setString(4, bookmark.getRoom().getHotel().getId());
//            throw new Exception();

            preparedStatement.executeUpdate();

        } catch (SQLException ex) {
            Logger.getLogger(BookMark.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static ArrayList<BookMark> getBookMarksFromDB(String customerId) {
        Connection con = DatabaseOperator.getInstance().getConnection();
        String sql = "SELECT [id]\n"
                + "      ,[customerId]\n"
                + "      ,[hotelId]\n"
                + "      ,[roomId]\n"
                + "      ,[date]\n"
                + "  FROM [dbo].[BookMark]"
                + "  where customerId=?";

        ArrayList<BookMark> resultList = new ArrayList();
        PreparedStatement preparedStatement;
        try {
            //        try {
            preparedStatement = con.prepareStatement(sql);

//            preparedStatement.execute();
            preparedStatement.setString(1, customerId);

            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {

                int id = rs.getInt("id");
                int roomId = rs.getInt("roomId");
                Room tempRoom = Room.getRoom(roomId);
                String dateString = rs.getString("date");
                Calendar tempCal = DatetimeUtil.getCalendarFromDatetimeString(dateString);
                BookMark bookmark = new BookMark(id, null, tempRoom, tempCal);

                resultList.add(bookmark);
            }

        } catch (SQLException ex) {
            Logger.getLogger(BookMark.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParseException ex) {
            Logger.getLogger(BookMark.class.getName()).log(Level.SEVERE, null, ex);
        }
        return resultList;
    }

//    public static  addRoomToBookmarkModel(ArrayList<BookMark> bookmarks) {
//        for(BookMark bookmark:bookmarks){
//            Room.getRoom(bookmark.getRoom().getId());
//        }
//        
//    }
}
