package model;

import database.DatabaseOperator;
import exception.CannotRemoveException;
import org.apache.commons.validator.ValidatorException;
import utils.Is;

import java.sql.*;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Room {

    public static void removeRoom(String roomId) throws CannotRemoveException {

        try {
            Connection con = DatabaseOperator.getInstance().getConnection();

            String sql = "DELETE FROM [dbo].[Room]\n"
                    + " WHERE [id]=?";

            PreparedStatement ps
                    = con.prepareStatement(sql);
            ps.setString(1, roomId);

            ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(Booking.class.getName()).log(Level.SEVERE, null, ex);
            throw new CannotRemoveException();
        }

    }

    public static void updateRoom(String roomId, String purpose, String roomNumber, String description, String noOfBed, String maxNoOfAdult, String maxNoOfChildren, int hotelId) throws SQLException {

        String sql = "UPDATE [dbo].[room]\n"
                + "   SET "
                + "      [maxNoOfAdult] = " + maxNoOfAdult
                + "      ,[maxNoOfChildren] =" + maxNoOfChildren
                + "      ,[purpose] ='" + purpose + "'"
                + "      ,[description] = '" + description + "'"
                + "      ,[roomNumber] = '" + roomNumber + "'"
                + "      ,[hotelId] = " + hotelId
                + "      ,[noOfBed] = " + noOfBed
                //                + "      ,[icon] = \n"
                + " WHERE [id]=" + roomId;
        System.out.println(sql);
        Connection con = DatabaseOperator.getInstance().getConnection();
        PreparedStatement ps
                = con.prepareStatement(sql);

        ps.executeUpdate();
    }

    private int id;
    private Price price;
    private int maxNoOfAdult;
    private int maxNoOfChildren;
    private String purpose;
    private String description;
    private String roomNumber;
    private Hotel hotel;
    private int noOfBed;
    private String iconURL;
    private ArrayList<Facility> inRoomFacilities = new ArrayList();
    private boolean bookMarked = false;
    private float rating = 0;

    public float getRating() {
        return rating;
    }

    public void setRating(float rating) {
        this.rating = rating;
    }

    public boolean isBookMarked() {
        return bookMarked;
    }

    public void setBookMarked(boolean bookMarked) {
        this.bookMarked = bookMarked;
    }

    /* Constructor */
    public Room(int id, Price price, int maxNoOfAdult, int maxNoOfChildren, String purpose, String description, String roomNumber, Hotel hotel, int noOfBed, String iconURL, float rating) {
        this.id = id;
        this.price = price;
        this.maxNoOfAdult = maxNoOfAdult;
        this.maxNoOfChildren = maxNoOfChildren;
        this.purpose = purpose;
        this.description = description;
        this.roomNumber = roomNumber;
        this.hotel = hotel;
        this.noOfBed = noOfBed;
        this.iconURL = iconURL;
        this.rating = rating;
    }

    /* Setter and Getter */
    public String getIconURL() {
        return iconURL;
    }

    public void setIconURL(String iconURL) {
        this.iconURL = iconURL;
    }

    public int getNoOfBed() {
        return noOfBed;
    }

    public void setNoOfBed(int noOfBed) {
        this.noOfBed = noOfBed;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Price getPrice() {
        return price;
    }

    public void setPrice(Price price) {
        this.price = price;
    }

    public int getMaxNoOfAdult() {
        return maxNoOfAdult;
    }

    public void setMaxNoOfAdult(int maxNoOfAdult) {
        this.maxNoOfAdult = maxNoOfAdult;
    }

    public int getMaxNoOfChildren() {
        return maxNoOfChildren;
    }

    public void setMaxNoOfChildren(int maxNoOfChildren) {
        this.maxNoOfChildren = maxNoOfChildren;
    }

    public String getPurpose() {
        return purpose;
    }

    public void setPurpose(String purpose) {
        this.purpose = purpose;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getRoomNumber() {
        return roomNumber;
    }

    public void setRoomNumber(String roomNumber) {
        this.roomNumber = roomNumber;
    }

    public Hotel getHotel() {
        return hotel;
    }

    public void setHotel(Hotel hotel) {
        this.hotel = hotel;
    }

    //    public void setInRoomFacilities(ArrayList<Facility> inRoomFacilities) {
//        this.inRoomFacilities = inRoomFacilities;
//    }
    /* Helper function */
    public void addFacilities(Facility facility) {
        inRoomFacilities.add(facility);
    }

    public ArrayList<Facility> getInRoomFacilities() {
        return inRoomFacilities;
    }


    /* Database interaction */
    private static Room parseRoomFromDBResultSet(ResultSet roomListResults) {

        Room resultRoom = null;
        try {
            int uid = roomListResults.getInt("id");
//            if (!containRoomIds.contains(uid)) {//not contain
//                containRoomIds.add(uid);
            int maxNoOfAdult = roomListResults.getInt("maxNoOfAdult");
            int maxNoOfChildren = roomListResults.getInt("maxNoOfChildren");
            String purpose = roomListResults.getString("purpose");
            String description = roomListResults.getString("description");
            String roomNumber = roomListResults.getString("roomNumber");
            int noOfBed = roomListResults.getInt("noOfBed");
            String iconURL = roomListResults.getString("icon");
            int priceId = roomListResults.getInt("priceId");
            Price tempPrice = null;
            if (!roomListResults.wasNull()) {
                tempPrice = Price.getPrice(priceId);
            }
            int hotelId = roomListResults.getInt("hotelId");
            Hotel tempHotel = null;
            if (!roomListResults.wasNull()) {
                tempHotel = Hotel.getHotelFromDB(hotelId);
            }
            float rating = getRoomRatingFromDB("" + uid);
            resultRoom = new Room(uid, tempPrice, maxNoOfAdult, maxNoOfChildren, purpose, description, roomNumber, tempHotel, noOfBed, iconURL, rating);
            Facility.addFacilityToRoom(resultRoom);

        } catch (SQLException ex) {
            Logger.getLogger(Room.class.getName()).log(Level.SEVERE, null, ex);
        }

        return resultRoom;
    }

    public static ArrayList<Room> getRoomList(Integer page, Integer pageSize, String startDateString, String endDateString, String noOfAdultString, String noOfChildrenString, String priceFromString, String priceToString, String[] facilities) {
        return getRoomList(null, page, pageSize, startDateString, endDateString, noOfAdultString, noOfChildrenString, priceFromString, priceToString, facilities);
    }

    public static ArrayList<Room> getRoomListForManagement(String hotelId) {
        String sql = "SELECT [room].[id]\n"
                + "      ,[room].[priceId]\n"
                + "      ,[room].[maxNoOfAdult]\n"
                + "      ,[room].[maxNoOfChildren]\n"
                + "      ,[room].[purpose]\n"
                + "      ,[room].[description]\n"
                + "      ,[room].[roomNumber]\n"
                + "      ,[room].[hotelId]\n"
                + "      ,[room].[noOfBed]\n"
                + "      ,[room].[icon]\n"
                + "from [room] where [room].[hotelId]=" + hotelId;

        System.out.println(sql);
        return getRoomListHelper(sql);
    }

    public static ArrayList<Room> getRoomList(String hotelId, Integer page, Integer pageSize, String startDateString, String endDateString, String noOfAdultString, String noOfChildrenString, String priceFromString, String priceToString, String[] facilities) {

        String earlierThan = "<=";
        String laterThan = ">=";
        System.out.println("============getRoomList");
        String extraSQL = hotelId != null ? ("  [room].[hotelId]=" + hotelId + " and ") : "";
        String extraSQL2 = hotelId != null ? (" where [room].[hotelId]=" + hotelId + "  ") : "";
//        String sql = "SELECT Top " + page * pageSize + "\n"
//                + "		[id]\n"
//                + "      ,[priceId]\n"
//                + "      ,[maxNoOfAdult]\n"
//                + "      ,[maxNoOfChildren]\n"
//                + "      ,[purpose]\n"
//                + "      ,[description]\n"
//                + "      ,[roomNumber]\n"
//                + "      ,[hotelId]\n"
//                + "      ,[noOfBed]\n"
//                + "      ,[icon]\n"
//                + "  FROM [dbo].[room]\n where" + extraSQL
//                + "\n"
//                + " [id] not in\n"
//                + "(SELECT Top " + ((page - 1) * pageSize) + "\n"
//                + "		[id]\n"
//                + "     \n"
//                + "  FROM [dbo].[room]" + extraSQL2 + ")";

        String sql = "SELECT distinct  top " + pageSize + "[room].[id]\n"
                + "      ,[room].[priceId]\n"
                + "      ,[room].[maxNoOfAdult]\n"
                + "      ,[room].[maxNoOfChildren]\n"
                + "      ,[room].[purpose]\n"
                + "      ,[room].[description]\n"
                + "      ,[room].[roomNumber]\n"
                + "      ,[room].[hotelId]\n"
                + "      ,[room].[noOfBed]\n"
                + "      ,[room].[icon]\n"
                + "  FROM [dbo].[room] left outer join [booking]on [room].[id]=[Booking].roomId\n"
                + "  left outer join  [InRoomFacilityMatch] on [room].[Id]=[InRoomFacilityMatch].[roomId] \n"
                + "\n"
                + "  left outer join Facility on	[InRoomFacilityMatch].[facilityId]=[facility].[id]\n"
                + "  left outer join [Price] on[room].[priceId]=[price].[Id]\n"
                + "  where 1=1";
        if (hotelId != null) {
            sql += "	and [room].[hotelId]=" + hotelId;
        }

        if (priceFromString != null && priceFromString != null) {
            sql += "	and [price].[price]>=" + priceFromString + " " + "and [price].[price]<=" + priceToString + " --priceFrom  ,priceTo\n";
        }
        if (noOfAdultString != null) {

            sql += "\n"
                    + "	and [room].[maxNoOfAdult]>=" + noOfAdultString;
        }

        if (noOfChildrenString != null) {
            sql += " and [room].[maxNoOfChildren]>=" + noOfChildrenString + " --noOfAdultString ,noOfChildrenString\n"
                    + "\n";
        }
        if (startDateString != null && endDateString != null) {
            sql += "	and room.id not in(\n"
                    + "  select [roomId]\n"
                    + "  from [booking]\n"
                    + "  where ((startDate " + earlierThan + " '" + startDateString + "')and (endDate" + laterThan + "'" + endDateString + "')) or --start,end\n"
                    + "((startDate " + earlierThan + "'" + startDateString + "')and (endDate" + laterThan + "'" + startDateString + "')) or --start,start\n"
                    + "((startDate" + earlierThan + "'" + endDateString + "')and (endDate" + laterThan + "'" + endDateString + "')) or --end,end\n"
                    + "((startDate" + laterThan + "'" + startDateString + "')and (startDate" + earlierThan + "'" + endDateString + "') and (endDate" + earlierThan + "'" + endDateString + "') )  --start,end,end\n"
                    + "\n"
                    + "\n"
                    + ") ";
        }

        sql += "and [room].[id] not in(";

        sql += "SELECT distinct top " + ((page - 1) * pageSize) + "[room].[id]\n"
                + "  FROM [dbo].[room] left outer join [booking]on [room].[id]=[Booking].roomId\n"
                + "  left outer join  [InRoomFacilityMatch] on [room].[Id]=[InRoomFacilityMatch].[roomId] \n"
                + "\n"
                + "  left outer join Facility on	[InRoomFacilityMatch].[facilityId]=[facility].[id]\n"
                + "  left outer join [Price] on[room].[priceId]=[price].[Id]\n"
                + "  where 1=1";
        if (hotelId != null) {
            sql += "	and [room].[hotelId]=" + hotelId;
        }

        if (priceFromString != null && priceFromString != null) {
            sql += "	and [price].[price]>=" + priceFromString + " " + "and [price].[price]<=" + priceToString + " --priceFrom  ,priceTo\n";
        }
        if (noOfAdultString != null) {

            sql += "\n"
                    + "	and [room].[maxNoOfAdult]>=" + noOfAdultString;
        }

        if (noOfChildrenString != null) {
            sql += " and [room].[maxNoOfChildren]>=" + noOfChildrenString + " --noOfAdultString ,noOfChildrenString\n"
                    + "\n";
        }
        if (startDateString != null && endDateString != null) {
            sql += "	and room.id not in(\n"
                    + "  select [roomId]\n"
                    + "  from [booking]\n"
                    + "  where ((startDate " + earlierThan + " '" + startDateString + "')and (endDate" + laterThan + "'" + endDateString + "')) or --start,end\n"
                    + "((startDate " + earlierThan + "'" + startDateString + "')and (endDate" + laterThan + "'" + startDateString + "')) or --start,start\n"
                    + "((startDate" + earlierThan + "'" + endDateString + "')and (endDate" + laterThan + "'" + endDateString + "')) or --end,end\n"
                    + "((startDate" + laterThan + "'" + startDateString + "')and (startDate" + earlierThan + "'" + endDateString + "') and (endDate" + earlierThan + "'" + endDateString + "') )  --start,end,end\n"
                    + "\n"
                    + "\n"
                    + ") ";
        }

        sql += ")";
        System.out.println(sql);
        return getRoomListHelper(sql);
    }

    private static ArrayList<Room> getRoomListHelper(String sql) {
        Connection con = DatabaseOperator.getInstance().getConnection();
        ArrayList<Room> resultList = new ArrayList();
        try {
            Statement statement = con.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);

            ResultSet roomListResults;
            // TODO hotelId sql injection?
            roomListResults = statement.executeQuery(sql);

            while (roomListResults.next()) {

                Room tempRoom = Room.parseRoomFromDBResultSet(roomListResults);
                resultList.add(tempRoom);
            }

        } catch (SQLException ex) {
            Logger.getLogger(DatabaseOperator.class.getName()).log(Level.SEVERE, null, ex);
        }
        return resultList;
    }

    public static Room getRoom(Integer roomId) {
        Connection con = DatabaseOperator.getInstance().getConnection();
        String selectSQL = "SELECT [id]"
                + "      ,[priceId]\n"
                + "      ,[maxNoOfAdult]\n"
                + "      ,[maxNoOfChildren]\n"
                + "      ,[purpose]\n"
                + "      ,[description]\n"
                + "      ,[roomNumber]\n"
                + "      ,[hotelId]\n"
                + "      ,[noOfBed]\n"
                + "      ,[icon]\n"
                + " FROM [dbo].[room] WHERE id = ?";
        PreparedStatement preparedStatement;
        Room room = null;
        try {
            preparedStatement = con.prepareStatement(selectSQL);
            preparedStatement.setInt(1, roomId);
            ResultSet rs = preparedStatement.executeQuery();
            if (rs.next()) {
                room = parseRoomFromDBResultSet(rs);
            }
        } catch (SQLException ex) {
            Logger.getLogger(Hotel.class.getName()).log(Level.SEVERE, null, ex);
        }
        return room;
    }

    public static int getTotalPageNoFromDB(int noOfRecordPerPage, String startDateString, String endDateString, String noOfAdultString, String noOfChildrenString, String priceFromString, String priceToString, String[] facilities) {
        int noOfRecord = Room.getTotalNumRecordFromDB(null, startDateString, endDateString, noOfAdultString, noOfChildrenString, priceFromString, priceToString, facilities);
        return (int) Math.ceil((double) ((double) noOfRecord / (double) noOfRecordPerPage));

    }

    public static int getTotalPageNoFromDB(String hotelId, int noOfRecordPerPage, String startDateString, String endDateString, String noOfAdultString, String noOfChildrenString, String priceFromString, String priceToString, String[] facilities) {
        int noOfRecord = Room.getTotalNumRecordFromDB(hotelId, startDateString, endDateString, noOfAdultString, noOfChildrenString, priceFromString, priceToString, facilities);
        return (int) Math.ceil((double) ((double) noOfRecord / (double) noOfRecordPerPage));

    }

//    public static int getTotalNumRecordFromDB() {
////        String extraSQL = "select [hotel].[id] from  [dbo].[hotel]";
//        return Room.getTotalNumRecordFromDB(null, startDateString, endDateString, noOfAdultString, noOfChildrenString, priceFromString, priceToString, facilities);
//
//    }
    public static float getRoomRatingFromDB(String roomId) throws SQLException {
        Connection con = DatabaseOperator.getInstance().getConnection();
        String sql = "SELECT avg([rating]) as [rating]\n"
                + "  FROM [dbo].[roomRating]\n"
                + "  where roomId=?";

        PreparedStatement preparedStatement;
        float rating = 0;
//        try {
        preparedStatement = con.prepareStatement(sql);

        preparedStatement.setString(1, roomId);

        ResultSet rs = preparedStatement.executeQuery();
        if (rs.next()) {
            rating = rs.getFloat("rating");
            if (rs.wasNull()) {
                rating = -1;
            }

        }
        return rating;

    }
//
//    public static float getRoomRatingFromDB(String roomId) throws SQLException {
//        Connection con = DatabaseOperator.getInstance().getConnection();
//        String sql = "SELECT avg([rating]) as [rating]\n"
//                + "  FROM [dbo].[roomRating]\n"
//                + "  where roomId=?";
//
//        PreparedStatement preparedStatement;
//        float rating = 0;
////        try {
//        preparedStatement = con.prepareStatement(sql);
//
//        preparedStatement.setString(1, roomId);
//
//        ResultSet rs = preparedStatement.executeQuery();
//        if (rs.next()) {
//            rating = rs.getFloat("rating");
//            if (rs.wasNull()) {
//                rating = -1;
//            }
//
//        }
//        return rating;
//
//    }

    public static boolean userHasRated(String userId, String roomId) throws SQLException {
        Connection con = DatabaseOperator.getInstance().getConnection();
        String sql = "SELECT count(*) as [rated]\n"
                + "  FROM [dbo].[roomRating]\n"
                + "  where creatorId=? and roomId=?";

        PreparedStatement preparedStatement;
        int rated = 0;
//        try {
        preparedStatement = con.prepareStatement(sql);
        preparedStatement.setString(1, userId);
        preparedStatement.setString(2, roomId);

        ResultSet rs = preparedStatement.executeQuery();
        if (rs.next()) {
            rated = rs.getInt("rated");
            if (rated > 0) {
                return true;
            } else {
                return false;
            }

        } else {
            return false;
        }

    }

    public static int getTotalNumRecordFromDB(String hotelId, String startDateString, String endDateString, String noOfAdultString, String noOfChildrenString, String priceFromString, String priceToString, String[] facilities) {
        String earlierThan = "<=";
        String laterThan = ">=";
        System.out.println("=============getTotalNumRecordFromDB");
        Connection con = DatabaseOperator.getInstance().getConnection();
//        String selectSQL = "SELECT count(*) as total\n"
//                + "  FROM [dbo].[room]\n"
//                + "  where hotelId in (" + hotelId + ")";

        String sql = "SELECT count(distinct [room].[id]) as [total]"
                + "  FROM [dbo].[room] left outer join [booking]on [room].[id]=[Booking].roomId\n"
                + "  left outer join  [InRoomFacilityMatch] on [room].[Id]=[InRoomFacilityMatch].[roomId] \n"
                + "\n"
                + "  left outer join Facility on	[InRoomFacilityMatch].[facilityId]=[facility].[id]\n"
                + "  left outer join [Price] on[room].[priceId]=[price].[Id]\n"
                + "  where 1=1";
        if (hotelId != null) {
            sql += "	and [room].[hotelId]=" + hotelId;
        }

        if (priceFromString != null && priceFromString != null) {
            sql += "	and [price].[price]>=" + priceFromString + " " + "and [price].[price]<=" + priceToString + " --priceFrom  ,priceTo\n";
        }
        if (noOfAdultString != null) {

            sql += "\n"
                    + "	and [room].[maxNoOfAdult]>=" + noOfAdultString;
        }

        if (noOfChildrenString != null) {
            sql += " and [room].[maxNoOfChildren]>=" + noOfChildrenString + " --noOfAdultString ,noOfChildrenString\n"
                    + "\n";
        }
        if (startDateString != null && endDateString != null) {
            sql += "	and room.id not in(\n"
                    + "  select [roomId]\n"
                    + "  from [booking]\n"
                    + "  where ((startDate " + earlierThan + " '" + startDateString + "')and (endDate" + laterThan + "'" + endDateString + "')) or --start,end\n"
                    + "((startDate " + earlierThan + "'" + startDateString + "')and (endDate" + laterThan + "'" + startDateString + "')) or --start,start\n"
                    + "((startDate" + earlierThan + "'" + endDateString + "')and (endDate" + laterThan + "'" + endDateString + "')) or --end,end\n"
                    + "((startDate" + laterThan + "'" + startDateString + "')and (startDate" + earlierThan + "'" + endDateString + "') and (endDate" + earlierThan + "'" + endDateString + "') )"
                    + "\n"
                    + "\n"
                    + ") ";
        }

        System.out.println(sql);
        PreparedStatement preparedStatement;

        int total = 0;
        try {
            preparedStatement = con.prepareStatement(sql);
//            preparedStatement.setString(1, hotelId);
            ResultSet rs = preparedStatement.executeQuery();
            if (rs.next()) {
                total = rs.getInt("total");
            }
        } catch (SQLException ex) {
            Logger.getLogger(Hotel.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println("total:" + total);
        return total;
    }

    //have rooms --> add bookmark to it
    public static void addBookMarksFromDBToModels(ArrayList<Room> rooms, String userId, String hotelId) {
        ArrayList<BookMark> bookmarks = BookMark.getBookMarksFromDB(userId);
        for (Room room : rooms) {
            for (BookMark bookmark : bookmarks) {
                if (bookmark.getRoom().getId() == room.getId()) {
                    room.setBookMarked(true);
                    System.out.println(room.getId() + " " + true);
                    break;
                }
            }
        }

    }

    public static Room addRoom(int priceId, String purpose, String roomNumber, String description, String noOfBed, String maxNoOfAdult, String maxNoOfChildren, int hotelId, String iconUrl) throws ValidatorException, SQLException {
        // validation
        if (Is.Blank(purpose) || Is.Blank(roomNumber) || Is.Blank(description) || Is.Blank(noOfBed) || Is.Blank(maxNoOfAdult) || Is.Blank(maxNoOfChildren)) {
            throw new ValidatorException("Please fill in all required fields");
        } else if (!(Is.PositiveInteger(noOfBed) && Is.PositiveInteger(maxNoOfAdult) && Is.PositiveInteger(maxNoOfChildren))) {
            throw new ValidatorException("Please type in positive integer in numeric fields");
        }
        return DatabaseOperator.getInstance().addRoom(priceId, purpose, roomNumber, description, noOfBed, maxNoOfAdult, maxNoOfChildren, hotelId, iconUrl);
    }

    public static void addRoomRatingToDB(String userId, String roomId, int roomRatings) throws SQLException {
        Connection con = DatabaseOperator.getInstance().getConnection();
        String sql = "INSERT INTO [dbo].[roomRating]\n"
                + "           ([creatorId]\n"
                + "           ,[roomId]\n"
                + "           ,[rating])\n"
                + "     VALUES\n"
                + "           (?\n"
                + "           ,?\n"
                + "           ,?)";

        PreparedStatement preparedStatement;
        int total = 0;
//        try {
        preparedStatement = con.prepareStatement(sql);
        preparedStatement.setString(1, userId);
        preparedStatement.setString(2, roomId);
        preparedStatement.setInt(3, roomRatings);

        preparedStatement.executeUpdate();

//        } catch (SQLException ex) {
//            Logger.getLogger(Hotel.class.getName()).log(Level.SEVERE, null, ex);
//        }
//        System.out.println("total:" + total);
//        return total;
    }

    public static ArrayList<Room> getSuggestedRoomList() {
        String sql = "SELECT top 3 [id]\n"
                + "      ,[priceId]\n"
                + "      ,[maxNoOfAdult]\n"
                + "      ,[maxNoOfChildren]\n"
                + "      ,[purpose]\n"
                + "      ,[description]\n"
                + "      ,[roomNumber]\n"
                + "      ,[hotelId]\n"
                + "      ,[noOfBed]\n"
                + "      ,[icon]\n"
                + "  FROM [dbo].[room]\n"
                + "  where [room].[id] in\n"
                + "  (SELECT top 3 roomId  as [rating]\n"
                + " FROM [dbo].[roomRating], [room]\n"
                + "\n"
                + "   group by roomId\n"
                + "   order by avg([rating]) desc)";
        System.out.println(sql);
        return getRoomListHelper(sql);
    }

    public static ArrayList<Room> getRelatedRoomList(String hotelId) {
        String sql = "SELECT top 3 [id]\n"
                + "      ,[priceId]\n"
                + "      ,[maxNoOfAdult]\n"
                + "      ,[maxNoOfChildren]\n"
                + "      ,[purpose]\n"
                + "      ,[description]\n"
                + "      ,[roomNumber]\n"
                + "      ,[hotelId]\n"
                + "      ,[noOfBed]\n"
                + "      ,[icon]\n"
                + "  FROM [dbo].[room]\n"
                + "  where [room].[id] in\n"
                + "  (SELECT top 3 id  \n"
                + " FROM [room]\n"
                + "\n"
                + "   where hotelId=" + hotelId + "\n"
                + "   order by NEWID()\n"
                + "   )";
        System.out.println(sql);
        return getRoomListHelper(sql);
    }
}
