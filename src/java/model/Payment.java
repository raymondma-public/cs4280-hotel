/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import exception.ParseErrorException;
import database.DatabaseOperator;
import exception.CannotPayException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import page.editUserPage.EditUserPageServlet;
import utils.DatetimeUtil;

/**
 *
 * @author RaymondMa
 */
public class Payment {

    private int noOfPpl;

    public int getNoOfPpl() {
        return noOfPpl;
    }

    public void setNoOfPpl(int noOfPpl) {
        this.noOfPpl = noOfPpl;
    }

    public Payment(int id, Calendar paymentDate, Price price, int noOfPpl) {
        this.noOfPpl = noOfPpl;
        this.id = id;
        this.paymentDate = paymentDate;
        this.price = price;
    }

    static Payment getPayment(String paymentId) throws ParseErrorException {
        try {
            String sql = "SELECT [id]\n"
                    + "      ,[paymentDate]\n"
                    + "      ,[priceId],[noOfPpl]\n"
                    + "  FROM [dbo].[payment]"
                    + "where [id]=?";
            Connection con = DatabaseOperator.getInstance().getConnection();

            PreparedStatement ps
                    = con.prepareStatement(sql);
            ps.setString(1, paymentId);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                int id = rs.getInt("id");
                String paymentDateString = rs.getString("paymentDate");
                int priceId = rs.getInt("priceId");
                int noOfPpl = rs.getInt("noOfPpl");
                Calendar paymentDate = DatetimeUtil.getCalendarFromDatetimeString(paymentDateString);
                Price price = Price.getPrice(priceId);

                Payment tempPayment = new Payment(id, paymentDate, price, noOfPpl);
                return tempPayment;
            } else {
                return null;
            }

        } catch (ParseException ex) {

            Logger.getLogger(Payment.class.getName()).log(Level.SEVERE, null, ex);
            throw new ParseErrorException();
        } catch (SQLException ex) {

            Logger.getLogger(Payment.class.getName()).log(Level.SEVERE, null, ex);
            throw new ParseErrorException();
        }

    }

    private int id;
    private Calendar paymentDate;

//    public Payment(int id, Calendar paymentDate, Price price) {
//        this.id = id;
//        this.paymentDate = paymentDate;
//        this.price = price;
//    }
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPaymentDate() {
        return DatetimeUtil.calendarToString(paymentDate);
    }

    public void setPaymentDate(Calendar paymentDate) {
        this.paymentDate = paymentDate;
    }

    public Price getPrice() {
        return price;
    }

    public void setPrice(Price price) {
        this.price = price;
    }
    private Price price;

    public static void pay(String bookingId, String priceId, int noOfPpl) throws CannotPayException {
        Connection con = DatabaseOperator.getInstance().getConnection();
        try {
            con.setAutoCommit(false);
            String paySql = "INSERT INTO [dbo].[payment]\n"
                    + "           ([paymentDate]\n"
                    + "           ,[priceId]\n"
                    + "           ,[noOfPpl])\n"
                    + "     VALUES\n"
                    + "           (GETDATE()\n"
                    + "           ,?\n"
                    + "           ,?)";

            PreparedStatement ps
                    = con.prepareStatement(paySql);
            ps.setString(1, priceId);
            ps.setInt(2, noOfPpl);

            ps.executeUpdate();

            String getPaymentIdSQL = "SELECT @@IDENTITY AS [id]";
            PreparedStatement ps2 = con.prepareStatement(getPaymentIdSQL);
            ResultSet rs = ps2.executeQuery();
            int id = 0;
            if (rs.next()) {
                id = rs.getInt("id");
            } else {
                throw new SQLException();
            }

            String addPayToBookingSQL = ""
                    + "UPDATE [dbo].[Booking]\n"
                    + "   SET \n"
                    + "      [paymentId] = ?   \n"
                    + "     \n"
                    + " WHERE [Booking].[id]=?  ";
            PreparedStatement ps3 = con.prepareStatement(addPayToBookingSQL);
            ps3.setInt(1, id);
            ps3.setString(2, bookingId);
            ps3.executeUpdate();

            con.commit();
        } catch (SQLException ex) {
            Logger.getLogger(Payment.class.getName()).log(Level.SEVERE, null, ex);
            try {
                con.rollback();

                con.setAutoCommit(true);
            } catch (SQLException ex1) {
                Logger.getLogger(EditUserPageServlet.class.getName()).log(Level.SEVERE, null, ex1);
            }
            throw new CannotPayException();
        }

    }

}
