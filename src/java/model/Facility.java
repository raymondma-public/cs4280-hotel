/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import database.DatabaseOperator;

import java.sql.*;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author RaymondMa
 */
public class Facility {

    public static ArrayList<Facility> getFacilities() throws SQLException {
        ArrayList<Facility> result = new ArrayList();
        Connection con = DatabaseOperator.getInstance().getConnection();
        Statement stmt = con.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
        String selectSQL = "SELECT * FROM [dbo].[facility]";
        ResultSet rs = stmt.executeQuery(selectSQL);
        while (rs.next()) {
            result.add(new Facility(rs.getInt("id"), rs.getString("name"), rs.getString("description"), rs.getString("iconURL")));
        }
        return result;
    }

    static void addFacilityToRoom(Room resultRoom) {

        System.out.println("============addFacilityToRoom");
        Connection con = DatabaseOperator.getInstance().getConnection();
        String selectSQL = " SELECT distinct [inRoomFacilityMatch].[id]\n"
                + "      ,[roomId]\n"
                + "      ,[facilityId]\n"
                + "      ,[hotelId],[Facility].*\n"
                + "  FROM [dbo].[inRoomFacilityMatch],[Facility]\n"
                + "    where roomId=?  and\n"
                + "	[inRoomFacilityMatch].facilityId =facility.id";

        PreparedStatement preparedStatement;
        try {
            //        try {
            preparedStatement = con.prepareStatement(selectSQL);

            preparedStatement.setInt(1, resultRoom.getId());
            System.out.println(resultRoom.getId() + " " + resultRoom.getHotel().getId());
//            preparedStatement.setInt(2, resultRoom.getHotel().getId());
//            preparedStatement.execute();
            ResultSet rs = preparedStatement.executeQuery();
            int facilityCount = 0;
            while (rs.next()) {

                Facility tempFacility = Facility.parseFacilityFromDBResultSet(rs);
                resultRoom.addFacilities(tempFacility);
                System.out.println("facilityCount: " + facilityCount);
                facilityCount++;
            }

        } catch (SQLException ex) {
            Logger.getLogger(Hotel.class.getName()).log(Level.SEVERE, null, ex);
        }
//        return resultPrice;

    }

    private static Facility parseFacilityFromDBResultSet(ResultSet rs) {
        Facility facility = null;
        try {

            int id = rs.getInt("facilityId");
            String name = rs.getString("name");
            String description = rs.getString("description");
            String iconURL = rs.getString("iconURL");
            facility = new Facility(id, name, description, iconURL);
        } catch (SQLException ex) {
            Logger.getLogger(Facility.class.getName()).log(Level.SEVERE, null, ex);
        }
        return facility;

    }

    public static void addFacility(String roomId, String facilityName) throws SQLException {
        String sql = "INSERT INTO [dbo].[inRoomFacilityMatch]\n"
                + "           ([roomId]\n"
                + "           ,[facilityId])\n"
                //                + "           ,[hotelId])\n"
                + "     select\n"
                + "           " + roomId + "\n"
                + "           ,(SELECT [id]\n"
                + "   FROM [dbo].[facility]\n"
                + "   where name like  '" + facilityName + "')\n"
                //                + "           ,?"
                + "";
        System.out.println(sql);
        Connection con = DatabaseOperator.getInstance().getConnection();
        PreparedStatement ps = con.prepareStatement(sql);
//        ps.setString(1, roomId);
//        ps.setString(2, );

        ps.executeUpdate();

    }

    public static void removeFacilities(String roomId) throws SQLException {
        String sql = "DELETE FROM [dbo].[inRoomFacilityMatch]\n"
                + "      WHERE roomId=?";

        Connection con = DatabaseOperator.getInstance().getConnection();
        PreparedStatement ps = con.prepareStatement(sql);
        ps.setString(1, roomId);

        ps.executeUpdate();

    }

    public Facility(int id, String name, String description, String iconURL) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.iconURL = iconURL;
    }
    private int id;
    private String name;
    private String description;
    private String iconURL;

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setIconURL(String iconURL) {
        this.iconURL = iconURL;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String getIconURL() {
        return iconURL;
    }
}
