package model;

import database.DatabaseOperator;
import exception.UserLoginNotOKException;
import exception.UserNotFoundException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import org.apache.commons.validator.ValidatorException;
import utils.Is;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import utils.Hash;

/**
 *
 * @author RaymondMa
 */
public class User {

    public static boolean passwordCorrect(String userId, String hash) throws SQLException {
        String sql = "select [passwordHash] from [user] where [id]=?";
        Connection con = DatabaseOperator.getInstance().getConnection();
        PreparedStatement ps = con.prepareStatement(sql);
        ps.setString(1, userId);
        ResultSet rs = ps.executeQuery();
        if (rs.next()) {
            String realPwHash = rs.getString("passwordHash");
            if (realPwHash.equals(hash)) {
                return true;
            }
        }
        return false;
    }

    private String id;
    private String userName;
    private String iconImageURL;
    private String email;
    private String phone;
    private String userType;
    private Hotel managingHotel;

    public String getDefaultIconImageURL() {
        return "img/default_user_pic.jpg";
    }

    public String getIconImageURL() {
        return iconImageURL;
    }

    public void setIconImageURL(String iconImageURL) {
        this.iconImageURL = iconImageURL;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public User(String id, String userName, String iconImageURL, String email, String phone, String userType) {
        this.id = id;
        this.userName = userName;
        this.iconImageURL = iconImageURL;
        this.email = email;
        this.phone = phone;
        this.userType = userType;
    }

    public void setManagingHotel(Hotel hotel) {
        managingHotel = hotel;
    }

    public Hotel getManagingHotel() {
        return managingHotel;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPhone() {
        return phone;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserName() {
        return userName;
    }

    public static User login(String email, String password) throws SQLException, UserLoginNotOKException, UserNotFoundException, ValidatorException {
        // validation
        if (!Is.Email(email)) {
            throw new ValidatorException("Invalid Email: " + email);
        } else if (Is.Blank(password)) {
            throw new ValidatorException("Blank Password");
        }
        // db
        User user = DatabaseOperator.getInstance().userLogin(email, password);
        return user;
    }

    public static void register(String role, String email, String password, Hotel hotel) throws ValidatorException, SQLException {
        // validation
        if (Is.NotInList(role, getRoleList())) {
            throw new ValidatorException("Invalid role: " + role);
        } else if (!Is.Email(email)) {
            throw new ValidatorException("Invalid Email: " + email);
        } else if (Is.Blank(password)) {
            throw new ValidatorException("Blank Password");
        } else if (role.equals("manager") && hotel == null) {
            throw new ValidatorException("Cannot associate User (manager) to manage a hotel");
        }
        // db
        DatabaseOperator.getInstance().register(role, email, password, hotel);
    }

    public static User getUserById(String userId) throws SQLException, UserNotFoundException {
        return DatabaseOperator.getInstance().getUserById(userId);
    }

    public static ArrayList<User> getUserList() throws SQLException {
        return DatabaseOperator.getInstance().getUserList();
    }

    public static void updateNameToDB(String userId, String name) throws SQLException {
        try {
            Connection con = DatabaseOperator.getInstance().getConnection();
            String sql = "UPDATE [dbo].[user]\n"
                    + "   SET [userName] = ?\n"
                    + "      \n"
                    + " WHERE [user].[id]=?";
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setString(1, name.trim());
            ps.setString(2, userId);

            int affectedRows = ps.executeUpdate();
            if (affectedRows == 0) {
                throw new SQLException("User name update fail");
            }
        } catch (SQLException ex) {
            Logger.getLogger(User.class.getName()).log(Level.SEVERE, null, ex);
            throw ex;
        }

    }

    public static void updatePhoneToDB(String userId, String phone) throws SQLException {
        try {
            Connection con = DatabaseOperator.getInstance().getConnection();
            String sql = " UPDATE [dbo].[user]\n"
                    + "   SET \n"
                    + "      [phone] = ? WHERE [user].[id]=?";
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setString(1, phone.trim());
            ps.setString(2, userId);

            int affectedRows = ps.executeUpdate();
            if (affectedRows == 0) {
                throw new SQLException("User phone update fail");
            }
        } catch (SQLException ex) {
            Logger.getLogger(User.class.getName()).log(Level.SEVERE, null, ex);
            throw ex;
        }
    }

    public static void updatePasswordToDB(String userId, String password) throws SQLException {
        try {
            Connection con = DatabaseOperator.getInstance().getConnection();
            String sql = "UPDATE [dbo].[user]\n"
                    + "   SET \n"
                    + "      [passwordHash] = ? WHERE [user].[id]=?\n"
                    + "    ";
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setString(1, Hash.hash(password));
            ps.setString(2, userId);

            int affectedRows = ps.executeUpdate();
            if (affectedRows == 0) {
                throw new SQLException("User password update fail");
            }
        } catch (SQLException ex) {
            Logger.getLogger(User.class.getName()).log(Level.SEVERE, null, ex);
            throw ex;
        }
    }

    public static void updateUserRoleToDB(String userId, String userType) throws SQLException {
        try {
            Connection con = DatabaseOperator.getInstance().getConnection();
            String sql = " UPDATE [dbo].[user]\n"
                    + "   SET \n"
                    + "      [userType] = ? WHERE [user].[id]=?";
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setString(1, userType.trim());
            ps.setString(2, userId);

            int affectedRows = ps.executeUpdate();
            if (affectedRows == 0) {
                throw new SQLException("User role update fail");
            }
        } catch (SQLException ex) {
            Logger.getLogger(User.class.getName()).log(Level.SEVERE, null, ex);
            throw ex;
        }
    }

    public boolean isChiefManager() {
        return userType.equals("chiefManager");
    }

    public boolean isManager() {
        return userType.equals("manager");
    }

    public boolean isCustomer() {
        return userType.equals("customer");
    }

    public static String[] getRoleList() {
        return new String[]{"customer", "manager", "chiefManager"};
    }

    public static User deleteUserById(String userId) throws UserNotFoundException, SQLException {
        try {
            User user = getUserById(userId);
            Connection con = DatabaseOperator.getInstance().getConnection();
            String sql = "DELETE FROM [dbo].[user]"
                    + "WHERE [user].[id] = ?";
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setString(1, userId);

            ps.executeUpdate();
            if (ps.getUpdateCount() == 0) {
                throw new SQLException();
            } else {
                return user;
            }
        } catch (SQLException ex) {
            Logger.getLogger(User.class.getName()).log(Level.SEVERE, null, ex);
            throw ex;
        }
    }

    public static void updateUserManageHotelToDB(String userId, String managingHotelId) throws SQLException {
        try {
            Connection con = DatabaseOperator.getInstance().getConnection();
            String sql = " UPDATE [dbo].[user]\n"
                    + "   SET \n"
                    + "      [managingHotelId] = ? WHERE [user].[id]=?";
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setString(1, managingHotelId);
            ps.setString(2, userId);

            int affectedRows = ps.executeUpdate();
            if (affectedRows == 0) {
                throw new SQLException("User manage's hotel update fail");
            }
        } catch (SQLException ex) {
            Logger.getLogger(User.class.getName()).log(Level.SEVERE, null, ex);
            throw ex;
        }
    }
}
