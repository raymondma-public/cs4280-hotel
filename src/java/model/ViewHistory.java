/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import database.DatabaseOperator;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import utils.DatetimeUtil;

/**
 *
 * @author RaymondMa
 */
public class ViewHistory {

    private User customer;

    private Room room;
    private Calendar date;

    public ViewHistory(User customer, Room room, Calendar date) {
        this.customer = customer;
        this.room = room;
        this.date = date;
    }

    public User getCustomer() {
        return customer;
    }

    public void setCustomer(User customer) {
        this.customer = customer;
    }

    public Room getRoom() {
        return room;
    }

    public void setRoom(Room room) {
        this.room = room;
    }

    public String getDate() {
        Calendar cal = date;
//        cal.add(Calendar.DATE, 1);
        SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
        System.out.println(cal.getTime());
// Output "Wed Sep 26 14:23:28 EST 2012"

        String formatted = format1.format(cal.getTime());
        return formatted;
    }

    public void setDate(Calendar date) {
        this.date = date;
    }

    public static void addViewedHistoryToDB(String customerId, String roomId) {
        Connection con = DatabaseOperator.getInstance().getConnection();
        String sql = "INSERT INTO [dbo].[Viewed]\n"
                + "           ([customerId]\n"
                //                + "           ,[hotelId]\n"
                + "           ,[roomId]\n"
                + "           ,[date])\n"
                + "     VALUES\n"
                + "           (?"
                //                + "           ,?"
                + "           ,?"
                + "           ,GETDATE())";

        PreparedStatement preparedStatement;
        try {
            //        try {
            preparedStatement = con.prepareStatement(sql);

//            preparedStatement.execute();
            preparedStatement.setString(1, customerId);
            preparedStatement.setString(2, roomId);
//            preparedStatement.setString(3, roomId);
//            preparedStatement.setString(4, bookmark.getRoom().getHotel().getId());
//            throw new Exception();

            preparedStatement.execute();

        } catch (SQLException ex) {
            Logger.getLogger(BookMark.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static ArrayList<ViewHistory> getBookMarksFromDB(String customerId) {
        Connection con = DatabaseOperator.getInstance().getConnection();
        String sql = "SELECT [id]\n"
                + "      ,[customerId]\n"
                + "      ,[roomId]\n"
                + "      ,[date]\n"
                + "  FROM [dbo].[Viewed]\n"
                + "  where customerId=?";

        ArrayList<ViewHistory> resultList = new ArrayList();
        PreparedStatement preparedStatement;
        try {
            //        try {
            preparedStatement = con.prepareStatement(sql);

//            preparedStatement.execute();
            preparedStatement.setString(1, customerId);

            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {

                int id = rs.getInt("id");
                int roomId = rs.getInt("roomId");
                Room tempRoom = Room.getRoom(roomId);
                String dateString = rs.getString("date");
                Calendar tempCal = DatetimeUtil.getCalendarFromDatetimeString(dateString);
                ViewHistory viewHistory = new ViewHistory(null, tempRoom, tempCal);

                resultList.add(viewHistory);
            }

        } catch (SQLException ex) {
            Logger.getLogger(BookMark.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParseException ex) {
            Logger.getLogger(BookMark.class.getName()).log(Level.SEVERE, null, ex);
        }
        return resultList;
    }

    public static void removeViewHistoryFromDB(String customerId, String roomId) {
        Connection con = DatabaseOperator.getInstance().getConnection();
        String sql = "DELETE FROM [dbo].[Viewed]\n"
                + "      WHERE customerId=? and \n"
                + "	  roomId=?";

        PreparedStatement preparedStatement;
        try {
            //        try {
            preparedStatement = con.prepareStatement(sql);

//            preparedStatement.execute();
            preparedStatement.setString(1, customerId);
            preparedStatement.setString(2, roomId);
//            preparedStatement.setString(3, roomId);
//            preparedStatement.setString(4, bookmark.getRoom().getHotel().getId());
//            throw new Exception();

            preparedStatement.executeUpdate();

        } catch (SQLException ex) {
            Logger.getLogger(BookMark.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
