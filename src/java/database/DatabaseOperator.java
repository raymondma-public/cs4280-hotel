/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package database;

import exception.UserLoginNotOKException;

import java.sql.*;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;

import exception.UserNotFoundException;
import model.*;
import utils.Hash;

public class DatabaseOperator {

    private static DatabaseOperator instance = new DatabaseOperator();
    private Connection con;
    private ArrayList<User> userList;

    public Connection getConnection() {
        return con;
    }

    private DatabaseOperator() {
        try {
            connect();
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(DatabaseOperator.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(DatabaseOperator.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void connect() throws ClassNotFoundException, SQLException {
        String url = "jdbc:sqlserver://w2ksa.cs.cityu.edu.hk:1433;databaseName=aiad051_db;";
        String username = "aiad051";
        String password = "aiad051";
        java.lang.Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
        con = DriverManager.getConnection(url, username, password);
    }

    public static DatabaseOperator getInstance() {
        return instance;
    }

//    public ArrayList<User> getSampleFromServer() {
//        try {
//            connect();
//        } catch (ClassNotFoundException ex) {
//            Logger.getLogger(DatabaseOperator.class.getName()).log(Level.SEVERE, null, ex);
//        } catch (SQLException ex) {
//            Logger.getLogger(DatabaseOperator.class.getName()).log(Level.SEVERE, null, ex);
//        }
//        
//        ArrayList<User> resultList = new ArrayList();
//        try {
//            Statement statement = con.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
//
//            ResultSet roomListResults;
//
//            roomListResults = statement.executeQuery("SELECT * FROM [phonebook] ORDER BY [Name] ASC");
//
////            ResultSetMetaData phonebookResultsMetaData = roomListResults.getMetaData();
////            // list of data 
////            for (int i = 0; i < phonebookResultsMetaData.getColumnCount() + 1; i++) {
////                out.println(phonebookResultsMetaData.getColumnClassName(i));
////            }
//            while (roomListResults.next()) {
//                String uid = roomListResults.getString("UID");
//                String name = roomListResults.getString("Name");
//                String phone = roomListResults.getString("Phone");
//                User user = new User();
//                user.setId(uid);
//                user.setPhone(phone);
//                user.setUserName(name);
//                resultList.add(user);
//
//            }
//        } catch (SQLException ex) {
//            Logger.getLogger(DatabaseOperator.class.getName()).log(Level.SEVERE, null, ex);
//        }
//        return resultList;
//    }
    public ArrayList<Room> getRoomList() {
        try {
            connect();
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(DatabaseOperator.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(DatabaseOperator.class.getName()).log(Level.SEVERE, null, ex);
        }

        ArrayList<Room> resultList = new ArrayList();
        try {
            Statement statement = con.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);

            ResultSet roomListResults;

//            roomListResults = statement.executeQuery("SELECT *  "
//                    + "FROM [dbo].[room]left outer join [price] on [room].priceId=[price].id ");
            roomListResults = statement.executeQuery("SELECT *  \n"
                    + "FROM (([dbo].[room]left outer join [price] on [room].priceId=[price].id)\n"
                    + "left outer join inRoomFacilityMatch on [inRoomFacilityMatch].roomId=room.id )\n"
                    + "left outer join facility on [inRoomFacilityMatch].facilityId=facility.id");

//            ResultSetMetaData phonebookResultsMetaData = roomListResults.getMetaData();
//            // list of data 
//            for (int i = 0; i < phonebookResultsMetaData.getColumnCount() + 1; i++) {
//                out.println(phonebookResultsMetaData.getColumnClassName(i));
//            }
            ArrayList<Integer> containRoomIds = new ArrayList();
            while (roomListResults.next()) {
                int uid = roomListResults.getInt("id");
                if (!containRoomIds.contains(uid)) {//not contain
                    containRoomIds.add(uid);
                    int maxNoOfAdult = roomListResults.getInt("maxNoOfAdult");
                    int maxNoOfChildren = roomListResults.getInt("maxNoOfChildren");
                    String purpose = roomListResults.getString("purpose");
                    String description = roomListResults.getString("description");
                    String roomNumber = roomListResults.getString("roomNumber");
                    String date = roomListResults.getString("date");
                    boolean isAdult = roomListResults.getBoolean("isAdult");
                    float price = roomListResults.getFloat("price");
                    int noOfBed = roomListResults.getInt("noOfBed");
                    String iconURL = roomListResults.getString("icon");
                    int priceId = roomListResults.getInt("priceId");
                    Price tempPrice = null;
                    if (priceId >= 0) {
                        tempPrice = new Price(priceId, null, isAdult, price);
                    }

                    float roomRating = Room.getRoomRatingFromDB("" + uid);

                    Room tempRoom = new Room(uid, tempPrice, maxNoOfAdult, maxNoOfChildren, purpose, description, roomNumber, null, noOfBed, iconURL, roomRating);
                    resultList.add(tempRoom);
                }
//                else {//contain room already
                int facilityId = roomListResults.getInt("id");
                String facilityName = roomListResults.getString("name");
                String facilityDescription = roomListResults.getString("description");
                String iconURL = roomListResults.getString("iconURL");
                Facility facility = new Facility(facilityId, facilityName, facilityDescription, iconURL);
                Room theRoom = findRoom(resultList, uid);
                theRoom.addFacilities(facility);
//                }

//                User user = new User();
//                user.setId(uid);
//                user.setPhone(phone);
//                user.setUserName(name);
            }
        } catch (SQLException ex) {
            Logger.getLogger(DatabaseOperator.class.getName()).log(Level.SEVERE, null, ex);
        }
        return resultList;

    }

    public static void main(String[] args) {
        DatabaseOperator dbOperator = DatabaseOperator.getInstance();
//        ArrayList<User> users=dbOperator.getSampleFromServer();
    }

    private Room findRoom(ArrayList<Room> resultList, int uid) {
        for (Room r : resultList) {
            if (r.getId() == uid) {
                return r;
            }
        }
        return null;
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void register(String role, String email, String password, Hotel hotel) throws SQLException {
        String insertTableSQL = "INSERT INTO [dbo].[user]\n"
                + "([passwordHash] ,[email],[userType], [managingHotelId])\n"
                + " VALUES\n"
                + "(?    ,?   ,?, ?)";

        PreparedStatement ps = con.prepareStatement(insertTableSQL);

        ps.setString(1, Hash.hash(password));
        ps.setString(2, email);
        ps.setString(3, role);
        if (hotel == null) {
            ps.setString(4, null);
        } else {
            ps.setString(4, String.valueOf(hotel.getId()));
        }

        int affectedRows = ps.executeUpdate();

        if (affectedRows == 0) {
            throw new SQLException("Creating user failed, no rows affected.");
        }

    }

    public String verifyUserAndReturnUserId(String email, String password) throws SQLException, UserLoginNotOKException {
        String selectSQL = "SELECT *\n"
                + "  FROM[dbo].[user]\n"
                + "        where email =?";
        PreparedStatement preparedStatement = con.prepareStatement(selectSQL);
        preparedStatement.setString(1, email);
        ResultSet rs = preparedStatement.executeQuery();
        rs.next();
        String passwordHash = rs.getString("passwordHash");
        String userId = rs.getString("id");
//            String username = rs.getString("USERNAME");
//        }
        if (passwordHash.equals(Hash.hash(password))) {
            return userId;
        } else {
            throw new UserLoginNotOKException();
        }

    }

    public User userLogin(String email, String password) throws SQLException, UserLoginNotOKException, UserNotFoundException {
        String selectSQL = "SELECT *\n"
                + "  FROM[dbo].[user]\n"
                + "        where email =?";
        PreparedStatement preparedStatement = con.prepareStatement(selectSQL);
        preparedStatement.setString(1, email);
        ResultSet rs = preparedStatement.executeQuery();
        if (rs.next()) {
            String passwordHash = rs.getString("passwordHash");
            String userId = rs.getString("id");
            String userName = rs.getString("userName");
            String imageURL = rs.getString("iconImageURL");
            String phone = rs.getString("phone");
            String userType = rs.getString("userType");
            if (passwordHash.equals(Hash.hash(password))) {
                User user = new User(userId, userName, imageURL, email, phone, userType);
                if (user.isManager()) {
                    int managingHotelId = rs.getInt("managingHotelId");
                    Hotel managingHotel = Hotel.getHotelFromDB(managingHotelId);
                    user.setManagingHotel(managingHotel);
                }
                return user;
            } else {
                throw new UserLoginNotOKException();
            }
        } else {
            throw new UserNotFoundException(email);
        }
    }

//    public User getUserInfo() {
//
//    }
    public User getUserById(String userId) throws SQLException, UserNotFoundException {
        String selectSQL = "SELECT *\n"
                + "  FROM [dbo].[user]\n"
                + "  where id=?";
        PreparedStatement preparedStatement = con.prepareStatement(selectSQL);
        preparedStatement.setString(1, userId);
        ResultSet rs = preparedStatement.executeQuery();
        if (rs.next()) {
            String userName = rs.getString("userName");
            String imageURL = rs.getString("iconImageURL");
            String email = rs.getString("email");
            String phone = rs.getString("phone");
            String userType = rs.getString("userType");

            User user = new User(userId, userName, imageURL, email, phone, userType);
            return user;
        } else {
            throw new UserNotFoundException(userId);
        }
    }

    public ArrayList<User> getUserList() throws SQLException {
        ArrayList<User> userList = new ArrayList();
        String sql = "SELECT *"
                + "FROM [dbo].[user]";
        PreparedStatement preparedStatement = con.prepareStatement(sql);
        ResultSet rs = preparedStatement.executeQuery();
        while (rs.next()) {
            String userId = rs.getString("id");
            String userName = rs.getString("userName");
            String imageURL = rs.getString("iconImageURL");
            String email = rs.getString("email");
            String phone = rs.getString("phone");
            String userType = rs.getString("userType");

            User user = new User(userId, userName, imageURL, email, phone, userType);
            if (user.isManager()) {
                int managingHotelId = rs.getInt("managingHotelId");
                Hotel managingHotel = Hotel.getHotelFromDB(managingHotelId);
                user.setManagingHotel(managingHotel);
            }
            userList.add(user);
        }
        return userList;
    }

    public Room addRoom(int priceId, String purpose, String roomNumber, String description, String noOfBed, String maxNoOfAdult, String maxNoOfChildren, int hotelId, String iconUrl) throws SQLException {
        String insertTableSQL = "INSERT INTO [dbo].[room]"
                + "([priceId], [purpose], [roomNumber], [description], [noOfBed], [maxNoOfAdult], [maxNoOfChildren], [hotelId], [icon])"
                + " VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";

        PreparedStatement ps;
        ps = con.prepareStatement(insertTableSQL);

        ps.setString(1, String.valueOf(priceId));
        ps.setString(2, purpose);
        ps.setString(3, roomNumber);
        ps.setString(4, description);
        ps.setString(5, noOfBed);
        ps.setString(6, maxNoOfAdult);
        ps.setString(7, maxNoOfChildren);
        ps.setString(8, String.valueOf(hotelId));
        ps.setString(9, iconUrl);
        ps.executeUpdate();

        Statement stmt = con.createStatement();
        ResultSet rs = stmt.executeQuery("SELECT @@IDENTITY AS [@@IDENTITY]");
        if (rs != null && rs.next() != false) {
            return Room.getRoom(rs.getInt(1));
        } else {
            throw new SQLException();
        }
    }

    public int addPrice(float price) throws SQLException {
        Calendar cal = Calendar.getInstance();
        String sql = "INSERT INTO [dbo].[price]( [price])VALUES ( ?)";
        System.out.println(sql);
        PreparedStatement ps = con.prepareStatement(sql);
//        ps.setString(1, "''");
//        ps.setString(2, String.valueOf(isAdult));
        ps.setFloat(1, price);

        int affectedRows = ps.executeUpdate();

        if (affectedRows == 0) {
            throw new SQLException("Creating price failed, no rows affected.");
        }

        return getLastModifiedId();
//        try  {
//        ResultSet generatedKeys = ps.getGeneratedKeys();
//        if (generatedKeys.next()) {
//            int id = generatedKeys.getInt(1);
//            return id;
////            return new Price(id, cal, isAdult, price);
//        } else {
//            throw new SQLException("Creating user failed, no ID obtained.");
//        }

//        }
    }

    public static int getLastModifiedId() throws SQLException {
        Connection con = DatabaseOperator.getInstance().getConnection();
        String getPaymentIdSQL = "SELECT @@IDENTITY AS [id]";
        PreparedStatement ps2 = con.prepareStatement(getPaymentIdSQL);
        ResultSet rs = ps2.executeQuery();
        int id = 0;
        if (rs.next()) {
            id = rs.getInt("id");
        } else {
            throw new SQLException();
        }
        return id;
    }
}
