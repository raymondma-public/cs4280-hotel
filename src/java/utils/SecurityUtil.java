package utils;

import exception.CSRFException;

import javax.servlet.http.HttpServletRequest;
import java.net.URI;
import java.net.URISyntaxException;

public class SecurityUtil {

    public static void checkReferer(HttpServletRequest request) throws CSRFException, URISyntaxException {
        String referer = request.getHeader("referer");
        if (referer != null) {
            String refererDomain = getDomainName(referer);
            String requestUrlDomain = getDomainName(request.getRequestURL().toString());
            if (!refererDomain.equals(requestUrlDomain))
                throw new CSRFException();
        }
    }

    private static String getDomainName(String url) throws URISyntaxException {
        URI uri = new URI(url);
        String domain = uri.getHost();
        return domain.startsWith("www.") ? domain.substring(4) : domain;
    }

}
