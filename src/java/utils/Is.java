package utils;

// https://commons.apache.org/proper/commons-validator/javadocs/api-1.5.0/org/apache/commons/validator/routines/package-summary.html
import org.apache.commons.validator.routines.EmailValidator;
import org.apache.commons.validator.routines.FloatValidator;
import org.apache.commons.validator.routines.IntegerValidator;

public class Is {

    private Is() {
    }

    public static Boolean Integer(String s) {
        return s != null && IntegerValidator.getInstance().isValid(s);
    }

    public static Boolean PositiveInteger(String s) {
        try {
            return Integer(s) && Integer.parseInt(s) > 0;
        } catch (Exception ex) {
            return false;
        }
    }

    public static Boolean Email(String s) {
        return s != null && EmailValidator.getInstance().isValid(s);
    }

    public static Boolean Blank(String s) {
        return s != null && s.trim().length() == 0;
    }

    public static Boolean NotBlank(String s) {
        return !Blank(s);
    }

    public static Boolean InList(String s, String[] strings) {
        for (String string : strings) {
            if (s.contains(string)) {
                return true;
            }
        }
        return false;
    }

    public static Boolean NotInList(String s, String[] strings) {
        return !InList(s, strings);
    }

    public static Boolean Float(String s) {
        return s != null && FloatValidator.getInstance().isValid(s);
    }

    public static boolean PositiveFloat(String s) {
        try {
            return Float(s) && Float.parseFloat(s) > 0;
        } catch (Exception ex) {
            return false;
        }
    }
}
