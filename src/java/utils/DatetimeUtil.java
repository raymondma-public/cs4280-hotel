/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

/**
 *
 * @author RaymondMa
 */
public class DatetimeUtil {

    public static Calendar getCalendarFromDatetimeString(String datetimeString) throws ParseException {
        Calendar cal = Calendar.getInstance();
        System.out.println("============getCalendarFromDatetimeString");
        System.out.println(datetimeString);

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS", Locale.US);
        cal.setTime(sdf.parse(datetimeString));// all done
        
        return cal;
    }

    public static String calendarToString(Calendar cal) {
        SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
        System.out.println(cal.getTime());
// Output "Wed Sep 26 14:23:28 EST 2012"

        String formatted = format1.format(cal.getTime());
        return formatted;
    }
}
