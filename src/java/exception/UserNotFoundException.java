package exception;

public class UserNotFoundException extends Exception {

    public UserNotFoundException(String s) {
        super("User not found: " + s);
    }

    public UserNotFoundException() {

    }
}
