package exception;

/**
 * Created by Donald on 25-Apr-16.
 */
public class RateLimitException extends Throwable {
    public RateLimitException(String event) {
        super(event + " rate limited, please stop " + event + " so frequent");
    }
}
