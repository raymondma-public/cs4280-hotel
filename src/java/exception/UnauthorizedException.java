package exception;

public class UnauthorizedException extends Exception {
    public UnauthorizedException() {
        super("Login is required");
    }
}
