package exception;

public class ForbiddenException extends Throwable {
    public ForbiddenException(String userType, String[] roles) {
        super("User role " + userType + " not in " + roles);
    }

    public ForbiddenException(String userType, String role) {
        super("User role " + userType + " not equal to " + role);
    }
}
