/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exception;

public class UserLoginNotOKException extends Exception {

    public UserLoginNotOKException() {
        super("Incorrect email or password");
    }
}
