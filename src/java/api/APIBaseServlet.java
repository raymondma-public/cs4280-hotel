package api;

import com.google.gson.Gson;
import exception.CSRFException;
import exception.ForbiddenException;
import exception.UnauthorizedException;
import model.User;
import sun.util.logging.PlatformLogger;
import utils.Is;
import utils.SecurityUtil;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.logging.Logger;
import java.util.logging.Level;

public abstract class APIBaseServlet extends HttpServlet {

    protected static Gson gson = new Gson();

    protected void checkUserPermission(String authorizedRole, HttpServletRequest request) throws UnauthorizedException, ForbiddenException {
        User user = (User) request.getAttribute("user");
        checkUserLogon(request);
        String userRole = user.getUserType();
        if (!authorizedRole.equals(userRole)) {
            throw new ForbiddenException(userRole, authorizedRole);
        }
    }

    protected void checkUserPermission(String[] authorizedRoles, HttpServletRequest request) throws UnauthorizedException, ForbiddenException {
        User user = (User) request.getAttribute("user");
        checkUserLogon(request);
        String userRole = user.getUserType();
        if (Is.NotInList(userRole, authorizedRoles)) {
            throw new ForbiddenException(userRole, authorizedRoles);
        }
    }

    protected void checkUserLogon(HttpServletRequest request) throws UnauthorizedException {
        HttpSession session = request.getSession(false);
        User user = (User) request.getAttribute("user");
        if (session == null || user == null) {
            throw new UnauthorizedException();
        }
    }

    private static String getBody(HttpServletRequest request) throws IOException {
        StringBuilder buffer = new StringBuilder();
        BufferedReader reader = request.getReader();
        String line;
        while ((line = reader.readLine()) != null) {
            buffer.append(line);
        }
        String body = buffer.toString();
        return body;
    }

    private static HashMap<String, Object> getJSONBody(HttpServletRequest request) throws IOException {
        String bodyStr = getBody(request);
        return gson.fromJson(bodyStr, HashMap.class);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        middleware(request, response);

        PrintWriter out = response.getWriter();

        HashMap<String, Object> responseData = (HashMap) request.getAttribute("responseData");

        try {
            SecurityUtil.checkReferer(request);

            processGet(request, response);
        } catch (URISyntaxException e) {
            responseData.put("error_message", e.getMessage());
            responseData.put("status", 400);
        } catch (CSRFException e) {
            responseData.put("error_message", e.getMessage());
            responseData.put("status", 400);
        } catch (ForbiddenException e) {
            responseData.put("error_message", e.getMessage());
            responseData.put("status", 403);
        } catch (UnauthorizedException e) {
            responseData.put("error_message", e.getMessage());
            responseData.put("status", 401);
        } finally {
            endRequset(response, responseData);
        }

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        middleware(request, response);

        PrintWriter out = response.getWriter();

        HashMap<String, Object> responseData = (HashMap) request.getAttribute("responseData");

        try {
            processPost(request, response);
        } catch (ForbiddenException e) {
            responseData.put("error_message", e.getMessage());
            responseData.put("status", 403);
        } catch (UnauthorizedException e) {
            responseData.put("error_message", e.getMessage());
            responseData.put("status", 401);
        } finally {
            endRequset(response, responseData);
        }
    }

    @Override
    protected void doPut(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        middleware(request, response);

        HashMap<String, Object> responseData = (HashMap) request.getAttribute("responseData");

        try {
            processPut(request, response);
        } catch (UnauthorizedException e) {
            responseData.put("error_message", e.getMessage());
            responseData.put("status", 401);
        } catch (ForbiddenException e) {
            responseData.put("error_message", e.getMessage());
            responseData.put("status", 403);
        } finally {
            endRequset(response, responseData);
        }
    }

    @Override
    protected void doDelete(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        middleware(request, response);

        HashMap<String, Object> responseData = (HashMap) request.getAttribute("responseData");

        try {
            processDelete(request, response);
        } catch (UnauthorizedException e) {
            responseData.put("error_message", e.getMessage());
            responseData.put("status", 401);
        } catch (ForbiddenException e) {
            responseData.put("error_message", e.getMessage());
            responseData.put("status", 403);
        } finally {
            endRequset(response, responseData);
        }
    }

    private void middleware(HttpServletRequest request, HttpServletResponse response) throws IOException {
        HttpSession session = request.getSession(false);
        if (session != null)
            request.setAttribute("user", session.getAttribute("user"));

        request.setAttribute("JSONbody", getJSONBody(request));

        HashMap<String, Object> responseData = new HashMap();
        responseData.put("success", false); // set for default fail
        request.setAttribute("responseData", responseData);

        response.setContentType("application/json");
    }


    private void endRequset(HttpServletResponse response, HashMap<String, Object> responseData) {
        try {
            PrintWriter out = response.getWriter();

            if (responseData.get("success").equals(false) && !responseData.containsKey("error_message")) {
                responseData.put("error_message", "Unexpected Error");
                responseData.put("status", 500);
            }
            if (!responseData.containsKey("status")) {
                if (responseData.get("success").equals(false))
                    responseData.put("status", 400);
                else
                    responseData.put("status", 200);
            }
            response.setStatus((Integer) responseData.get("status"));

            out.print(gson.toJson(responseData));
        } catch (IOException e) {
            Logger.getLogger(APIBaseServlet.class.getName()).log(Level.WARNING, null, e);
        }
    }

    protected abstract void processGet(HttpServletRequest request, HttpServletResponse response) throws UnauthorizedException, ForbiddenException, IOException;

    protected abstract void processPost(HttpServletRequest request, HttpServletResponse response) throws UnauthorizedException, ForbiddenException, IOException;

    protected abstract void processPut(HttpServletRequest request, HttpServletResponse response) throws UnauthorizedException, ForbiddenException, IOException;

    protected abstract void processDelete(HttpServletRequest request, HttpServletResponse response) throws UnauthorizedException, ForbiddenException, IOException;

    @Override
    public String getServletInfo() {
        return "Hotel API";
    }

}
