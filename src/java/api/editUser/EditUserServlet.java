package api.editUser;

import api.APIBaseServlet;
import exception.ForbiddenException;
import exception.UnauthorizedException;
import exception.UserNotFoundException;
import model.User;
import sun.security.validator.ValidatorException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;

public class EditUserServlet extends APIBaseServlet {

    @Override
    protected void processGet(HttpServletRequest request, HttpServletResponse response) throws UnauthorizedException, ForbiddenException, IOException {

    }

    @Override
    protected void processPost(HttpServletRequest request, HttpServletResponse response) throws UnauthorizedException, ForbiddenException, IOException {

    }

    @Override
    protected void processPut(HttpServletRequest request, HttpServletResponse response) throws UnauthorizedException, ForbiddenException, IOException {
        checkUserPermission("chiefManager", request);

        HashMap<String, Object> responseData = (HashMap) request.getAttribute("responseData");
        HashMap<String, Object> json = (HashMap) request.getAttribute("JSONbody");

        String userId = request.getParameter("userId");
        try {
            User user = User.getUserById(userId);
            if (json.containsKey("userName"))
                User.updateNameToDB(userId, (String) json.get("userName"));
            if (json.containsKey("phone"))
                User.updatePhoneToDB(userId, (String) json.get("phone"));
            if (json.containsKey("userType")) {
                if (json.get("userType").equals("manager") && json.get("managingHotelId") == null)
                    throw new ValidatorException("Managing Hotel is required");
                User.updateUserRoleToDB(userId, (String) json.get("userType"));
                User.updateUserManageHotelToDB(userId, (String) json.get("managingHotelId"));
            } else if (json.containsKey("managingHotelId")) {
                User.updateUserManageHotelToDB(userId, (String) json.get("managingHotelId"));
            }
            responseData.put("user", User.getUserById(userId));
            responseData.put("success", true);
        } catch (SQLException e) {
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            responseData.put("error_message", e.getMessage());
        } catch (UserNotFoundException e) {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            responseData.put("error_message", e.getMessage());
        } catch (ValidatorException e) {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            responseData.put("error_message", e.getMessage());
        }
    }

    @Override
    protected void processDelete(HttpServletRequest request, HttpServletResponse response) throws UnauthorizedException, ForbiddenException, IOException {
        checkUserPermission("chiefManager", request);

        HashMap<String, Object> responseData = (HashMap) request.getAttribute("responseData");
        HashMap<String, Object> json = (HashMap) request.getAttribute("JSONbody");

        String userId = request.getParameter("userId");
        try {
            User user = User.deleteUserById(userId);
            responseData.put("user", user);
            responseData.put("success", true);
        } catch (SQLException e) {
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            responseData.put("error_message", e.getMessage());
        } catch (UserNotFoundException e) {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            responseData.put("error_message", e.getMessage());
        }
    }

}
