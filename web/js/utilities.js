var Utilities = (function () {
    function notify(theme, title, message, info, icon) {
        $.amaran({
            position: "bottom right",
            cssanimationIn: 'bounceInDown',
            cssanimationOut: 'bounceOut',
            content: {
                title: title,
                message: message,
                info: info,
                icon: 'fa ' + icon
            },
            theme: theme,
            delay: 5000
        });
    }

    return {
        notify: notify
    }
})();